<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$ route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$ route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$ route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
|
|      SEMPRE DEIXAR DEFAULT CONTROLLER E 404 OVERRIDE EM PRIMEIRO
|
|
*/

$route['default_controller'] = 'home';
$route['404_override'] = 'home';

$route['admin/help/([a-zA-Z0-9_-]+)']       = 'admin/help/$1';
$route['admin/([a-zA-Z0-9_-]+)/(:any)']	    = '$1/admin/$2';
$route['admin/([a-zA-Z0-9_-]+)/(:any)/(:any)']                      = '$1/admin/$2/$3';
$route['admin/([a-zA-Z0-9_-]+)/(:any)/(:any)/(:any)']                      = '$1/admin/$2/$3/$4';
$route['admin/(login|logout|remove_installer_directory)']			= 'admin/$1';
$route['admin/([a-zA-Z0-9_-]+)']            = '$1/admin/index';

$route['sis/user/(:any)']	                    = 'users/view/$1';
$route['sis/my-profile']	                    = 'users/index';
$route['sis/downloads/(:any)']	                    = 'home/sis/downloads/$1';

$route['sis/([a-zA-Z0-9_-]+)/(:any)']	    = '$1/sis/$2';
$route['sis/([a-zA-Z0-9_-]+)/(:any)/(:any)']                      = '$1/sis/$2/$3';
$route['sis/([a-zA-Z0-9_-]+)/(:any)/(:any)/(:any)']                      = '$1/sis/$2/$3/$4';
$route['sis/(login|logout)']			= 'sis/$1';
$route['sis/([a-zA-Z0-9_-]+)']            = '$1/sis/index';


$route['api/ajax/(:any)']          			= 'api/ajax/$1';
$route['api/([a-zA-Z0-9_-]+)/(:any)']	    = '$1/api/$2';
$route['api/([a-zA-Z0-9_-]+)']              = '$1/api/index';

$route['login']                                 = 'users/login';
$route['firstaccess']                           = 'users/register';
$route['forgot']                                = 'users/reset_pass';
$route['forgot_complete']                       = 'users/reset_complete';

$route['sitemap.xml']                       = 'sitemap/xml';

$route['sobre']                                 = 'home/sobre';

/* End of file routes.php */
