<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 * @author      Gabriel Perini
 * @copyright   Copyright (c) 2021, LazzoCMS LLC
 * @package 	LazzoCMS\Core\Controllers
 */
class Sis_Controller extends Public_Controller
{
	/**
	 * 
	 */
	public function __construct()
	{
		parent::__construct();

		if (!isset($this->current_user->id))
		{
			redirect('login');
		}

		$this->load->library("Permissions_sis");

        $this->template->set_layout('sis');
	}
}
