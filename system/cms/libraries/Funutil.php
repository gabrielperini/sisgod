<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is a util class for run in LazzoCMS
 *
 * @author 		BF2 Tecnologia
 * @website		http://bf2tecnologia.com.br
 * @package 	LazzoCMS
 * @subpackage 	Util Module
 * @update		22/08/2013 14:15
 */
class Funutil
{
	/**
	 * Reference to the CodeIgniter instance
	 *
	 * @var object
	 */
	private $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function setLang($section = 'items', $mod = null){
		if (!$mod) $mod = $this->CI->module;
		$this->lg = strlen($this->CI->uri->segment(1)) == 2 ? $this->CI->uri->segment(1) : 'br';
		$supp_lang = array();
		foreach(config_item('supported_languages') as $key => $val) $supp_lang[$key] = $val['folder'];
		$this->CI->lang->load($mod.'/'.$section,$supp_lang[$this->lg]);
	}


	public function doLink($link)
	{
		$rt = strlen($this->CI->uri->segment(1)) == 2 ? $this->CI->uri->segment(1) : null;
		return $rt ? site_url($rt.'/'.$link) : site_url($link);
	}
	
	//MOUNT ARRAY OPTIONS FIELD IN SETTINGS
	public function makeArrays($array,$module){
	
		$rtn = null; $i = null;
		$arropt = array(
				'VARCHAR',
				'CHAR',
				'TEXT',
				'LONGTEXT',
		);
		
		foreach($array[0][$module] as $reg => $vals):
			if(in_array($vals['type'],$arropt)):
				if(!isset($vals['color']) or !$vals['color']):
					if($reg != 'id' && $reg != 'thumbnail' && $reg != 'image'):
						if($i): $rtn .= '|'; endif;
						$rtn .= $reg.'='.substr($this->modifyLetter($reg),0,-1); $i=1;
					endif;
				endif;
			endif;
		endforeach;

		return $rtn;
	}
	
	//CREATE DEFAULT TITLE BASED ON MODULE NAME
	public function modifyLetter($letter){
		$rtn='';
		$exp = explode("_",$letter);
		foreach($exp as $rt):
		$rtn .= ucfirst($rt).' ';
		endforeach;
		return $rtn;
	}
	
	//HASH OF SQL TYPES
	public function hashType($hash)
	{
		switch ($hash){
			case 253: { $rtn = 'VARCHAR'; break; }
			case 254: { $rtn = 'CHAR'; break; }
			case 3: { $rtn = 'INT'; break; }
			case 7: { $rtn = 'TIMESTAMP'; break; }
			case 10: { $rtn = 'DATE'; break; }
			case 11: { $rtn = 'TIME' ; break; }
			case 12: { $rtn = 'DATETIME'; break; }
			case 252: { $rtn = 'TEXT'; break; }
			case 1: { $rtn = 'TINYINT'; break; }
			default : {$rtn = $hash; break; }
		}

		return $rtn;
	}
	
	//CHANGE FILES CONTENTS
	public function changeFiles($path,$arrfind,$arrreplace){
		$file_contents = file_get_contents($path);
		$file_contents = str_replace($arrfind,$arrreplace,$file_contents);
		file_put_contents($path,$file_contents);
	}
	
	//END FILES CONTENTS
	public function endFiles($path,$arr){
		$myFile = $path;
		$fh = fopen($myFile, 'a') or die("can't open file");
		$stringData = $arr;
		fwrite($fh, $stringData);
		fclose($fh);
	}
	
	//CHANGE CONTENS AND STRUCTURES OF DEFAULTS FILES
	public function choiceChange($type=null, $files = array()){
		if($type):
			foreach($files as $fi) {
				if ($fi['isdir']) mkdir($fi['new'], 0777);
				$this->copyFiles($fi['default'], $fi['new']);
				if ($fi['isdir']) {
					$dir_new = opendir($fi['new']);
					while($arq = readdir($dir_new)) {
						if ($arq!="." && $arq!="..") {
							$this->changeFiles($fi['new'].'/'.$arq, $fi['find'], $fi['replace']);
						}
					}
					closedir($dir_new);
				}
				else $this->changeFiles($fi['new'], $fi['find'], $fi['replace']);
			}
		else:
			foreach($files as $fi) {
				$this->deleteFiles($fi['new']);
			}
		endif;
	}
	
	private function copyFiles($source, $dest){
		if(is_dir($source)) {
			$dir_handle=opendir($source);
			while($file=readdir($dir_handle)){
				if($file!="." && $file!=".."){
					if(is_dir($source."/".$file)){
						mkdir($dest."/".$file);
						copyFiles($source."/".$file, $dest."/".$file);
					} else {
						copy($source."/".$file, $dest."/".$file);
					}
				}
			}
			closedir($dir_handle);
		} else {
			copy($source, $dest);
		}
	}
	
	public function deleteFiles($source) {
		if (is_dir($source)) {
			$dir_handle=opendir($source);
			while($file=readdir($dir_handle)){
				if($file!="." && $file!=".."){
					if(is_dir($source."/".$file)){
						$this->deleteFiles($source."/".$file);
					} else {
						unlink($source."/".$file);
					}
				}
			}
			closedir($dir_handle);
		} else {
			return unlink($source);
		}
		return rmdir($source);			 			 		
	}
	
	//MAKE FORMS STRUCTURES
	public function makeForms($array,$module){
		
		$rr = $ctl = $obr = $req = $modfile = '';
		$array = $array[0][$module];
		
		foreach($array as $rs => $val){
			$tit = $rs;
			if(in_array($val['form'], ['none','id']) and !in_array($tit,['id_users','id_lang'])) continue;

			if(!in_array($tit,['id_users','id_lang'])){

				$val['NULL'] = isset($val['NULL']) ? $val['NULL'] : '';
	
				$trim = 'trim'; 
				if(!$val['NULL']):
					$req = '|required'; 
				endif; 
	
				$val['constraint'] = isset($val['constraint']) ? $val['constraint'] : '';
				if($val['constraint']): $cont = '|max_length['.$val['constraint'].']'; endif;
	
				$is_array = (bool)($val['form'] === 'multiselect');
	
				$arrKey = $is_array ? '[]' : '';
				
				$rr .= "array(
					'field' => '".$tit.$arrKey."',
					'label' => '".ucfirst($tit)."',
					'rules' => 'trim".$cont.$req."'
				),";
	
				if($is_array and $val['form'] !== 'file'):
					$ctl .= "'$tit' => serialize(\$input['$tit']),\n\t\t\t";
				elseif($val['form'] !== 'file'):
					$ctl .= "'$tit' => \$input['$tit'],\n\t\t\t";
				endif;
	
				if($val['form'] == 'file'):
					$modfile .= 'if($_FILES["'. $tit .'"]):'."\n".
					"\t\t\t".'$upload_path = UPLOAD_PATH.\'files/'.str_replace('_', '/', $module).'/\';'."\n".
					"\t\t\t".'mkdir($upload_path, 0755);'."\n".
					"\t\t\t".'if($_FILES["'. $tit .'"][\'tmp_name\']){'."\n".
					"\t\t\t\t".'copy($_FILES["'. $tit .'"][\'tmp_name\'],$upload_path.$_FILES["'. $tit .'"][\'name\']);'."\n".
					"\t\t\t\t".'if($input["'. $tit .'Old"]){'."\n".
					"\t\t\t\t\t".'unlink($upload_path.$input["'. $tit .'Old"]);'."\n".
					"\t\t\t\t".'}'."\n".
					"\t\t\t\t".'$to_insert["'. $tit .'"] = $_FILES["'. $tit .'"][\'name\'];'."\n".
					"\t\t\t".'}'."\n".
					"\t\t".'endif;'."\n\n\t\t";
				endif;
			}else{
				$is_array = (bool)($tit === 'id_lang');
	
				$arrKey = $is_array ? '[0]' : '';

				$rr .= "array(
					'field' => '".$tit."',
					'label' => '".ucfirst($tit)."',
					'rules' => ''
				),";
				$ctl .= "'$tit' => \$input['$tit']$arrKey,\n\t\t\t";
			}
			$obr = $req = $cont = $trim = $options = $whereQuery = '';
		}
		
		return array($rr,$ctl,$modfile);
	}
	
	// SHOW LANGS INTO SUP BAR MENU
	public function showLang() {
		$rtn = ''; $rt = '';
		$query = $this->CI->db->order_by('name','ASC')->get('languages')->result();
		foreach ($query as $rs){
			if($rs->prefix == 'br'):
				$rt = $rs->thumbnail;
			else: 
				//$qry = $this->db->get_where('settings',"module = '".$mod."' AND slug = 'set_".$mod."_".$item."_lang' AND value != '0'");
				//if($qry->num_rows() > 0):
				//echo 'ok';
					$rtn .= '<li style="float: right;"><a href="#section-content-'.$rs->prefix.'-tab"><img src="'.base_url().'uploads/'.SITE_REF.'/files/languages/'.$rs->thumbnail.'" height="20" /></a></li>';
				//endif; 
			endif;
		}
		return array($rt,$rtn);
	}
	
	//MAKE FORM LANGUAGE
	public function makeFormLang($mod=null,$id=null,$sect=null, $table=null)
	{
		$rtn = '';
		$details = 'Module_'.$mod;
		if ($sect === null) $sect = 'items';
		if ($table === null) $table = '_items';
		
		$rss = $this->CI->db->get_where('settings',"module = '".$mod."' AND slug = 'set_".$mod.'_'.$sect."_lang'")->result();
		
		if(!empty($rss) and $rss[0]->value != '0'):
			
			$fieldset = explode(",",$rss[0]->value);
			
			$query = $this->CI->db->get_where('languages','prefix != "'.config_item('default_language').'"')->result();
			
			foreach($query as $rs):
				
				$rtn .= '<div class="form_inputs" id="section-content-'.$rs->prefix.'-tab">
							<fieldset>
								<input type="hidden" name="id_lang[]" value="'.$rs->id.'" />
								<ul><li>'.$rs->name.'</li>';
				
						$qry = $this->CI->db->get_where($mod.$table,"id_lang = '".$rs->id."' AND id_mod = '".$id."'")->result();
						
						$val = !empty($qry) ? $qry[0]->id : '';
						if($val): $rtn .= '<input type="hidden" name="lang['.$rs->id.']" value="'.$val.'" />'; else: $rtn .= '<input type="hidden" name="lang['.$rs->id.']" value="x" />'; endif;
						
						$fields = $this->CI->db->select($fieldset)->get($mod.$table)->field_data();
						
						foreach($fields as $field):
							$classes = 'width-15';
							
							$field_type = funutil::hashType($field->type);
							$classes .= funutil::getClassesInput($field_type,null);
							$funct = funutil::defineField($field_type);
							
							$rtn .= '<li class="'.alternator('', 'even').'">
										<label for="'.$field->name.'">'.lang($mod.'_'.$sect.':'.$field->name).' </label>
										<div class="input">'.
											$funct($field->name.'_lang['.$rs->id.']', set_value($field->name.'_lang['.$rs->id.']', !empty($qry) ? $qry[0]->{$field->name} : ''), 'id="'.$field->name.'_lang['.$rs->id.']" class="'.$classes.'"').
										'</div>
									</li>';
						endforeach;
				
				$rtn .= '		</ul>
							</field>
						</div>';
	
			endforeach;
			
		endif;
		
		return $rtn;
	}
	
	public function updateLangs($mod=null,$input=null,$id=null){
		
		$for = count($input['id_lang']);
		$rtn = ''; $status = null;
		
		for($i=1;$i<$for;$i++):
			
			foreach($input as $rt => $rr):
				if(is_array($rr)):
					if($rt == 'id_lang') $rtn[$rt] = $rr[$i];
					elseif ($rt == 'lang') $actual_lang = $rr[$rtn['id_lang']];
					else {
						if ($rr[$rtn['id_lang']]) $rtn[str_replace("_lang","",$rt)] = $rr[$rtn['id_lang']];
						else {
							$rtn[str_replace("_lang","",$rt)] = $input[str_replace("_lang","",$rt)];
						}
					}
				else:
					$rtn[$rt] = $rr;
					if ($rtn[$rt] === '') $rtn[$rt] = null;
				endif;
			endforeach;
			
			if($actual_lang == 'x'):
				$rtn['id_mod'] = $id;
				$status = $thi->CIs->db->insert($mod,$rtn);
			else:
				$rtn['id_mod'] = $id;
				$status = $this->CI->db->where('id_mod', $id)->where('id_lang', $rtn['id_lang'])->update($mod, $rtn);
			endif;
			
			$rtn = array();
			
			if (!$status) return false;
		
		endfor;

		return true;
		
	}

	public function defaultTypes($table, $rules)
	{
		foreach ($table as $field => $arr) {			
			$form = strtolower($arr['form']); 

			if(!$form){
				continue;
			}

			if(array_key_exists( $form , $rules) ){
				$data = $rules[$form];
			}else {
				$data = array(
					'type' => 'VARCHAR',
					'constraint' => 100
				);
			}
			$table[$field] = array_merge($data,$arr);
		}

		return $table;
	}
}