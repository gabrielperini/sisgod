<?php  defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * LazzoCMS Files Model
 *
 * @author		Jerel Unruh - LazzoCMS Dev Team
 * @package		LazzoCMS\Core\Modules\Files\Models
 */
class File_m extends MY_Model {

	protected $table = 'files';

	// ------------------------------------------------------------------------

	/**
	 * Exists
	 *
	 * Checks if a given file exists.
	 * 
	 * @author	Eric Barnes <eric@lazzocms.com>
	 * @access	public
	 * @param	int		The file id
	 * @return	bool	If the file exists
	 */
	public function exists($file_id)
	{
		return (bool) (parent::count_by(array('id' => $file_id)) > 0);
	}
	
	public function get_files($key = 'folder_id', $value = 0)
	{
		$files = $this
		->order_by('sort', 'asc')
		->get_many_by(array($key => $value));
		
		return $files;
	}
}

/* End of file file_m.php */