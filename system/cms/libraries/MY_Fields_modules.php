<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Fields Modules
 *
 * simple library of fields to forms in the modules
 *
 * @version		1.1
 * @author		Gabriel Perini
 * @copyright	2021 Gabriel Perini
 */

class MY_Fields_modules
{
    /**
	 * Reference to the CodeIgniter instance
	 *
	 * @var object
	 */
	private $CI;
	

	/**
	 * Constructor
	 */
	public function __construct()
	{
        $this->CI =& get_instance();
	}

    /**
     * 
     * Insert Fields
     * 
     */
    public function insert_fields($fields,$namespace)
    {
        foreach ($fields as $key => $data) {
            $form = $data['form'];
            if(!in_array($form,['id','none'])){
                unset($data['form']);
                $insert = array(
                    'field_name' => $namespace.':'.$key,
                    'field_slug' => $key,
                    'field_namespace' => $namespace,
                    'field_type' => $form,
                    'field_data' => serialize($data)
                );
                $this->CI->db->insert('data_fields',$insert);
            }
        }
    }


    /**
     * 
     * Delete Fields
     * 
     */
    public function delete_fields($namespace)
    {
        $this->CI->db->where('field_namespace',$namespace)->delete('data_fields');
    }

    /**
     * 
     * Field Text
     * 
     */
    public function field_text($field,$value)
    {
        $input = form_input($field->field_slug,set_value($field->field_slug,$value),'class="form-control" id="'.$field->field_slug.'" '.$field->field_data['extra']);

        $rtn = $this->default_container($field,$input);

        return $rtn;
    }

    /**
     * 
     * Field Textarea
     * 
     */
    public function field_textarea($field,$value)
    {
        $input = form_textarea($field->field_slug,set_value($field->field_slug,$value),'class="form-control" id="'.$field->field_slug.'" '.$field->field_data['extra']);

        $rtn = $this->default_container($field,$input);

        return $rtn;
    }

    /**
     * 
     * Field checkbox
     * 
     */
    public function field_checkbox($field,$value)
    {
        $input .= '<div class="form-check" >';
        $input .= form_checkbox($field->field_slug,true,(bool)$value,'class="form-check-input" id="'.$field->field_slug.'"');
        $input .= '<label class="form-check-label" '. $field->field_data['extra'] .' >'.$field->field_data['checkLabel'].'</label></div>';

        $rtn = $this->default_container($field,$input);

        return $rtn;
    }

    /**
     * 
     * Field ckeditor
     * 
     */
    public function field_ckeditor($field,$value)
    {
        $input = form_textarea($field->field_slug,$value,'class="form-control" id="'.$field->field_slug.'" '.$field->field_data['extra']);

        $script = '<script> CKEDITOR.replace( "'.$field->field_slug.'" ); </script>';

        $rtn = $this->default_container($field,$input . $script);

        return $rtn;
    }

    /**
     * 
     * Field dropdown
     * 
     */
    public function field_dropdown($field,$value)
    {

        $options = $this->options($field->field_data);

        $input = form_dropdown($field->field_slug,$options,$value,'class="form-control" id="'.$field->field_slug.'" '.$field->field_data['extra']);

        $rtn = $this->default_container($field,$input);

        return $rtn;
    }

    /**
     * 
     * Field multiselect
     * 
     */
    public function field_multiselect($field,$value)
    {

        $options = $this->options($field->field_data);

        $input = form_multiselect($field->field_slug."[]",$options,unserialize($value),'id="'.$field->field_slug.'" '.$field->field_data['extra']);

        $rtn = $this->default_container($field,$input);

        return $rtn;
    }

    /**
     * 
     * Field date
     * 
     */
    public function field_date($field,$value)
    {
        $input = form_input($field->field_slug,set_value($field->field_slug,$value),
            'class="form-control" id="'.$field->field_slug.'" data-toggle="datepicker" autocomplete="off" '.$field->field_data['extra']);

        $rtn = $this->default_container($field,$input);

        return $rtn;
    }

    /**
     * 
     * Field datetime
     * 
     */
    public function field_datetime($field,$value)
    {
        $input = form_input($field->field_slug,$value ? (new Datetime($value))->format("Y-m-d\TH:i") : "",
            'class="form-control" id="'.$field->field_slug.'" data-toggle="datetimepicker" autocomplete="off" '.$field->field_data['extra']);

        $rtn = $this->default_container($field,$input);

        return $rtn;
    }

    /**
     * 
     * Field time
     * 
     */
    public function field_time($field,$value)
    {
        $input = form_input($field->field_slug,set_value($field->field_slug,$value),
            'class="form-control" id="'.$field->field_slug.'" data-toggle="timepicker" autocomplete="off"'.$field->field_data['extra']);

        $rtn = $this->default_container($field,$input);

        return $rtn;
    }

    /**
     * 
     * Field file
     * 
     */
    public function field_file($field,$value)
    {
        $req = $field->field_data['required'] ? '<span>*</span>' : '';

        $input = form_upload($field->field_slug,$value,'class="custom-file-input"');

        $link = UPLOAD_PATH.'files/' . str_replace('_', '/', $field->field_namespace) .'/'. $value ;

        $file = $value ? form_hidden($field->field_slug."Old",$value) . '<br/><a href="'.$link.'" target="_blank">'.$value.'</a>' : ''; 

        $children =  '<div class="custom-file" id="'.$field->field_slug.'">'.
                    $input.
                    '<label class="custom-file-label" '.$field->field_data['extra'].' for="'.$field->field_slug.'">'.lang($field->field_namespace.':choose').'</label>' .
                '</div>'.$file;

        $rtn = $this->default_container($field,$children);

        return $rtn;

        
    }
    
    /**
     * 
     * Field image
     * 
     */
    public function field_image($field,$value)
    {
        return $this->field_hidden($field,$value);
    }

    /**
     * 
     * Field hidden
     * 
     */
    public function field_hidden($field,$value)
    {
        $input = form_hidden($field->field_slug,set_value($field->field_slug,$value),'class="form-control" id="'.$field->field_slug.'" '.$field->field_data['extra']);

        return $input;
    }

    /**
     * 
     * Default Container
     * 
     */
    private function default_container($field,$children)
    {
        $req = $field->field_data['NULL'] ? '' : '<span>*</span>';

        $rtn = '<div class="position-relative form-group ">'.
                    '<label for="'.$field->field_slug.'">' . lang($field->field_name) . ' ' . $req . '</label>'. 
                    $children . 
                '</div>';

        return $rtn;
    }
    
    /**
     * 
     * options to selects
     * 
     */
    private function options($data)
    {
        if(is_array($data['options'])){
            if($data['options']['where']) $this->CI->db->where($data['options']['where']);
            $opts = $this->CI->db->get($data['options']['table'])->result();
            foreach ($opts as $o) {
               $options[$o->{$data['options']['key']}] = $o->{$data['options']['value']};
            }
        }else{
            $opts = explode('|',$data['options']);
            $options = array();
            foreach ($opts as $opt) {
                $values = explode('=',$opt);
                $options[$values[0]] = $values[1];
            }
        }
        return $options;
    }



}