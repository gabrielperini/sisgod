<?php  defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Push Notifications Rest Api One Signal
 *
 * @author		Gabriel Perini
 * 
 */
class Push {

    /**
     * Variáveis para preencher
     * @var categoria_android é a categoria da mensagem 
     * @var app_id é o id do OneSignal
     * @var app_rest_id é o id da Rest Api do OneSignal
     */
    private $categoria_android = null;
    private $app_id = "";
    private $app_rest_id = "";


    /**
     * Variável de data do envio da mensagem
     * 
     * @var
     */
    private $date = null;

    /**
     * Variável de data do envio da mensagem
     * 
     * @var
     */
    private $icon = 'icon';

    public function __contruct($data = array())
    {
        $this->categoria_android = $data['categoria_android'];
        $this->app_id = $data['app_id'];
        $this->app_rest_id = $data['app_rest_id'];
    }

    /**
     * Define a data de envio da mensagem
     * 
     * @param string $data
     * 
     * @return object $this
     * 
     */
    public function setDate($date)
    {
        $dt = new DateTime($date);
        $this->date = $dt->format('Y-m-d H:i:s') . " UTC-0300";
        
        return $this;
    }

    /**
     * Envia a mensagem
     * 
     * @param string $title 
     * @param string $content
     * @param $userId Array OR String
     * 
     * @return JSON response   
     */    
    public function sendMessage($title , $content , $userId) 
    {
        $content = array(
            "en" => $content
        );
        $title = array(
            "en" => $title
        );
        $fields = array(
            'app_id' => $this->app_id,
            'contents' => $content,
            'headings' => $title,
            'large_icon' => $this->icon 
        );

        if($this->date){
            $fields['send_after'] = $this->date;
        }
        if($this->categoria_android){
            $fields['android_channel_id'] = $this->categoria_android;
        }

        if($userId){
            
            if(is_array($userId)){

                $fields['include_external_user_ids'] = $userId;

            }else if(is_string($userId)){

                $fields['include_external_user_ids'] =  array(
                                                            $userId
                                                        );

            }
            
        }else{
            $fields['included_segments'] = array(
                'All'
            );
        }
        
        $fields = json_encode($fields);
        // print($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $this->app_rest_id
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $response = curl_exec($ch);
        curl_close($ch);
        
        return "JSON" . $response;
    }

    /**
     * Cancela a mensagem ja enviada
     * 
     * @param string $idN 
     * 
     * @return JSON response   
     */ 
    public function deletePush($idN)
    {
        $ch = curl_init();
        
        $url = "https://onesignal.com/api/v1/notifications/". $idN ."?app_id=" . $this->app_id;

        $httpHeader = array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $this->app_rest_id
        );

        $options = array (
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $httpHeader,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_SSL_VERIFYPEER => FALSE
        );

        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}