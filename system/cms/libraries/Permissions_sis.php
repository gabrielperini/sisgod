<?php  defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Permissions to SisGod
 *
 * @author		Gabriel Perini
 * 
 */
class Permissions_sis {

    private $tipo;
    private $grau;
    private $ci;

    public function __construct()
    {
        $this->ci = ci();
        $this->tipo = $this->ci->current_user->tipo_de_usuario;
        $this->grau = $this->ci->current_user->grau_demolay;
    }

    public function verify($tipo,$grau = ["0","1","2"])
    {

        if(!is_array($tipo)) $tipo = [$tipo];
        if(!is_array($grau)) $grau = [$grau];

        $result = in_array($this->tipo,$tipo);
        if($this->tipo === "D" and $result){
            $result = in_array($this->grau,$grau);
        }
        return $result;
    }

    public function replace($replace)
    {
        if(array_key_exists($this->tipo,$replace)){
            $option = $replace[$this->tipo];
            
            if($this->tipo === "D"){
                if(array_key_exists($this->grau,$option)){
                    $option = $option[$this->grau];
                }else{
                    $option = $option["default"];
                }
            }
        }else{
            $option = $replace["default"] ? $replace["default"] : false;
        }
        return $option;
    }

}