<ol>
	<li class="mb-3">
		<label>Address</label>
		<?php echo form_input('address', $options['address'],"class='form-control'"); ?>
	</li>
	<li class="mb-3">
		<label>Width</label>
		<?php echo form_input('width', ($options['width'] != '' ? $options['width'] : '100%'),"class='form-control'"); ?>
	</li>
	<li class="mb-3">
		<label>Height</label>
		<?php echo form_input('height', ($options['height'] != '' ? $options['height'] : '400px'),"class='form-control'"); ?>
	</li>
	<li class="mb-3">
		<label>Zoom Level</label>
		<?php echo form_input('zoom', ($options['zoom'] != '' ? $options['zoom'] : '16'),"class='form-control'"); ?>
	</li>
	<li class="mb-3">
		<label>Description (optional)</label>
		<?php echo form_textarea('description', $options['description'],"class='form-control'"); ?>
	</li>
</ol>