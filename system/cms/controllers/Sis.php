<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * The admin class is basically the main controller for the backend.
 *
 * @author      LazzoCMS Dev Team
 * @copyright   Copyright (c) 2012, LazzoCMS LLC
 * @package	 	LazzoCMS\Core\Controllers
 */
class Sis extends Sis_Controller
{
	/**
	 * Constructor method
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the panel
	 */
	public function index()
	{
		$this->template
			->title("Painel");

		$this->template
			->build('sis/index');
	}

}