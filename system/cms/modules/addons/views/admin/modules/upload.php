<section class="main-card mb-3 card">
	<div class="card-header">
		<h4><?php echo lang('addons:modules:upload_title');?></h4>
	</div>
	<?php echo form_open_multipart('admin/addons/modules/upload') ?>
	
	<div class="card-body">
		<h6><?php echo lang('addons:modules:upload_desc') ?></h6>
		<div class="custom-file" id="customFile">
			<input type="file" class="custom-file-input" name="userfile" id="userfile" lang="pt-br">
			<label class="custom-file-label" for="userfile">
				Selecione Algum Arquivo
			</label>
		</div>
	</div>
	<div class="card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('upload'))) ?>
	</div>
			
	<?php echo form_close() ?>
	
</section>
