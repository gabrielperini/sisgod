<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
	<thead>
		<tr role="row" >
			<th><?php echo lang('name_label');?></th>
			<th><span><?php echo lang('desc_label');?></span></th>
			<th><?php echo lang('version_label');?></th>
			<th>Ações</th>
		</tr>
		</thead>

	<tbody>
	<?php foreach ($plugins as $plugin): ?>
	<tr role="row" >
		<td width="30%"><?php echo $plugin['name'] ?></td>
		<td width="60%"><?php echo $plugin['description'] ?></td>
		<td><?php echo $plugin['version'] ?></td>
		<td class="d-flex justify-content-center">
			<?php if ($plugin['self_doc']): ?>
			<button data-toggle="modal" data-target="#<?php echo $plugin['slug'] ?>" 
				class="pe-7s-search icon-gradient bg-dark font-weight-bold btn font-size-xlg border-0" 
				style="margin-right:8px;"></button>
			<?php else: ?>
				<i class="pe-7s-close icon-gradient bg-dark font-weight-bold btn font-size-xlg border-0"></i>
			<?php endif ?>
		</td>
	</tr>
	<?php endforeach ?>
	</tbody>
</table>