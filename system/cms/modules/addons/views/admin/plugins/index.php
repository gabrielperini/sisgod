<section class="main-card mb-3 card">
	<section class="card-header">
		<h4><?php echo lang('addons:plugins:add_on_plugins');?></h4>
	</section>
	<div class="card-body">
		<?php echo $this->load->view('admin/plugins/_table', array('plugins' => $plugins), true) ?>
	</div>
</section>

<section class="main-card mb-3 card">
	<section class="card-header">
		<h4><?php echo lang('addons:plugins:core_plugins');?></h4>
	</section>
	<div class="card-body">
		<?php echo $this->load->view('admin/plugins/_table', array('plugins' => $core_plugins), true) ?>
	</div>
</section>