<section class="main-card mb-3 card">
	<section class="card-header">
		<h4><?php echo lang('addons:plugins:core_field_types') ?></h4>
	</section>
	<div class="card-body">
		<?php if ($core): ?>
		<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
			<thead>
				<tr role="row" >
					<th><?php echo lang('name_label');?></th>
					<th><?php echo lang('version_label');?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($core as $c_ft): ?>
				<tr role="row" >
					<td width="60%"><?php echo $c_ft['name'] ?>
					<td><?php echo $c_ft['version'] ?>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
		<?php endif ?>
		<?php if ( ! empty($addon)): ?>
		<section class="title">
			<h4><?php echo lang('addons:plugins:add_on_field_types') ?></h4>
		</section>
		<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
			<thead>
				<tr role="row" >
					<th><?php echo lang('name_label');?></th>
					<th><?php echo lang('version_label');?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($addon as $a_ft): ?>
				<tr role="row" >
					<td width="60%"><?php echo $a_ft['name'] ?>
					<td><?php echo $a_ft['version'] ?>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
		<?php endif ?>
	</div>
</section>
