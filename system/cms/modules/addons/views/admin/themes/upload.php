<section class="main-card mb-3 card">
	<div class="card-header">
		<h4><?php echo lang('addons:themes:upload_title');?></h4>
	</div>
	<?php echo form_open_multipart('admin/addons/themes/upload') ?>
	
	<div class="card-body">
		<h6><?php echo lang('addons:themes:upload_desc') ?></h6>
		<div class="custom-file" id="customFile">
			<input type="file" class="custom-file-input" name="userfile" id="userfile" lang="es">
			<label class="custom-file-label" for="userfile">
				Selecione Algum Arquivo
			</label>
		</div>
	</div>
	<div class="card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('upload'))) ?>
	</div>
			
	<?php echo form_close() ?>
	
</section>