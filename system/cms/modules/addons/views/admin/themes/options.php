
<section class="main-card mb-3 card">
	<div class="card-header">
		<h4><?php echo lang('addons:themes:theme_label').' '.lang('addons:themes:options') ?></h4>
	</div>
	<?php if ($options_array):  
	echo form_open('admin/addons/themes/options/'.$slug, 'class="form_inputs options-form"');?>

		<div class="card-body">
			<?php echo form_hidden('slug', $slug) ?>
			
			<?php foreach($options_array as $option): ?>
				<div id="<?php echo $option->slug ?>" class="position-relative form-group">
					<label for="<?php echo $option->slug ?>">
						<?php echo $option->title ? $option->title : $option->slug ?>
						<small class="d-block"><?php echo $option->description ?></small>
					</label>
					<?php echo $controller->form_control($option) ; ?>
				</div>
			<?php endforeach ?>
		</div>
		<div class="card-footer">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('re-index') )) ?>
		</div>
	<?php echo form_close() ?>
	<?php endif ?>
</section>

<script type="text/javascript">
	(function($) {
		$(function() {
			$('.colour-picker').miniColors({
				letterCase: 'uppercase',
			});
		});
	})(jQuery);
</script>