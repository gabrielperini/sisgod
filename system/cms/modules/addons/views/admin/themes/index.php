<section class="main-card mb-3 card">
	<section class="card-header">
		<h4><?php echo lang('addons:themes');?></h4>
	</section>
	<?php echo form_open('admin/addons/themes/set_default') ?>
	<div class="card-body">
	<?php if ($themes): ?>
	
		<?php echo form_hidden('method', $this->method) ?>
		<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
			<thead>
				<tr role="row">
					<th width="50px" class="align-center"><?php echo lang('addons:themes:default_theme_label') ?></th>
					<th width="15%"><?php echo lang('addons:themes:theme_label') ?></th>
					<th class="collapse"><?php echo lang('global:description') ?></th>
					<th class="collapse" width="15%"><?php echo lang('global:author') ?></th>
					<th width="50px" class="align-center"><?php echo lang('addons:themes:version_label') ?></th>
					<th width="250px">Ações</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($themes as $theme): ?>
				<tr role="row">
					<td class="align-center"><input type="radio" name="theme" value="<?php echo $theme->slug ?>"
						<?php echo isset($theme->is_default) ? 'checked' : '' ?> />
					</td>
					<td>
						<?php if ( ! empty($theme->website)): ?>
							<?php echo anchor($theme->website, $theme->name, array('target'=>'_blank')) ?>
						<?php else: ?>
							<?php echo $theme->name ?>
						<?php endif ?>
					</td>
					<td class="collapse"><?php echo $theme->description ?></td>
					<td class="collapse">
						<?php if ($theme->author_website): ?>
							<?php echo anchor($theme->author_website, $theme->author, array('target'=>'_blank')) ?>
						<?php else: ?>
							<?php echo $theme->author ?>
						<?php endif ?>
					</td>
	
					<td class="align-center"><?php echo $theme->version ?></td>
					<td class="actions">
						<?php echo isset($theme->options) ? anchor('admin/addons/themes/options/'.$theme->slug, lang('addons:themes:options'), 'title="'.$theme->name.'" class="btn-hover-shine btn btn-dark"') : '' ?>
						<a href="<?php echo $theme->screenshot ?>" rel="screenshots" title="<?php echo $theme->name ?>" class="button modal"><?php echo lang('buttons:preview') ?></a>
						<?php if($theme->slug != 'admin_theme') { echo anchor('admin/addons/themes/delete/'.$theme->slug, lang('buttons:delete'), 'class="btn-hover-shine btn btn-danger"'); } ?>
					</td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
	<div class="card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )) ?>
	</div>
	<?php echo form_close() ?>
	<?php else: ?>
		<div class="blank-slate card-body">
			<p><?php echo lang('addons:themes:no_themes_installed') ?></p>
		</div>
	<?php endif ?>
	<div class="card-footer">
		<?php echo anchor('/admin/addons/themes/options/' . $this->settings->admin_theme, "Tema Admin",'class="mb-2 mr-2 btn-hover-shine btn btn-dark"'); ?>
	</div>
</section>