$(function(){
    $('#fixed-header input[type=checkbox],#fixed-sidebar input[type=checkbox]').click(function(){
        $(".app-container").toggleClass(this.value);
    })

    $('#color-header select,#color-sidebar select').change(function(){
        var e = this.value,
        part = this.name.slice(6);
        $(".app-"+part+"").attr("class","app-"+part+""),
        $(".app-"+part+"").addClass(""+part+"-shadow "+e)
    })
})