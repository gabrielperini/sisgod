<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Admin controller for the widgets module.
 *
 * @package   LazzoCMS\Core\Modules\Addons\Controllers
 * @author    LazzoCMS Dev Team
 * @copyright Copyright (c) 2012, LazzoCMS LLC
 */
class Admin_plugins extends Admin_Controller
{

	/** @var string The current active section */
	protected $section = 'plugins';

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index method
	 * 
	 * Lists all plugins.
	 */
	public function index()
	{
		$data = array('plugins' => array(), 'core_plugins' => array());

		$this->load->language('addons');

		$this->load->helper('directory');

		$this->load->library('plugins');

		$is_core = true;
		// Find all the plugins available.
		foreach (array(APPPATH, ADDONPATH, SHARED_ADDONPATH) as $directory)
		{
			$index = $is_core ? 'core_plugins' : 'plugins';
			$data[$index] = array_merge($data[$index], $this->_gather_plugin_info($directory.'modules/*/plugin.php'));
			$data[$index] = array_merge($data[$index], $this->_gather_plugin_info($directory.'plugins/*.php'));

			$is_core = false;
		}

		sort($data['core_plugins']);
		sort($data['plugins']);

		// Create the layout
		$this->template
			->set_partial('modals','admin/partials/modals',$this->plugins_in_modal($data))
			->title($this->module_details['name'])
			->build('admin/plugins/index',$data);
	}

	private function _gather_plugin_info($path)
	{
		if ( ! $files = glob($path))
		{
			return array();
		}

		$plugins = array();
		// Go through and load up some info about our plugin files.
		foreach ($files as $file)
		{
			$module_path = false;

			$tmp         = explode('/', $file);
			$file_name   = array_pop($tmp);

			// work out the filename
			$file_parts = explode('.', $file_name);
			array_pop($file_parts);
			$file_name = implode('.', $file_parts);

			// it's in a module, we have to use the module slug
			if ($file_name === 'plugin')
			{
				$module_path = dirname($file);
				$tmp         = explode('/', $module_path);
				$module      = array_pop($tmp);
				$class_name  = 'Plugin_'.ucfirst($module);
				$slug        = $module;

				// add the package path so $this->load will work in the plugin
				$this->load->add_package_path($module_path);
			}
			else
			{
				$class_name = 'Plugin_'.ucfirst($file_name);
				$slug = $file_name;
			}

			include_once $file;

			if (class_exists($class_name))
			{
				$plugin = new $class_name();
				array_push($plugins, array(
					'name' => (isset($plugin->name[CURRENT_LANGUAGE])) ? $plugin->name[CURRENT_LANGUAGE] : (isset($plugin->name[CURRENT_LANGUAGE])) ? $plugin->name[CURRENT_LANGUAGE] : ((isset($plugin->name['en'])) ? $plugin->name['en'] : ucfirst($file_name)),
					'slug' => $slug,
					'description' => (isset($plugin->description[CURRENT_LANGUAGE])) ? $plugin->description[CURRENT_LANGUAGE] : null,
					'version' => (isset($plugin->version)) ? $plugin->version : null,
					'self_doc' => (method_exists($plugin, '_self_doc') ? $plugin->_self_doc() : array()),
				));

			}

			if ($module_path)
			{
				$this->load->remove_package_path($module_path);
			}
		}

		return $plugins;
	}

	private function plugins_in_modal($data){
		$result = array();
		foreach ($data as $group) {
			foreach ($group as $plugin) {
				$modal = [];
				$modal['id'] = $plugin['slug'];
				$modal['title'] = $plugin['name'];
				$modal['size'] = 'modal-lg';

				$body = '';

				$body .= '<section class="method-area">';
					if ($plugin['self_doc'] and $plugin['self_doc']):
						foreach ($plugin['self_doc'] as $method => $doc):
							$body .= '<div class="method">';
								$body .= '<h3 class="font-weight-bold" >' . $plugin['slug'].':'.$method .'</h3>';
								$body .= '<p>'. htmlentities(isset($doc['description'][CURRENT_LANGUAGE]) ? $doc['description'][CURRENT_LANGUAGE] : (isset($doc['description']['en']) ? $doc['description']['en'] : ''), ENT_COMPAT, 'UTF-8');
								$body .= '</p>';
								$body .= '<pre class="mx-3 my-4" >';
									$body .= '<code class="text-info">';
									if (isset($doc['single']) and $doc['single']):
										$body .= '{{'. $plugin['slug'].':'.$method .'}}';
									endif;
									if (isset($doc['double']) and $doc['double']):

										$body .= '{{'. $plugin['slug'].':'.$method .'}}';
										$body .= (strpos($doc['variables'], '|') !== false ? '{{ '.str_replace('|', " }}\n    {{ ", $doc['variables']).' }}': $doc['variables'])."\n";
										$body .= '{{ /'. $plugin['slug'].':'.$method .'}}';
									endif;
									$body .= '</code>';
								$body .= '</pre>';

								if (isset($doc['attributes']) and $doc['attributes']):
								$body .= '<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">';
									$body .= '<tbody>';
										$body .= '<tr role="row">';
											$body .= '<th width="60" >Name</th>';
											$body .= '<th>Type</th>';
											$body .= '<th>Flags</th>';
											$body .= '<th>Default</th>';
											$body .= '<th>Required</th>';
										$body .= '</tr>';
										foreach ($doc['attributes'] as $attribute => $details):
											$body .= '<tr role="row">';
												$body .= '<td>'. $attribute .'</td>';
												$body .= '<td>'. (isset($details['type']) ? str_replace('|', ' | ', $details['type']) : '') . '</td>';
												$body .= '<td>'. (isset($details['flags']) ? str_replace('|', ' | ', $details['flags']) : '') . '</td>';
												$body .= '<td>'. ((isset($details['default']) and is_scalar($details['default']) and $details['default'] !== '') ? $details['default'] : lang('global:check-none') ). '</td>';
												$body .= '<td>'. ((isset($details['required']) and $details['required']) ? lang('global:yes') : lang('global:no')) . '</td>';
											$body .= '</tr>';
										endforeach;
									$body .= '</tbody>';
								$body .= '</table>';
								endif;
							$body .= '</div>';
						endforeach;
					endif;
				$body .= '</section>';

				$modal['body'] = $body;

				$modal['footer'] = [
					'close' => array(
						'text' => "global:close",
						'data-dismiss'=> "modal",
						'class' => 'btn btn-dark'
					)
				];

				$result[] = $modal;
			}
		}

		return ['modals' => $result];
	}

}
