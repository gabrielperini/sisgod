<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
		<div class="card card-login card-hidden">
			<div class="card-header card-header-rose text-center">
				<h4 class="card-title"><?php echo lang('user:password_reset_title')?></h4>
			</div>
            <div class="card-body">
				<div class="alert alert-success">
					<span><?php echo lang('user:password_reset_message') ?></span>
				</div>
			</div>
		</div>
	</div>
</div>