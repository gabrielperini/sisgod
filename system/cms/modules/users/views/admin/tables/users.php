<?php if (!empty($users)): ?>
	<table id="tableUsers" class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid" >
		<thead>
			<tr>
				<th width="10"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
				<th><?php echo lang('user:name_label');?></th>
				<th><?php echo lang('user:email_label');?></th>
				<th><?php echo lang('user:group_label');?></th>
				<th><?php echo lang('user:active') ?></th>
				<th><?php echo lang('user:joined_label');?></th>
				<th><?php echo lang('user:last_visit_label');?></th>
				<th width="150">Ações</th>
			</tr>
		</thead>
		<tbody>
			<?php $link_profiles = Settings::get('enable_profiles') ?>
			<?php foreach ($users as $member): ?>
				<tr>
					<td class="align-center"><?php echo form_checkbox('action_to[]', $member->id) ?></td>
					<td>
					<?php if ($link_profiles) : ?>
						<?php echo anchor('admin/users/preview/' . $member->id, $member->display_name, 'target="_blank" class="modal-large"') ?>
					<?php else: ?>
						<?php echo $member->display_name ?>
					<?php endif ?>
					</td>
					<td><?php echo mailto($member->email) ?></td>
					<td data-search="<?=$member->group_id?>" ><?php echo $member->group_name ?></td>
					<td data-search="<?=$member->active?>" ><?php echo $member->active ? lang('global:yes') : lang('global:no')  ?></td>
					<td data-search="<?=$member->created_on?>" ><?php echo format_date($member->created_on) ?></td>
					<td data-search="<?=$member->last_login?>"><?php echo ($member->last_login > 0 ? format_date($member->last_login) : lang('user:never_label')) ?></td>
					<td class="actions">
						<?php echo anchor('admin/users/edit/' . $member->id, lang('global:edit'), array('class'=>'btn-hover-shine btn btn-dark')) ?>
						<?php echo anchor('admin/users/delete/' . $member->id, lang('global:delete'), array('class'=>'btn-hover-shine btn btn-danger')) ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?php endif ?>