<section class="main-card mb-3 card">
	<div class="card-header">
		<h4>
		<?php echo ($this->method === 'create') ? 
			lang('user:add_title') : 
			sprintf(lang('user:edit_title'), $member->username) ?>
		</h4>
	</div>
	<div class="card-header card-header-tab-animation">
		<ul class="nav nav-justified">
			<li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#user-basic-data-tab"><span><?php echo lang('profile_user_basic_data_label') ?></span></a></li>
			<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#user-profile-fields-tab"><span><?php echo lang('user:profile_fields_label') ?></span></a></li>
		</ul>
	</div>

	<?php if ($this->method === 'create'): ?>
		<?php echo form_open_multipart(uri_string(), 'autocomplete="off"') ?>
	<?php else: ?>
		<?php echo form_open_multipart(uri_string()) ?>
		<?php echo form_hidden('row_edit_id', isset($member->row_edit_id) ? $member->row_edit_id : $member->profile_id); ?>
	<?php endif ?>
	<div class="card-body">
		<div class="tab-content">
			<!-- Content tab -->
			<div class="tab-pane active" role="tabpanel" id="user-basic-data-tab">
				<div class="position-relative form-group" >
					<label for="email"><?php echo lang('global:email') ?> <span>*</span></label>
					<?php echo form_input('email', $member->email, 'class=" form-control" id="email"') ?>
				</div>
				
				<div class="position-relative form-group" >
					<label for="username"><?php echo lang('user:username') ?> <span>*</span></label>
					<?php echo form_input('username', $member->username, 'class=" form-control" id="username"') ?>
				</div>

				<div class="position-relative form-group" >
					<label for="group_id"><?php echo lang('user:group_label') ?></label>
					<?php echo form_dropdown('group_id', array(0 => lang('global:select-pick')) + $groups_select, $member->group_id, 'class=" form-control" id="group_id"') ?>
				</div>
				
				<div class="position-relative form-group" >
					<label for="active"><?php echo lang('user:activate_label') ?></label>
					<?php $options = array(0 => lang('user:do_not_activate'), 1 => lang('user:active'), 2 => lang('user:send_activation_email')) ?>
					<?php echo form_dropdown('active', $options, $member->active, 'class=" form-control" id="active"') ?>
				</div>
				<div class="position-relative form-group" >
					<label for="password">
						<?php echo lang('global:password') ?>
						<?php if ($this->method == 'create'): ?> <span>*</span><?php endif ?>
					</label>
					<?php echo form_password('password', '', 'class=" form-control" id="password" autocomplete="off"') ?>
				</div>
				
			</div>
	
			<div class="tab-pane" role="tabpanel" id="user-profile-fields-tab">
				<div class="position-relative form-group" >
					<label for="display_name"><?php echo lang('profile_display_name') ?> <span>*</span></label>
					<?php echo form_input('display_name', $display_name, 'class=" form-control" id="display_name"') ?>
				</div>

				<?php foreach($profile_fields as $field): ?>
				<div class="position-relative form-group" >
					<label for="<?php echo $field['field_slug'] ?>">
						<?php echo (lang($field['field_name'])) ? lang($field['field_name']) : $field['field_name'];  ?>
						<?php if ($field['required']){ ?> <span>*</span><?php } ?>
					</label>
					<?php echo $field['input'] ?>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	
	<div class="card-footer">
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )) ?>
		</div>
	</div>
	<?php echo form_close() ?>
</section>

