<div class="row">
    <div class="label-loading text-center col-12 ">
        <h4>Atualizando Dados</h4>
    </div>
    <div class="col-12 mt-4 text-center percentage-label">
        <h6>0%</h6>
    </div>
    <div class="col-12 mt-1">
        <div class="progress">
            <div class="bar progress-bar progress-bar-animated bg-success progress-bar-striped"></div>
        </div>
    </div>
    <div class="col-12 mt-2 errors-container">
    </div>
</div>
<style>
    @keyframes label {
        0%   {opacity: 1}
        30%  {opacity: 0}
        60% {opacity: 1}
    }
    .label-loading{
        animation-name: label;
        animation-duration: 2.5s;
        animation-iteration-count: infinite;
    }
    
</style>