
<section class="main-card mb-3 card">
	<section class="card-header">
		<h4><?php echo lang('user:list_title') ?></h4>
	</section>

	<?php echo form_open('admin/users/action') ?>
	<div class="card-body">
		<?php template_partial('filters') ?>
		<div id="filter-stage">
			<?php echo $this->load->view('admin/tables/users') ?>
		</div>
	</div>
	<div class="table_action_buttons card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('activate', 'delete') )) ?>
	</div>
	<div class="card-footer">
		<button id="btn-update-sisdm" class="btn-hover-shine btn btn-dark" >Atualizar dados do SISDM</button>
	</div>
	<?php echo form_close() ?>
</section>
<script>
var users = <?=json_encode($users)?>;
</script>
