<fieldset id="filters" class="mb-4">

	<legend><?php echo lang('global:filters') ?></legend>
	<?php echo form_hidden('f_module', $module_details['slug']) ?>
		<ul class="form-row list-unstyled mx-3">
			<li class="col-12 col-md-4">
				<?php echo lang('user:active', 'f_active') ?>
				<?php echo form_dropdown('f_active', array(0 => lang('global:select-all'), 1 => lang('global:yes'), 2 => lang('global:no') ), array(0),'class="form-control" ') ?>
			</li>

			<li class="col-12 col-md-4" >
				<?php echo lang('user:group_label', 'f_group') ?>
				<?php echo form_dropdown('f_group', array(0 => lang('global:select-all')) + $groups_select,[],'class="form-control" ') ?>
			</li>
			
			<li class="col-12 col-md-4" >
				<label for="f_keywords">Nome</label>
				<?php echo form_input('f_keywords','','class="form-control" ') ?>
			</li>
		</ul>
		<li class="list-unstyled mx-3"><button id="cleanUserFilter" class="btn btn-dark" >Limpar Filtros</button></li>
</fieldset>
