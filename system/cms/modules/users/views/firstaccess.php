<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
	<?php echo form_open('',['id' => "firstaccess",'autocomplete'=>"off"]) ?>
		<div class="card card-login card-hidden">
		<div class="card-header card-header-rose text-center">
			<h4 class="card-title">Primeiro Acesso</h4>
		</div>
		<div class="card-body ">
			<?php if (validation_errors()): ?>
            <div class="alert alert-danger">
                <span><?php echo validation_errors();?></span>
            </div>
			<?php endif ?>
			<?php if ($error_string): ?>
            <div class="alert alert-danger">
                <span><?php echo $error_string;?></span>
            </div>
			<?php endif ?>
			<span class="bmd-form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">
						<i class="material-icons">credit_card</i>
						</span>
					</div>
					<input value="<?=$this->input->post('iddm')?>" name="iddm" type="text" class="form-control" placeholder="ID DeMolay...">
                    <input type="text" name="d0ntf1llth1s1n" value=" "  class="default-form" style="display:none" />
				</div>
			</span>
			<span class="bmd-form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">
						<i class="material-icons">lock</i>
						</span>
					</div>
					<input name="password" type="password" class="form-control" placeholder="Crie Uma Senha...">
				</div>
			</span>
			<span class="bmd-form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">
						<i class="material-icons">lock</i>
						</span>
					</div>
					<input name="confirmPassword" type="password" class="form-control" placeholder="Confirme a Senha...">
				</div>
			</span>
		</div>
		<div class="card-footer justify-content-center">
			<button type="submit" class="btn btn-rose btn-link btn-lg">Enviar</a>
		</div>
		</div>
	<?php echo form_close() ?>
	</div>
</div>