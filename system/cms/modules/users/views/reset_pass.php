<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
	<?php echo form_open('forgot', array('id'=>'reset-pass')) ?>
		<div class="card card-login card-hidden">
			<div class="card-header card-header-rose text-center">
				<h4 class="card-title">Esqueci a Senha</h4>
			</div>
			<?php if(!empty($success_string)): ?>
				<div class="card-body ">
					<div class="alert alert-success">
						<span><?php echo $success_string;?></span>
					</div>
				</div>
			<?php else: ?>
			<div class="card-body ">
				<?php if(!empty($error_string)): ?>
				<div class="alert alert-danger">
					<span><?php echo $error_string;?></span>
				</div>
				<?php endif ?>
				<p class="card-description text-center"><?php echo lang('user:reset_instructions') ?></p>
				<span class="bmd-form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
							<i class="material-icons">email</i>
							</span>
						</div>
						<input name="email" type="email" class="form-control" value="<?=$this->input->post('email')?>" placeholder="Email...">
					</div>
				</span>
			</div>
			<div class="card-footer justify-content-center">
				<button type="submit" class="btn btn-rose btn-link btn-lg">Enviar</a>
			</div>
			<?php endif; ?>
		</div>
	<?php echo form_close(); ?>
	</div>
</div>