<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
	<?php echo form_open('login', array('id'=>'login'), array('redirect_to' => $redirect_to)) ?>
		<div class="card card-login card-hidden">
		<div class="card-header card-header-rose text-center">
			<h4 class="card-title">Login</h4>
		</div>
		<div class="card-body ">
			<?php if (validation_errors()): ?>
            <div class="alert alert-danger">
                <span><?php echo validation_errors();?></span>
            </div>
			<?php endif ?>
			<?php if ($activated_email): ?>
			<div class="alert alert-success">
				<span><?php echo $this->lang->line('user:activated_message') ?></span>
			</div>
			<?php endif ?>
			<span class="bmd-form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">
						<i class="material-icons">email</i>
						</span>
					</div>
					<input name="email" type="email" class="form-control" placeholder="Email...">
				</div>
			</span>
			<span class="bmd-form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">
						<i class="material-icons">lock_outline</i>
						</span>
					</div>
					<input name="password" type="password" class="form-control" placeholder="Password...">
				</div>
			</span>
			<div class="form-check mx-4 mt-3">
				<label class="form-check-label">
					<?php echo form_checkbox('remember', '1', false,' class="form-check-input" ') ?>
					<span class="form-check-sign">
						<span class="check"></span>
					</span>
					<?php echo lang('user:remember') ?>
				</label>
			</div>
		</div>
		<div class="card-footer justify-content-center">
			<button type="submit" class="btn btn-rose btn-link btn-lg">Entrar</a>
		</div>
		</div>
	<?php echo form_close() ?>
	</div>
</div>