$(function(){
    var closeDisabled = false;
    $("#btn-update-sisdm").click(async function(event){
        event.preventDefault();
        closeDisabled = true;
        var bar = $('.bar'), percentageLabel = $('.percentage-label'), 
            label = $('.label-loading h4'), percentage = 0,
            percentageby = 100 / users.length;

        bar.width(0);
        percentageLabel.text('0%');
        label.text('Atualizando Dados');

        $('#update-userdata-sisdm').modal('show');
        for (const user of users) {
            label.text('Atualizando Dados de '+user.display_name);
            await $.get('admin/users/update_data_sisdm/'+user.id, function(e){
                percentage += percentageby;
                bar.width(percentage.toFixed(0)+"%");
                percentageLabel.text(percentage.toFixed(0)+'%');
                if(e.success){
                    lazzo.add_notification(user.display_name + "<br/>" + e.message,'success')
                }else{
                    $('<div>',{
                        class: "alert alert-danger fade show",
                        role:"alert",
                        html:user.display_name + "<br/>" + e.error
                    }).appendTo(".errors-container")
                    lazzo.add_notification(user.display_name + "<br/>" + e.error,"error")
                }
            } )
        }
        label.text('Dados Atualizados');
        closeDisabled = false;
        return false;
    })
    
    $('#update-userdata-sisdm').on('hide.bs.modal',function(e){
        if(closeDisabled){
            e.preventDefault();
            return false;
        }else{
            return true;
        }
    });
})