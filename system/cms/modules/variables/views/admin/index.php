<section class="main-card mb-3 card">
	<div class="card-body">
		<?php if ($variables): ?>

			<?php echo form_open('admin/variables/delete') ?>
				<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
					<thead>
					<tr role="row">
						<th width="30"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
						<th width="20%"><?php echo lang('name_label');?></th>
						<th><?php echo lang('variables:data_label');?></th>
						<th width="20%"><?php echo lang('variables:syntax_label');?></th>
						<th width="140">Ações</th>
					</tr>
					</thead>
					<tbody>
						<?php foreach ($variables as $variable): ?>
						<tr role="row">
							<td><?php echo form_checkbox('action_to[]', $variable->id) ?></td>
							<td><?php echo $variable->name;?></td>
							<td><?php echo $variable->data;?></td>
							<td><?php form_input('', printf('{{&nbsp;variables:%s&nbsp;}}', $variable->name));?></td>
							<td class="actions">
								<?php echo anchor('admin/variables/edit/' . $variable->id, lang('buttons:edit'), 'class="btn-hover-shine btn btn-dark "') ?>
								<?php echo anchor('admin/variables/delete/' . $variable->id, lang('buttons:delete'), array('class'=>'btn-hover-shine btn btn-danger')) ?>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>

				<?php $this->load->view('admin/partials/pagination') ?>

				<div class="table_action_buttons">
					<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )) ?>
				</div>
			<?php echo form_close() ?>

		<?php else: ?>
				<div class="no_data"><?php echo lang('variables:no_variables');?></div>
		<?php endif ?>
	</div>
</section>