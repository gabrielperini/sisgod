
<section class="main-card card mb-3">
		<div class="card-header">
			<h4>
				<?php echo ($this->method == 'create') ? 
				lang('variables:create_title'): 
				sprintf(lang('variables:edit_title'), $variable->name);?>
			</h4>
		</div>

		<?php echo form_open($this->uri->uri_string(), 'class="crud" id="variables"') ?>
		<?php if ($this->method == 'edit') echo form_hidden('variable_id', $variable->id) ?>
		
		<div class="card-body">
			<div class="position-relative form-group">
				<label for="name"><?php echo lang('name_label');?> <span>*</span></label>
				<?php echo  form_input('name', $variable->name,'class="form-control"') ?>
			</div>
			
			<div class="position-relative form-group">
				<label for="data"><?php echo lang('variables:data_label');?> <span>*</span></label>
				<?php echo  form_input('data', $variable->data,'class="form-control"') ?>
			</div>
		</div>

		<div class="card-footer">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
		</div>
		
		<?php echo form_close() ?>
		
</section>