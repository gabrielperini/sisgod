<?php if ($this->controller === 'admin_areas' and ! $this->input->is_ajax_request()): ?>

	<section class="main-card mb-3 card">
		<section class="card-header-tab card-header">
			<div class="card-header-title font-size-lg text-capitalize font-weight-normal" >
				<?php echo lang('widgets:areas')?>
			</div>
		</section>
		<div class="card-body" id="widget-areas-list">
		<?php endif ?>
		
			<?php foreach ($widget_areas as $widget_area): ?>
				<section class="widget-area-box" id="area-<?php echo $widget_area->slug ?>" data-id="<?php echo $widget_area->id ?>">
					<header>
						<h3><?php echo $widget_area->title ?></h3>
					</header>
					<div class="widget-area-content accordion-content">
						<div class="area-buttons buttons buttons-small">
									
							<?php echo anchor('admin/'.$this->module_details['slug'].'/areas/edit/'.$widget_area->slug, lang('global:edit'), 'data-toggle="modal" data-target="#edit" class="edit btn btn-dark btn-hover-shine"') ?>
							<button type="submit" name="btnAction" value="delete" class="btn btn-danger btn-hover-shine"><span><?php echo lang('global:delete') ?></span></button>
		
						</div>
		
						<!-- Widget Area Tag -->
						<input type="text" class="font-size-md form-control w-50" value='<?php echo sprintf('{{ widgets:area slug="%s" }}', $widget_area->slug) ?>' />
		
						<!-- Widget Area Instances -->
						<div class="widget-list">
							<?php $this->load->view('admin/instances/index', array('widgets' => $widget_area->widgets)) ?>
							<div style="clear:both"></div>
						</div>
					</div>
				</section>
			<?php endforeach ?>
		
		<?php if ($this->controller === 'admin_areas' and ! $this->input->is_ajax_request()): ?>
		</div>
	</section>
<?php endif ?>