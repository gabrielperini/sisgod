<?php if(!$this->input->is_ajax_request()):?>
<section class="main-card card mb-3">
	<div class="card-header">
		<h4><?php echo lang(sprintf('widgets:%s_area', ($this->method === 'create' ? 'add' : 'edit'))) ?></h4>
	</div>
<?php endif; ?>
	<?php echo form_open(uri_string(), 'class="form_inputs" id="edit-widget"') ?>
	
		<div class="card-body">
			<div class="postion-relative form-group">
				<label for="title"><?php echo lang('widgets:widget_area_title') ?></label> <span>*</span>
				<?php echo form_input('title', $area->title, 'class="form-control new-area-title"') ?>
			</div>
		
			<div class="postion-relative form-group">
				<label for="slug"><?php echo lang('widgets:widget_area_slug') ?></label> <span>*</span>
				<?php echo form_input('slug', $area->slug, 'class="form-control new-area-slug"') ?>
			</div>
		
		</div>
<?php if(!$this->input->is_ajax_request()):?>
		<div class="card-footer">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
		</div>
	
	<?php echo form_close() ?>
	
</section>
<?php else:?>
	<?php echo form_close() ?>
<?php endif;?>