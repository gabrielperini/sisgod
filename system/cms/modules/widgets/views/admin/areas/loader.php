<style>
	.ball-scale-multiple{
		transform: translate(0);
	}
	.ball-scale-multiple div{
		width: 100px;
		height: 100px;
		background-color: #343a40;
		left: 0;
	}
</style>
<div class="loader d-flex justify-content-center" style="height: 100px">
    <div class="ball-scale-multiple" style="width: 100px">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>