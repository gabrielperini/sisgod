<div class="row">
	<div class="col-12 col-md-6 mx-auto">
		<section class="main-card mb-3 card"  id="available-widgets">
			<div class="card-header-tab card-header">
				<div class="card-header-title font-size-lg text-capitalize font-weight-normal" >
					<?php echo lang('widgets:available_title') ?>
				</div>
			</div>
			<div class="card-body">
				<?php if ($available_widgets): ?>
				<ul >
					<?php foreach ($available_widgets as $widget): ?>
					<li id="widget-<?php echo $widget->slug ?>" class="widget-box">
						<p><span><?php echo $widget->title ?></span> <?php echo $widget->description ?></p>
						<div class="widget_info" style="display: none;">
							<p class="author"><?php echo lang('widgets:widget_author') . ': ' . ($widget->website ? anchor($widget->website, $widget->author, array('target' => '_blank')) : $widget->author) ?>
						</div>
					</li>
					<?php endforeach ?>
				</ul>
				<?php else: ?>
				<p><?php echo lang('widgets:no_available_widgets') ?></p>
				<?php endif ?>
			</div>
		</section>
	</div>
	<div class="col-12 col-md-6 mx-auto">
		<section class="main-card mb-3 card" id="widget-areas" >
			<div class="card-header-tab card-header">
				<div class="card-header-title font-size-lg text-capitalize font-weight-normal" >
					<?php echo lang('widgets:widget_area_wrapper') ?>
				</div>
			</div>
			<div class="card-body">
				<?php if ($widget_areas): ?>
				<!-- Available Widget Areas -->
				<div id="widget-areas-list">
					<?php $this->load->view('admin/areas/index', compact('widget_areas')) ?>
				</div>
				<?php else: ?>
					<?php echo anchor('admin/widgets/areas/create', lang('widgets:add_area'), 'class="add create-area"') ?>
				<?php endif ?>
			</div>
		</section>
	</div>
</div>