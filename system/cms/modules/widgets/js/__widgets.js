var draggable = null;
var droppable = null;
$(function(){
    // Create/Edit Areas
    $('.widget-area-content > .buttons > a.edit').click(function(){
        $('#edit .modal-body form').remove();
        $('#edit .modal-body>.loader').removeClass('d-none').addClass('d-flex');
        $.get($(this).attr('href'),function(data){
            $('#edit .modal-body>.loader').addClass('d-none').removeClass('d-flex');
            $('#edit .modal-body').append(data);
        })
    });

    $('#edit .modal-footer #save-widget').click(function () {
        var form = $($(this).attr('data-form'));

        $.post(form.attr('action'),form.serialize(),function(data){
            if(data.status === "success"){
                $('#widget-areas-list').html(data.html);
                lazzo.add_notification('Widget modificado com sucesso!');
            }else{
                lazzo.add_notification('Algo deu errado!','error');
            }
        })
    })

    // drop and drag
    droppable = $('#widget-areas-list .widget-area-box').droppable({
        hoverClass	: 'drop-hover',
        accept		: '.widget-box',
        greedy		: true,
        tolerance	: 'pointer',

        over : function(){
        },
        out : function(){
        },
        drop : function(e, ui){
            $('li.empty-drop-item', this).show().addClass('loading');

            prep_instance_form(ui.draggable, $(".widget-list > ol", this));
        }
    })
    draggable = $('#available-widgets ul li').draggable({
        revert		: 'invalid',
        cursor		: 'move',
        helper		: 'clone',
        cursorAt	: {left: 100},
        refreshPositions: true,

        start : function(e, ui){
            // Setup our new dragging object
            $(this).addClass('widget-drag')
            var width = $(e.currentTarget).width() + 20;
            var height = $(e.currentTarget).height() + 20;
            $('#widget-areas-list .widget-list li').css({width,height})
        },
        stop: function() {
            $(this).removeClass('widget-drag');
            $('#widget-areas-list .widget-list li').removeAttr('style')
        }
    })

})


function prep_instance_form(item, container, action)
{
    action || (action = 'add');

    var key	= (action == 'add') ? $(item).attr('id').replace(/^widget-/, '') : $(container).attr('id').replace(/^instance-/, ''),
        url	= (action == 'add') ? SITE_URL + 'admin/widgets/instances/create/' + key : $(item).attr('href');

    $.get(url, function(response){

        response = '<li id="' + action + '-instance-box" class="box widget-instance no-sortable">' +
                    response + '</li>';

        add_instance_form(response, container, action, key);

    }, 'html');
}

function add_instance_form(item, container, action, key)
{
    var widget = {
        $item: $(item),
        $container: $(container)
    }, method = 'appendTo';

    if (action === 'edit')
    {
        widget.$container.parent().children('li.empty-drop-item.clone').slideUp(50, function(){
            $(this).remove();
        });
        method = 'insertAfter';
    }
    else
    {
        widget.$container.children('li.empty-drop-item').hide().removeClass('loading');
    }

    handle_instance_form(widget.$item[method](widget.$container).slideDown(200, function(){
        draggable.draggable('disable');
    }).children('form'), action, key);
}

function handle_instance_form(form, action, key)
{
    var $form		= $(form),
        $submit		= $form.find('#instance-actions button[value=save]'),
        $cancel		= $form.find('#instance-actions a.cancel')
        area_id		= $form.parents('section.widget-area-box').attr('data-id'),
        url			= $form.attr('action');

    if ($form.data('has_events'))
    {
        return;
    }

    $form.data('has_events', true);

    $cancel.click(function(e){
        e.preventDefault();

        var callback = action === 'edit' ? function(){
            $('li#instance-'+key).slideDown();
        } : false;

        rm_instance_form($form, action, key, callback);
    });

    $submit.click(function(e){
        e.preventDefault();

        var data = $form.serialize() + (action === 'add' ? '&widget_area_id=' + area_id : '');

        $.post(url, data, function(response){
            var callback	= false,
                options		= {};

            if (response.status == 'success')
            {
                callback = function(){
                    rm_instance_form($form, action, key, function(){
                        update_area();
                        var $active = $('#widget-areas-list').find('> section > header:eq('+$('#widget-areas-list')).parent();
                        // var $active = $('#widget-areas-list').find('> section > header:eq('+$('#widget-areas-list').accordion('option', 'active')+')').parent();

                        // if (response.active && response.active !== ('#' + $active.attr('id') + ' header'))
                        // {
                            // $('#widget-areas-list').accordion('option', 'active', response.active);
                        // }
                    });
                }
            }
            else
            {
                options = {
                    ref: $form.children('header:eq(0)')
                }
            }

            // lazzo.add_notification(response.message, options, callback);
            console.log(response.message, 'success', 8000, callback);

        }, 'json');
    });
}

function rm_instance_form(form, action, key, callback)
{
    $(form).parent().slideUp(50, function(){
        $(this).remove()

        draggable.draggable('enable');

        callback && callback();
    });
}

function update_area(){
    var url = SITE_URL + 'admin/widgets/areas';

    $('#widget-areas-list').load(url);
}