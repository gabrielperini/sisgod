jQuery(function($){

	$.extend($.ui.accordion.prototype, {
		refresh: function(){
			this.destroy();
			this.widget().accordion(this.options)
			return this;
		}
	});

	lazzo.cache.widget_forms = {
		add		:{},
		edit	:{}
	}

	lazzo.widgets = {

		$areas		: null,
		$boxes		: null,
		$instances	: null,
		$container	: null,
		selector	: {
			container	: 'null',
			instances	: 'null'
		},

		ui_options: {
			// Widget Areas
			accordion: {
				collapsible	: true,
				header		: '> section > header',
				autoHeight	: false,
				clearStyle	: true
			},
			// Widget Instances List
			sortable: {
				cancel		: '.no-sortable, a, :input, option',
				placeholder	: 'empty-drop-item',

				start: function(){
					lazzo.widgets.$areas.accordion('refresh');
				},

				stop: function(){
					lazzo.widgets.$areas.accordion('refresh');
				},

				update: function(){
					var order = [];
					
					$(this).children('li').each(function(){					
						order.push($(this).attr('id'));
					});

					$.post(SITE_URL + 'widgets/ajax/update_order', { order: order.join(',') });
				}
			},
			// Widget Box
			draggable: {
				revert		: 'invalid',
				cursor		: 'move',
				helper		: 'clone',
				cursorAt	: {left: 100},
				refreshPositions: true,

				start : function(e, ui){
					// Grab our desired width from the widget area list
					var width = lazzo.widgets.$instances.width() - 22;

					// Setup our new dragging object
					$(this).addClass('widget-drag')
					$(ui.helper).css('width', width);
				},
				stop: function() {
					$(this).removeClass('widget-drag');
				}
			},
			// Widget Instances List
			droppable: {
				hoverClass	: 'drop-hover',
				accept		: '.widget-box',
				greedy		: true,
				tolerance	: 'pointer',

				over : function(){
					lazzo.widgets.$areas.accordion('refresh');
				},
				out : function(){
					lazzo.widgets.$areas.accordion('refresh');
				},
				drop : function(e, ui){
					$('li.empty-drop-item', this).show().addClass('loading');

					lazzo.widgets.prep_instance_form(ui.draggable, $(lazzo.widgets.selector.instances, this));

				}
			}
		},

		init: function()
		{
			// Slide toggle for widget codes
			$(".instance-code").click(function(){
				$('#'+$(this).attr('id')+'-wrap').toggle();
			});

			var clickEdit = function(){
				$('#edit .modal-body form').remove();
				$('#edit .modal-body>.loader').removeClass('d-none').addClass('d-flex');
				$.get($(this).attr('href'),function(data){
					$('#edit .modal-body>.loader').addClass('d-none').removeClass('d-flex');
					$('#edit .modal-body').append(data);
				})
			}

			$('.widget-area-content > .buttons > a.edit').click(clickEdit);
		
			$('#edit .modal-footer #save-widget').click(function () {
				var form = $($(this).attr('data-form'));
		
				$.post(form.attr('action'),form.serialize(),function(data){
					console.log(data);
					if(data.status === "success"){
						$('#widget-areas-list').html(data.html);
						lazzo.widgets.init();
						lazzo.add_notification('Widget modificado com sucesso!');
					}else{
						lazzo.add_notification('Algo deu errado!','error');
					}
				})
			})

			// Delete Areas
			$('.widget-area-content > .buttons > button[value=delete]').click( function(e){
				e.preventDefault();

				var $area	= $(this).parents('section.widget-area-box'),
					id		= $area.attr('data-id'),
					url		= SITE_URL + 'admin/widgets/areas/delete/' + id;

				$.get(url, function(response){
					if (response.status == 'success')
					{
						$area.slideUp(function(){
							$(this).remove();
						});
					}
					lazzo.add_notification('Área removida com sucesso');
				}, 'json');
			});

			// Edit Instances
			$('.widget-actions > a.edit').click( function(e){
				e.preventDefault();

				var $anchor = $(this);

				// hide
				$anchor.parents('.widget-instance').slideUp(50, function(){
					// fake loading..
					$(this).siblings('li.empty-drop-item').clone()
						// move
						.insertAfter(this)
						// show
						.show().addClass('loading clone');

						// next step
						lazzo.widgets.prep_instance_form($anchor, this, 'edit');
				});
			});

			// Delete Instances
			$('.widget-actions > button[value=delete]').click( function(e){
				e.preventDefault();

				var $item	= $(this).parents('li.widget-instance'),
					id		= $item.attr('id').replace(/instance-/, ''),
					url		= SITE_URL + 'admin/widgets/instances/delete/' + id;

				$.get(url, function(response){
					if (response.status == 'success')
					{
						$item.slideUp(50, function(){
							$(this).remove();
						});
					}
					lazzo.add_notification('Instância removida com sucesso');
				}, 'json');

				lazzo.widgets.$areas.accordion('refresh');
			});

			$.extend(true, lazzo.widgets, {
				$areas		: $('#widget-areas-list'),
				$boxes		: $('.widget-box'),
				selector	: {
					instances	: '.widget-list > ol',
					container	: '.widget-area-content'
				}
			});

			// Widget Instances Sortable
			lazzo.widgets.$areas.bind('accordioncreate', function(){
				lazzo.widgets.$instances = $(lazzo.widgets.selector.instances).sortable(lazzo.widgets.ui_options.sortable);
			});

			// Widget Instances Droppable
			lazzo.widgets.$areas.bind('accordioncreate', function(){
				lazzo.widgets.$container = $(lazzo.widgets.selector.container).droppable(lazzo.widgets.ui_options.droppable);
				lazzo.widgets.$areas.find('> section > header').droppable({
					accept: '.widget-box',
					addClasses: false,
					greedy: true,
					tolerance: 'pointer',
					over: function(){
						lazzo.widgets.$areas.accordion('option', 'active', this);
					}
				});
			});

			// Widget Areas Accordion
			lazzo.widgets.$areas.accordion(lazzo.widgets.ui_options.accordion);

			// Widget Boxes Draggable
			lazzo.widgets.$boxes.draggable(lazzo.widgets.ui_options.draggable);

			// MANAGE ------------------------------------------------------------------------------

			$('#widgets-list > tbody').sortable({
				handle: 'span.move-handle',
				stop: function(){
					$('#widgets-list > tbody > tr').removeClass('alt');
					$('#widgets-list > tbody > tr:nth-child(even)').addClass('alt');

					var order = [];

					$('#widgets-list > tbody > tr input[name="action_to\[\]"]').each(function(){
						order.push(this.value);
					});

					order = order.join(',');

					$.post(SITE_URL + 'widgets/ajax/update_order/widget', { order: order });
				}
			});

		},

		handle_area_form: function(anchor)
		{
			var $loading	= $('#cboxLoadingOverlay, #cboxLoadingGraphic'),
				$cbox		= $('#cboxLoadedContent'),
				$submit		= $cbox.find('button[value=save]'),
				$cancel		= $cbox.find('button.cancel'),
				$form		= $cbox.find('form'),
				url			= $(anchor).attr('href');

			$cancel.click(function(e){
				e.preventDefault();

				$.colorbox.close();
			});

			$submit.click(function(e){
				e.preventDefault();

				var data = $form.slideUp().serialize();

				$loading.show();

				$.post(url, data, function(response){
					var callback = false;

					if (response.status == 'success')
					{
						if (response.title)
						{
							// editing replace area title
						}

						if (response.html)
						{
							lazzo.widgets.$areas
								.html(response.html)
								.accordion('refresh');

							if (response.active)
							{
								lazzo.widgets.$areas.accordion('option', 'active', response.active);
							}
						}

						url.match(/create/) && $form.get(0).reset();

						callback = function(){
							$.colorbox.resize();
							$.colorbox.close();
						};
					}
					else
					{
						callback = $.colorbox.resize;
					}

					$loading.hide();
					$form.slideDown();

					lazzo.add_notification('Dados salvos');

				}, 'json');
			});

			$.colorbox.resize();
		},

		update_area: function(){
			var url = SITE_URL + 'admin/widgets/areas';

			lazzo.widgets.$areas.load(url, function(){
				$(this).accordion('refresh');
				console.log('oie');
				lazzo.widgets.init();
			});
		},

		prep_instance_form: function(item, container, action)
		{
			action || (action = 'add');

			var key	= (action == 'add') ? $(item).attr('id').replace(/^widget-/, '') : $(container).attr('id').replace(/^instance-/, ''),
				url	= (action == 'add') ? SITE_URL + 'admin/widgets/instances/create/' + key : $(item).attr('href');

			// known? receive the action form
			if (key in lazzo.cache.widget_forms[action])
			{
				// next step
				return lazzo.widgets.add_instance_form(lazzo.cache.widget_forms[action][key], container, action, key);
			}

			$.get(url, function(response){

				response = '<li id="' + action + '-instance-box" class="box widget-instance no-sortable">' +
							response + '</li>';

				// write action form into cache
				lazzo.cache.widget_forms[action][key] = response;

				lazzo.widgets.add_instance_form(response, container, action, key);

			}, 'html');
		},

		add_instance_form: function(item, container, action, key)
		{
			var widget = {
				$item: $(item),
				$container: $(container)
			}, method = 'appendTo';

			if (action === 'edit')
			{
				widget.$container.parent().children('li.empty-drop-item.clone').slideUp(50, function(){
					$(this).remove();
				});
				method = 'insertAfter';
			}
			else
			{
				widget.$container.children('li.empty-drop-item').hide().removeClass('loading');
			}

			lazzo.widgets.handle_instance_form(widget.$item[method](widget.$container).slideDown(200, function(){
				lazzo.widgets.$boxes.draggable('disable');
				lazzo.widgets.$areas.accordion('refresh');
			}).children('form'), action, key);
		},

		handle_instance_form: function(form, action, key)
		{
			var $form		= $(form),
				$submit		= $form.find('#instance-actions button[value=save]'),
				$cancel		= $form.find('#instance-actions a.cancel')
				area_id		= $form.parents('section.widget-area-box').attr('data-id'),
				url			= $form.attr('action');

			if ($form.data('has_events'))
			{
				return;
			}

			$form.data('has_events', true);

			$cancel.click(function(e){
				e.preventDefault();

				var callback = action === 'edit' ? function(){
					$('li#instance-'+key).slideDown(function(){
						lazzo.widgets.$areas.accordion('refresh');
					});
				} : false;

				lazzo.widgets.rm_instance_form($form, action, key, callback);
			});

			$submit.click(function(e){
				e.preventDefault();

				var data = $form.serialize() + (action === 'add' ? '&widget_area_id=' + area_id : '');

				$.post(url, data, function(response){
					var options		= {};

					if (response.status == 'success')
					{
						lazzo.widgets.rm_instance_form($form, action, key, function(){
							lazzo.widgets.update_area();
							var $active = lazzo.widgets.$areas.find('> section > header:eq('+lazzo.widgets.$areas.accordion('option', 'active')+')').parent();

							if (response.active && response.active !== ('#' + $active.attr('id') + ' header'))
							{
								lazzo.widgets.$areas.accordion('option', 'active', response.active);
							}
						});
					}
					else
					{
						options = {
							ref: $form.children('header:eq(0)')
						}
					}

					lazzo.add_notification('Dados Salvos');

				}, 'json');
			});
		},

		rm_instance_form: function(form, action, key, callback)
		{
			$(form).parent().slideUp(50, function(){
				action === 'add'
					? $(this).remove()
					: key
						? lazzo.cache.widget_forms[action][key] = $(this).detach()
						: lazzo.cache.widget_forms[action] = {};

				lazzo.widgets.$boxes.draggable('enable');
				lazzo.widgets.$areas.accordion('refresh');

				callback && callback();
			});
		}

//		,scroll_to: function(ele){
//			$('html, body').animate({
//				scrollTop: $(ele).offset().top
//			}, 1000);
//		}

	};
	
	lazzo.widgets.init();

	// Select code 
	$(".widget-code").focus(function(){$(this).select()});
	$(".widget-code").mouseup(function(e){e.preventDefault();});

});