<div id="files-uploader">
    <div id="file-to-replace">
        <h4><?php echo lang("files:replace_file")?>:<span class="name"></span></h4>
        <span class="alert-warning"><?php echo lang("files:replace_warning")?></span>
    </div>
    <div class="files-uploader-browser">
        <form action="<?php echo site_url('admin/files/upload'); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            <label for="file" class="upload"><?php echo lang("files:uploader")?></label>
            <input type="file" name="file" value="" multiple="multiple" />
            <input type="hidden" name="replace-id" value="" />
        </form>
        <ul id="files-uploader-queue" class="ui-corner-all"></ul>
    </div>
</div>