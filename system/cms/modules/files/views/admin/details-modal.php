<div id="item-details">
		<ul class="list-group">
			<li class="list-group-item"><label><?php echo lang('files:id') ?>:</label> 
				<span class="id"></span>
			</li>
			<li class="list-group-item"><label><?php echo lang('files:name') ?>:</label> 
				<span class="name"></span>
			</li>

		</ul>

		<ul class="meta-data list-group my-2" style="display:none">
			<li class="list-group-item" ><label><?php echo lang('files:slug') ?>:</label> 
				<span class="slug"></span>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:path') ?>:</label> 
				<input readonly="readonly" type="text" class="form-control path"/>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:added') ?>:</label> 
				<span class="added"></span>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:width') ?>:</label> 
				<span class="width"></span>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:height') ?>:</label> 
				<span class="height"></span>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:filename') ?>:</label> 
				<span class="filename"></span>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:filesize') ?>:</label> 
				<span class="filesize"></span>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:download_count') ?>:</label> 
				<span class="download_count"></span>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:location') ?>:</label> 
				<span class="location-static"></span>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:container') ?>:</label> 
				<span class="container-static"></span>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:location') ?>:</label> 
				<?php echo form_dropdown('location', $locations, '', 'class="location form-control"') ?>
			</li>
			<li class="list-group-item" ><label><?php echo lang('files:bucket') ?>:</label> 
				<?php echo form_input('bucket', '', 'class="container amazon-s3 form-control"') ?>
                <div class="input-group-append">
                    <button class="btn btn-secondary container-button"><?php echo lang('files:check_container') ?></button>
                </div>
			</li>
			<li class="list-group-item input-group" ><label><?php echo lang('files:container') ?>:</label> 
				<?php echo form_input('container', '', 'class="container rackspace-cf form-control"') ?>
                <div class="input-group-append">
                    <button class="btn btn-secondary container-button"><?php echo lang('files:check_container') ?></button>
                </div>
			</li>
			<li class="list-group-item" >
				<span class="container-info"></span>
			</li>
		</ul>

		<div class="item-description my-2">
			<div class="form-group position-relative">
                <label><?php echo lang('files:alt_attribute') ?>:</label>
				<input type="text" class="alt_attribute form-control" />
			</div>
			<div class="form-group position-relative">
                <label><?php echo lang('files:keywords') ?>:</label>
				<?php echo form_input('keywords', '', 'id="keyword_input" ') ?>
			</div>
			<div class="form-group position-relative">
                <label><?php echo lang('files:description') ?>:</label> <br />
				<textarea class="description form-control"></textarea>
			</div>
        </div>

	</div>