
<section class="main-card card mb-3">

	<div class="card-header">
		<h4 class="card-header-title" id="file-title">
			<?php echo lang('files:files_title') ?>
		</h4>

		<div class="btn-actions-pane-right" id="file-toolbar">
			<div id="file-buttons">
				<div class="button-menu-source buttons">
					<button class="btn btn-light btn-hover-shine" data-applies-to="pane root-pane"	data-menu="refresh"><?php echo lang('files:refresh') ?></button>
					<button class="btn btn-light btn-hover-shine" data-applies-to="file" data-menu="open-file"><?php echo lang('files:open') ?></button>
					<button class="btn btn-light btn-hover-shine" data-applies-to="folder" data-menu="open"><?php echo lang('files:open') ?></button>
					<button class="btn btn-light btn-hover-shine" data-role="create_folder"	data-applies-to="pane root-pane" data-menu="new-folder"><?php echo lang('files:new_folder') ?></button>
					<button class="btn btn-light btn-hover-shine" data-role="upload" data-applies-to="folder pane" data-menu="upload"><?php echo lang('files:upload') ?></button>
					<!-- <button class="btn btn-light btn-hover-shine" data-role="edit_file" data-applies-to="file" data-menu="rename"><?php echo lang('files:rename') ?></button> -->
					<button class="btn btn-light btn-hover-shine" data-role="edit_folder" data-applies-to="folder" data-menu="rename"><?php echo lang('files:rename') ?></button>
					<button class="btn btn-light btn-hover-shine" data-role="download_file" data-applies-to="file" data-menu="download"><?php echo lang('files:download') ?></button>
					<button class="btn btn-light btn-hover-shine" data-role="synchronize" data-applies-to="folder" data-menu="synchronize"><?php echo lang('files:synchronize') ?></button>
					<button class="btn btn-light btn-hover-shine" data-role="upload delete_file" data-applies-to="file" data-menu="replace"><?php echo lang('files:replace') ?></button>
					<button class="btn btn-light btn-hover-shine" data-role="delete_file" data-applies-to="file" data-menu="delete"><?php echo lang('files:delete') ?></button>
					<button class="btn btn-light btn-hover-shine" data-role="delete_folder" data-applies-to="folder" data-menu="delete"><?php echo lang('files:delete') ?></button>
					<button class="btn btn-light btn-hover-shine" data-applies-to="folder file pane" data-menu="details"><?php echo lang('files:details') ?></button>
				</div>
			</div>
			<input type="text" id="file-search" name="file-search" value="" placeholder="<?php echo lang('files:search_message') ?>"/>
		</div>
	</div>

	<div class="card-body">
		<section class="side">
			<ul id="folders-sidebar" class="list-group">
				<li class="folder places" data-id="0"><a href="#"><?php echo lang('files:places') ?></a></li>
				<?php if ( ! $folders) : ?>
					<li class="no_data"><?php echo lang('files:no_folders_places') ?></li>
				<?php elseif ($folder_tree) : ?>
					<?php echo tree_builder($folder_tree, '<li class="folder" data-id="{{ id }}" data-name="{{ name }}"><div></div><a href="#">{{ name }}</a>{{ children }}</li>') ?>
				<?php endif ?>
			</ul>
		</section>

		<section class="center">

			<?php if ( ! $folders) : ?>
				<div class="no_data"><?php echo lang('files:no_folders') ?></div>
			<?php endif ?>

			<ul class="folders-center pane"></ul>
				
			<ul class="context-menu-source list-group">
				<li class="list-group-item list-group-item-action" data-applies-to="file" data-menu="open-file"><?php echo lang('files:open') ?></li>
				<li class="list-group-item list-group-item-action" data-applies-to="folder" data-menu="open"><?php echo lang('files:open') ?></li>
				<li class="list-group-item list-group-item-action" data-role="create_folder"	data-applies-to="pane root-pane" data-menu="new-folder"><?php echo lang('files:new_folder') ?></li>
				<li class="list-group-item list-group-item-action" data-role="upload" data-applies-to="folder pane" data-menu="upload"><?php echo lang('files:upload') ?></li>
				<!-- <li class="list-group-item list-group-item-action" data-role="edit_file" data-applies-to="file" data-menu="rename"><?php echo lang('files:rename') ?></li> -->
				<li class="list-group-item list-group-item-action" data-role="upload delete_file" data-applies-to="file" data-menu="replace"><?php echo lang('files:replace') ?></li>
				<li class="list-group-item list-group-item-action" data-role="edit_folder"	data-applies-to="folder" data-menu="rename"><?php echo lang('files:rename') ?></li>
				<!-- <li class="list-group-item list-group-item-action" data-applies-to="file" data-menu="edit"><?php echo lang('files:edit') ?></li> -->
				<li class="list-group-item list-group-item-action" data-role="download_file"	data-applies-to="file" data-menu="download"><?php echo lang('files:download') ?></li>
				<li class="list-group-item list-group-item-action" data-role="synchronize"	data-applies-to="folder" data-menu="synchronize"><?php echo lang('files:synchronize') ?></li>
				<li class="list-group-item list-group-item-action" data-role="delete_file"	data-applies-to="file" data-menu="delete"><?php echo lang('files:delete') ?></li>
				<li class="list-group-item list-group-item-action" data-role="delete_folder"	data-applies-to="folder" data-menu="delete"><?php echo lang('files:delete') ?></li>
				<li class="list-group-item list-group-item-action" data-applies-to="folder file pane" data-menu="details"><?php echo lang('files:details') ?></li>
			</ul>
			
		</section>

		<setion class="side sidebar-right" id="file-sidebar">
			<div class="d-flex dropdown-header justify-content-between">
				<h6 class="dropdown-title">Resultados</h6>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">×</span>
				</button>
			</div>
			<ul id="search-results" class="list-group"></ul>
		</setion>
	</div>

	<div class="card-footer justify-content-between">
		<nav class="" id="file-breadcrumbs" aria-label="breadcrumb">
			<ol class="breadcrumb mb-0">
				<li id="crumb-root" class="breadcrumb-item"><a data-id="0" href="#"><?php echo lang('files:places') ?></a></li>
			</ol>
		</nav>
		<h6 id="activity"></h6>
	</div>
	
	
</section>


<!-- modal -->
<div class="d-none">
	<ul>
		<li class="new-folder" data-id="" data-name=""><span class="name-text"><?php echo lang('files:new_folder_name') ?></span></li>
	</ul>
</div>
