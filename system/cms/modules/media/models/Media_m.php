<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Media model
 *
 * @author		Gabriel Perini
 * @package		LazzoCms/modules/models
 */
class Media_m extends MY_Model
{
	protected $_table = 'media';
	protected $_path_upload = 'uploads/default/images/';

	/**
	 * 
	 * get all method
	 * 
	 * @return array
	 * 
	 */
	public function get_all()
	{
		$medias = $this->db->get($this->_table)->result();
		foreach ($medias as $media) {
			$media->thumbtag = $this->get_thumb_tag($media);
			$media->data = $this->get_extra_file_data($media);
		}
		return $medias;
	}

	/**
	 * 
	 * get by id method
	 * 
	 * @return array
	 * 
	 */
	public function get_by_id($id)
	{
		$medias = $this->db->where('id',$id)->get($this->_table)->result();
		foreach ($medias as $media) {
			$media->thumbtag = $this->get_thumb_tag($media);
			$media->data = $this->get_extra_file_data($media);
		}
		return $medias[0];
	}

	/**
	 * 
	 * get all ordered method
	 * 
	 * @return array
	 * 
	 */
	public function get_all_ordered($way)
	{
		if($way === 'name'){
			$this->db->order_by('title');
		}else{
			$this->db->order_by('date',$way);
		}

		return $this->get_all();
	}

	/**
	 * 
	 * get thumb tag method
	 * 
	 * @return html object
	 * 
	 */
	private function get_thumb_tag($media)
	{
		$dt = new Datetime($media->date);
		$tag = '<img src="'.$this->_path_upload . $dt->format('Y/m/') . $media->thumb .'" ';
		$tag .= $media->alt ? 'alt="'.$media->alt.'"': '';
		return $tag .= ' >';
	}

	/**
	 * 
	 * get html tag method
	 * 
	 * @return html object
	 * 
	 */
	private function get_html_tag($media)
	{
		$dt = new Datetime($media->date);

		switch($media->type){
			case 'image':
				$tag = '<img src="'.$this->_path_upload . $dt->format('Y/m/') . $media->file .'" ';
				$tag .= $media->alt ? 'alt="'.$media->alt.'"': '';
				$tag .= ' >';
			break;
			case 'audio':
			case 'video':
				$tag = '<'.$media->type.' controls src="'.$this->_path_upload . $dt->format('Y/m/') . $media->file .'" ';
				$tag .= $media->alt ? 'alt="'.$media->alt.'"': '';
				$tag .= ' >';
			break;
		}
		return $tag;
		
	}

	/**
	 * 
	 * Upload method
	 * 
	 * @return object
	 * 
	 */
	public function upload($name)
	{
		$this->load->library('upload');
		$this->load->library('image_lib');
		
		//get year and month to path name
		$year = date("Y") . '/';
		$month = date("m") . '/';
		
		// set path name and create if not exists 
		//year
		$upload_path = $this->_path_upload.$year;
		if(!is_dir($upload_path)) mkdir($upload_path);
		//month
		$upload_path .= $month;
		if(!is_dir($upload_path)) mkdir($upload_path);
		
		$config['upload_path'] 		= $upload_path;
		$config['allowed_types'] 	= '*';
		$config['remove_spaces']	= TRUE;

		$this->upload->initialize($config);

		if($this->upload->do_upload($name)){
			
			$upload_data = $this->upload->data();
			$type = strstr($upload_data['file_type'],'/',true);
			if($type === 'image'){
				unset($config);

				//temporary png
				$im = new Imagick();
				$im->readImageBlob(file_get_contents($upload_path.$upload_data['file_name']));
				$im->setImageFormat("png24");
				if($_SERVER['SERVER_NAME'] === "localhost"){
					$local = $_SERVER['DOCUMENT_ROOT'] . BASE_URI;
				}
				$fileTemp = $local . $this->_path_upload.$upload_data['file_name']."_temp.png";
				$im->writeImage($fileTemp);
				$im->clear();
				$im->destroy();

				$config['source_image'] = $this->_path_upload.$upload_data['file_name']."_temp.png";
				$config['maintain_ratio'] = true;
				$config['quality'] = '100%';
				$config['height'] = 150;
				$config['width'] = 150;
				$config['new_image'] = $upload_path.$upload_data['raw_name'].'_thumb.png';
	
				if($upload_data['image_width'] > $upload_data['image_height']):
					$config['master_dim'] = 'height';
				elseif($upload_data['image_height'] > $upload_data['image_width']):
					$config['master_dim'] = 'width';
				endif;
	
				$this->image_lib->initialize($config);
				if($this->image_lib->resize()){
					$thumb = $upload_data['raw_name'].'_thumb.png';
				}else{
					$thumb = '../../'.$type.'_default.png';
				}
				$this->image_lib->clear();

				// unlink temp
				unlink($this->_path_upload.$upload_data['file_name']."_temp.png");

			}else{
				$thumb = '../../'.$type.'_default.png';
			}
			
			$insert = (object)[
				'title' => $upload_data['raw_name'],
				'file' => $upload_data['raw_name'].$upload_data['file_ext'],
				'thumb' => $thumb,
				'date' => (new DateTime)->format('Y-m-d H:i:s'),
				'type' => $type,
				'id_users' => $this->current_user->id
			];

			$insert->id = $this->insert($insert);

			$insert->thumbtag = $this->get_thumb_tag($insert);
			$insert->data = $this->get_extra_file_data($insert);
			$insert->errors = $errors;

			return $insert;
		}else{
			return false;
		}
	}

	/**
	 * 
	 * delete image method
	 * 
	 * @return bool
	 * 
	 */
	public function delete_image($id)
	{
		$media = $this->get_by_id($id);
		if($media){
			$dt = new Datetime($media->date);
			$path = $this->_path_upload . $dt->format('Y/m/');

			unlink($path . $media->file);
			if(strpos($media->thumb,"../../") === false){
				unlink($path . $media->thumb);
			}

			$this->delete($id);
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 
	 * insert method
	 * 
	 * @return id
	 * 
	 */
	public function insert($data)
	{
		$this->db->insert($this->_table,$data);
		return $this->db->insert_id();
	}

	/**
	 * 
	 * update method
	 * 
	 * @return this
	 * 
	 */
	public function update($id,$data)
	{
		$this->db->where('id',$id)->update($this->_table,$data);
		return $this;
	}

	/**
	 * 
	 * delete method
	 * 
	 * @return this
	 * 
	 */
	public function delete($id)
	{
		$this->db->where('id',$id)->delete($this->_table);
		return $this;
	}

	/**
	 * 
	 * get langs method
	 * 
	 * @return array
	 * 
	 */
	public function get_langs()
	{
		return 	[
			'media.title' => lang('media:data.title'),
			'media.date' => lang('media:data.date'),
			'media.url' => lang('media:data.url'),
			'media.id_users' => lang('media:data.id_users'),
			'media.alt' => lang('media:data.alt'),

			'media.successMessage' => lang('media:message.success'),
			'media.errorMessage' => lang('media:message.error'),
			'media.missingMessage' => lang('media:message.missing'),
		];
	}

	/**
	 * 
	 * get extra file data method
	 * 
	 * @return array
	 * 
	 */
	private function get_extra_file_data($media)
	{
		$dt = new Datetime($media->date);
		return [
			'id' => $media->id,
			'title' => $media->title,
			'date' => $dt->format('d/m/Y H:i:s'),
			'url' => base_url() . $this->_path_upload . $dt->format('Y/m/') . $media->file,
			'id_users' => $this->ion_auth->get_user($media->id_users)->display_name,
			'alt' => $media->alt,
			'filetag' => $this->get_html_tag($media)
		];
	}
}