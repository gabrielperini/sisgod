<section class="main-card mb-3 card">
    <div class="card-header">
        <h4><?=lang('media:index.title')?></h4>
    </div>
    <div class="card-body wrap-medias row">
        <?php
        foreach($medias as $media){
        ?>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mb-3">
            <div class="media-thumb" data-toggle="modal" data-target="#edit" data-file='<?=json_encode($media->data)?>' >
                <?=$media->thumbtag?>
            </div>
        </div>
        <?php }?>
        <h5 class="no-media text-center w-100 <?=empty($medias) ? '' : 'd-none' ?>"><?=lang('media:index.nomedias')?></h5>
    </div>
    <div class="card-footer"></div>
</section>