<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Media list, upload, edit and delete
 *
 * @package 	LazzoCms/Modules/Media/controllers
 * @author      Gabriel Perini
 * @copyright   Copyright (c) 2021, Gabriel Perini
 */

class Admin extends Admin_Controller
{

	/**
	 * Constructor method
	 * 
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('media_m');
		$this->lang->load('media');
	}

	/**
	 * Index method, lists all medias
	 *
	 * @return void
	 */
	public function index()
	{
		$medias = $this->media_m->get_all_ordered($this->settings->media_ordering);
		$langs = $this->media_m->get_langs();

		$this->template
			->title($this->module_details['name'])
			->append_js('module::media.upload.js')
			->append_css('module::media.css')
			->set_partial('modals','admin/partials/modals',['modals' => [
				[
					'id' => 'add',
					'title' => lang('media:add.title'),
					'size' => 'modal-lg',
					'body' => $this->load->view('admin/modal/add',[],true),
					'footer' => []
				],
				[
					'id' => 'edit',
					'title' => lang('media:edit.title'),
					'size' => 'size-edit',
					'body' => $this->load->view('admin/modal/edit',[],true),
					'footer' => [
						'save' => array(
							'text' => "save_label",
							'id' => 'save-media',
							'data-dismiss'=> "modal",
							'class' => 'btn btn-primary'
						),
						'delete' => array(
							'text' => "global:delete",
							'id' => 'delete-media',
							'data-dismiss'=> "modal",
							'class' => 'btn btn-danger'
						),
						'close' => array(
							'text' => "cancel_label",
							'id' => 'cancel',
							'data-dismiss'=> "modal",
							'class' => 'btn btn-secondary'
						),
					]
				]
			]])
			->set('medias',$medias)
			->set('langs',$langs)
			->build('admin/index');
	}


	/**
	 * Upload method, upload to the server the medias
	 *
	 * @return void
	 */
	public function upload()
	{
		if($_SERVER['REQUEST_METHOD'] === 'POST'){

			if($obj = $this->media_m->upload('file')){
				$this->template
					->build_json([
						'success' => true,
						'file' => $obj
					]);
			}else{
				$this->template
					->build_json([
						'success' => false,
					]);
			}
		}
	}

	/**
	 * 
	 * Edit ALT method
	 * 
	 */
	public function edit_alt($id)
	{
		if($this->input->is_ajax_request()){
			
			$this->template
				->build_json([
					'success' => $this->media_m->update($id,['alt' => $this->input->post('alt')]),
				]);
			
		}
	}

	/**
	 * 
	 * delete media method
	 * 
	 */
	public function delete_image($id)
	{
		if($this->input->is_ajax_request()){
			
			$this->template
				->build_json([
					'success' => $this->media_m->delete_image($id),
				]);
		}
	}

	/**
	 * 
	 * get all medias method
	 * 
	 */
	public function get_media()
	{
		if($this->input->is_ajax_request()){

			$this->template
				->build_json([
					'success' => true,
					'data' => $this->media_m->get_all_ordered($this->settings->media_ordering)
				]);
		}
	}

}