$(function(){
    var csrf_hash_name = $.cookie(lazzo.csrf_cookie_name)

    var dropzone = $('#add .media-upload').uploadFile({
        url: 'admin/media/upload',
        acceptFiles:"audio/*,video/*,image/*",
        formData: {csrf_hash_name},
        onSuccess: (files,data,xhr,pd) => {
            console.log(data.file.errors)
            if($('.media-thumb').length === 0){
                $('.no-media').addClass('d-none')
            }
            var container = $('<div>',{ class: "col-6 col-sm-4 col-md-3 col-lg-2 mb-3" });
            $('<div>',{
                class:"media-thumb" ,
                'data-toggle':"modal",
                'data-target':"#edit",
                'data-file': JSON.stringify(data.file.data),
                html: data.file.thumbtag,
            }).appendTo(container);
            $('.wrap-medias').prepend(container)

            var w = container.find('.media-thumb').width();
            container.find('.media-thumb').height(w).width(w);

            $('.media-thumb').click(mediaThumbClick)

            pd.progressbar.css('background-color','#52e600');
        },
        onError: (files,data,xhr,pd) => {
            pd.progressbar.css('background-color','red');
        },
    });

    $('#add button.close span,#add').click(function(e){
        if (e.target === this) {
            dropzone.reset();
            return true;
        }else{
            return;
        }
    })

    $('.media-thumb').map(function(i,e){
        var w = $(e).width();
        $(e).height(w).width(w);
    })

    var mediaThumb = null;

    
    function mediaThumbClick(){
        mediaThumb = $(this);
        var data = JSON.parse(mediaThumb.attr('data-file'));
        var modal = $('#edit');
        modal.find('.file-container').html(data.filetag);
        delete data.filetag;
        var Inputs = [];
        for (const k in data) {
            if(k !== 'id'){
                var formGroup = $('<div>',{ class: "form-group"});
                var label = $('<label>',{for:k,text:lazzo.lang['media.' + k]});
                var input = $('<input>',{class:"form-control", id:k, value:data[k], type:"text",readonly:(k !== "alt")})
                Inputs.push(formGroup.append(label,input));
            }else{
                var input = $('<input>',{id:k, value:data[k], type:"hidden"})
                Inputs.push(input);
            }
        }
        modal.find('.file-data-container').html(Inputs);
        
        return true;
    }

    $('.media-thumb').click(mediaThumbClick)

    $('#edit button.close span,#edit').click(function(e){
        if (e.target === this) {
            setTimeout(reset_edit_modal,200)
            return true;
        }else{
            return;
        }
    })

    $('#edit #save-media').click(function () {
        var dataForm = $('#edit .file-data-container') 
        var id = dataForm.find('#id').val();
        var alt = dataForm.find('#alt').val();
        if(alt){
            $.blockUI()
            $.post('admin/media/edit_alt/'+id,{
                alt,csrf_hash_name
            },function(data){
                $.unblockUI()
                if(data.success){
                    var dataFile = JSON.parse(mediaThumb.attr('data-file'));
                    dataFile.alt = alt;
                    mediaThumb.attr('data-file',JSON.stringify(dataFile))
                    reset_edit_modal();
                    lazzo.add_notification(lazzo.lang['media.successMessage']);
                }else{
                    lazzo.add_notification(lazzo.lang['media.errorMessage'],'error');
                }
            })
        }else{
            lazzo.add_notification(lazzo.lang['media.missingMessage'],'error');
            return false;
        }
    })

    $('#edit #delete-media').click(function () {
        var dataForm = $('#edit .file-data-container') 
        var id = dataForm.find('#id').val();
        $.blockUI()
        $.get('admin/media/delete_image/'+id,function(data){
            $.unblockUI()
            // console.log(data);
            if(data.success){
                mediaThumb.parent().remove();
                reset_edit_modal();
                lazzo.add_notification(lazzo.lang['media.successMessage']);
                if($('.media-thumb').length === 0){
                    $('.no-media').removeClass('d-none')
                }
            }else{
                lazzo.add_notification(lazzo.lang['media.errorMessage'],'error');
            }
        })
        return true;
    })
});

function reset_edit_modal() {
    var modal = $('#edit');
    mediaThumb = null;
    modal.find('.file-container').html('');
    modal.find('.file-data-container').html('');
}