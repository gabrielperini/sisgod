<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Media Module
 *
 * @author Gabriel Perini
 * @package LazzoCms\Modules
 */
class Module_media extends Module
{
	public $version = '1.0.0';

	public function info()
	{
		$info = array(
			'icon' => 'pe-7s-photo',
			'name' => array(
				'en' => 'Media',
				'br' => 'Mídia',
			),
			'description' => array(
				'en' => 'Allows the users upload images, videos and images',
				'br' => 'Permite os usuários fazerem o upload de imagens, vídeos e audios',
			),
			'frontend' => true,
			'backend' => true,
			'menu' => "data",

			'sections' => null,
			'shortcuts' => array(
				array(
					'name' => 'global:add',
					'uri' => '#',
					'class' => 'upload-image',
					'data-toggle' => 'modal',
					'data-target' => '#add'
				),
			)
		);

		return $info;
	}

	public function install()
	{
		$this->dbforge->drop_table('media',true);

		$tables = array(
			'media' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100),
				'file' => array('type' => 'VARCHAR', 'constraint' => 255),
				'thumb' => array('type' => 'VARCHAR', 'constraint' => 255),
				'date' => array('type' => 'TEXT', 'constraint' => 100),
				'alt' => array('type' => 'TEXT', 'constraint' => 255),
				'type' => array('type' => 'set', 'constraint' => array('video', 'image', 'audio')),
				'id_users' => array('type' => 'INT')
			),
		);

		if ( ! $this->install_tables($tables)) {
			return false;
		}

		// Install settings
		$settings = array(
			array(
				'slug' => 'media_ordering',
				'title' => 'Media List Order',
				'description' => 'Choose the media list order ',
				'type' => 'select',
				'default' => 'DESC',
				'value' => 'DESC',
				'options' => 'ASC=Oldest First|DESC=Newest First|name=Alphabetic',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'media',
				'order' => 0,
			),
		);

		foreach ($settings as $setting)
		{
			if ( ! $this->db->insert('settings', $setting))
			{
				return false;
			}
		}

		return true;
	}

	public function uninstall()
	{
		// This is a core module, lets keep it around.
		return false;
	}

	public function upgrade($old_version)
	{
		return true;
	}

}
