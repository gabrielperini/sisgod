<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['media:index.title'] 						= 'Media List';
$lang['media:index.nomedias'] 					= 'Add some medias!';

$lang['media:add.title'] 						= 'Add File Media';
$lang['media:edit.title'] 						= 'Edit File Media';

$lang['media:data.title'] 						= 'Title';
$lang['media:data.date'] 						= 'Upload Date';
$lang['media:data.url'] 						= 'URL';
$lang['media:data.id_users'] 					= 'Upload By';
$lang['media:data.alt'] 						= 'Alternative Text';

$lang['media:message.success'] 					= 'Success!';
$lang['media:message.error'] 					= 'Something is wrong! Try again';
$lang['media:message.missing'] 					= 'Something is missing!';

/* End of file media_lang.php */