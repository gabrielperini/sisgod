<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['media:index.title'] 						= 'Lista de Mídias';
$lang['media:index.nomedias'] 					= 'Adicione alguma mídia!';

$lang['media:add.title'] 						= 'Adicionar Arquivo de Mídia';
$lang['media:edit.title'] 						= 'Editar Arquivo de Mídia';

$lang['media:data.title'] 						= 'Título';
$lang['media:data.date'] 						= 'Data de Upload';
$lang['media:data.url'] 						= 'URL';
$lang['media:data.id_users'] 					= 'Carregado por';
$lang['media:data.alt'] 						= 'Texto Alternativo';

$lang['media:message.success'] 					= 'Mídia Atualizada';
$lang['media:message.error'] 					= 'Algo deu errado, tente novamente!';
$lang['media:message.missing'] 					= 'Algo está faltando!';

/* End of file media_lang.php */