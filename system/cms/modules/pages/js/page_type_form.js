(function($) {
	$(function(){

		// Generate a slug from the title
		lazzo.generate_slug('input[name="title"]', 'input[name="slug"]', '_');

	});

})(jQuery);