
<section class="main-card card mb-3">
	<div class="card-header">
		<h4>
			<?php echo ($this->method == 'add') ? 
			lang('redirects:add_title') : 
			lang('redirects:edit_title');?>
		</h4>
	</div>
	<?php echo form_open(uri_string()) ?>
		<div class="card-body">
			<div class="position-relative form-group">
				<label for="type"><?php echo lang('redirects:type');?></label>
				<?php echo form_dropdown('type', array('301' => lang('redirects:301'), '302' => lang('redirects:302')), !empty($redirect['type']) ? $redirect['type'] : '302','class="form-control"');?>
			</div>
	
			<div class="position-relative form-group">
				<label for="from"><?php echo lang('redirects:from');?></label>
				<?php echo form_input('from', str_replace('%', '*', $redirect['from']),'class="form-control"');?>
			</div>
	
			<div class="position-relative form-group">
				<label for="to"><?php echo lang('redirects:to');?></label>
				<?php echo form_input('to', $redirect['to'],'class="form-control"');?>
			</div>
		</div>
		<div class="card-footer">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
		</div>
	<?php echo form_close() ?>
</section>