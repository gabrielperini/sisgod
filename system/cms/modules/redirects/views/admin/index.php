<?php if ($redirects): ?>

	<section class="main-card mb-3 card">
		<div class="card-body">

	    <?php echo form_open('admin/redirects/delete') ?>
		<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
		    <thead>
				<tr role="row">
					<th width="15"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th width="25"><?php echo lang('redirects:type');?></th>
					<th width="25%"><?php echo lang('redirects:from');?></th>
					<th><?php echo lang('redirects:to');?></th>
					<th width="200">Ações</th>
				</tr>
		    </thead>
		    <tbody>
			<?php foreach ($redirects as $redirect): ?>
			    <tr role="row">
					<td><?php echo form_checkbox('action_to[]', $redirect->id) ?></td>
					<td><?php echo $redirect->type;?></td>
					<td><?php echo str_replace('%', '*', $redirect->from);?></td>
					<td><?php echo $redirect->to;?></td>
					<td class="align-center">
					<div class="actions">
						<?php echo anchor('admin/redirects/edit/' . $redirect->id, lang('redirects:edit'), 'class="btn-hover-shine btn btn-dark "');?>
						<?php echo anchor('admin/redirects/delete/' . $redirect->id, lang('redirects:delete'), array('class'=>'btn-hover-shine btn btn-danger'));?>
					</div>
					</td>
			    </tr>
			<?php endforeach ?>
		    </tbody>
		</table>
	
		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )) ?>
		</div>
	    <?php echo form_close() ?>
		
		</div>
	</section>

<?php else: ?>
	<section class="main-card mb-3 card">
		<div class="card-body">
			<div class="no_data"><?php echo lang('redirects:no_redirects');?></div>
		</div>
	</section>
<?php endif ?>