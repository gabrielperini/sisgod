
<section class="main-card card mb-3">
	
	<div class="card-header">
		<h4>
			<?php echo ($this->method == 'edit') ? 
			sprintf(lang('keywords:edit_title'), $keyword->name) : 
			lang('keywords:add_title') ?>
		</h4>
	</div>

	<?php echo form_open(uri_string()) ?>

	<div class="card-body">
		<div class="position-relative form-group ">
			<label for="name"><?php echo lang('keywords:name');?> <span>*</span></label>
			<?php echo form_input('name', $keyword->name,'class="form-control"');?>
		</div>
	</div>
		
	<div class="card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
	</div>	
		
	<?php echo form_close();?>

</section>

<script type="text/javascript">
	jQuery(function($) {
		$('form input[name="name"]').keyup($.debounce(100, function(){
			$(this).val( this.value.toLowerCase().replace(',', '') );
		}));
	});
</script>