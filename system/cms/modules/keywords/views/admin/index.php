<section class="main-card mb-3 card">
	<div class="card-body">
		<?php if ($keywords): ?>
			<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
				<thead>
					<tr role="row">
						<th width="40%"><?php echo lang('keywords:name');?></th>
						<th width="200">Ações</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($keywords as $keyword):?>
					<tr role="row">
						<td><?php echo $keyword->name ?></td>
						<td class="actions">
						<?php echo anchor('admin/keywords/edit/'.$keyword->id, lang('global:edit'), 'class="btn-hover-shine btn btn-dark "') ?>
						<?php if ( ! in_array($keyword->name, array('user', 'admin'))): ?>
							<?php echo anchor('admin/keywords/delete/'.$keyword->id, lang('global:delete'), 'class="btn-hover-shine btn btn-danger"') ?>
						<?php endif ?>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		<?php else: ?>
			<div class="no_data"><?php echo lang('keywords:no_keywords');?></div>
		<?php endif;?>
	</div>
</section>