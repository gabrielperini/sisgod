<section class="main-card mb-3 card">
	<div class="card-header">
		<h4>
		<?= ($this->method == 'edit') ? 
			sprintf(lang('groups:edit_title'), $group->name) : lang('groups:add_title') ?>
		</h4>
	</div>
	<?php echo form_open(uri_string()) ?>
		<div class="card-body">
			<div class="position-relative form-group">
				<label for="description"><?php echo lang('groups:name');?> <span>*</span></label>
				<?php echo form_input('description', $group->description,['class' => 'form-control']);?>
			</div>
			
			<div class="position-relative form-group">
				<label for="name"><?php echo lang('groups:short_name');?> <span>*</span></label>
	
				<?php if ( ! in_array($group->name, array('user', 'admin'))): ?>
				<?php echo form_input('name', $group->name,['class' => 'form-control']);?>
				
				<?php else: ?>
				<?php echo form_input('n', $group->name,['class' => 'form-control', 'readonly' => true]);?>
				<?php endif ?>
				
			</div>
		</div>
		<div class="card-footer">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
		</div>
	<?php echo form_close();?>
			
	</div>
</section>

<script type="text/javascript">
	jQuery(function($) {
		$('form input[name="description"]').keyup($.debounce(300, function(){

			var slug = $('input[name="name"]');

			$.post(SITE_URL + 'ajax/url_title', { title : $(this).val() }, function(new_slug){
				slug.val( new_slug );
			});
		}));
	});
</script>