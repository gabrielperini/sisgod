<section class="main-card mb-3 card">
	<div class="card-body">
		<?php if ($groups): ?>
			<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
				<thead>
					<tr role="row">
						<th class="sorting" width="40%"><?php echo lang('groups:name');?></th>
						<th class="sorting"><?php echo lang('groups:short_name');?></th>
						<th width="300">Ações</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($groups as $group):?>
					<tr role="row">
						<td><?php echo $group->description ?></td>
						<td><?php echo $group->name ?></td>
						<td class="actions">
							<?php if ( ! in_array($group->name, array('user', 'admin'))): ?>
								<?php echo anchor('admin/groups/delete/'.$group->id, lang('buttons:delete'), 'class="btn-hover-shine btn btn-danger"') ?>
							<?php endif ?>
							<?php echo anchor('admin/groups/edit/'.$group->id, lang('buttons:edit'), 'class="btn-hover-shine btn btn-dark "') ?>
							<?php if ( $group->name !== 'admin'): ?>
								<?php echo anchor('admin/permissions/group/'.$group->id, lang('permissions:edit').' &rarr;', 'class="btn-hover-shine btn btn-dark "') ?>
							<?php endif ?>
						</td>
					</tr>
				<?php endforeach;?>
				</tbody>
			</table>
		
		<?php else: ?>
			<section class="title">
				<p><?php echo lang('groups:no_groups');?></p>
			</section>
		<?php endif;?>
	</div>
</section>