<section class="main-card mb-3 card">
	<div class="card-body">
		<p><?php echo lang('permissions:introduction') ?></p>
		
		<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
			<thead>
				<tr role="row">
					<th class="sorting"><?php echo lang('permissions:group') ?></th>
					<th>Ações</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($groups as $group): ?>
				<tr role="row">
					<td><?php echo $group->description ?></td>
					<td class="buttons actions">
						<?php if ($admin_group != $group->name):?>
						<?php echo anchor('admin/permissions/group/' . $group->id, lang('permissions:edit'), array('class'=>'btn-hover-shine btn btn-dark ')) ?>
						<?php else: ?>
						<?php echo lang('permissions:admin_has_all_permissions') ?>
						<?php endif ?>
					</td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>

	</div>
</section>