(function($) {

	$(function() {

		$('a.ajax').click(function(){ ajaxForm(this); return false; });

		// Pick a rule type, show the correct field
		$('input[name="link_type"]').on('change', function(){changeRadio(this)}).filter(':checked').change();

		var option = {
			delay: 100,
			disableNesting: 'no-nest',
			forcePlaceholderSize: true,
			handle: 'div',
			helper:	'clone',
			items: 'li',
			opacity: .4,
			placeholder: 'placeholder',
			tabSize: 25,
			listType: 'ul',
			tolerance: 'pointer',
			toleranceElement: '> div',
			update: function() { updateOrder(this) }
		};
		$('.nestedSortable').nestedSortable(option);
		
	});

})(jQuery);

function ajaxForm(item){
	var q = $(item)
	// make sure we load it into the right one
	var id = q.attr('rel');

	// remove all and add for the selected
	if(!q.attr('edit')){
		$('#link-list a').removeClass('selected')
		q.addClass('selected');
	}
	
	if (q.hasClass('add')) {
		// if we're creating a new one remove the selected icon from link in the tree
		$('.group-'+ id +' #link-list a').removeClass('selected');
	}

	$.blockUI()
	// Load the form
	$('div#link-details.group-'+ id +'').load(q.attr('href'), '', function(){
		$.unblockUI()
		// reset all containers
		$('div#link-details:not(.group-'+ id +')').html('');
		// display the create/edit title in the header
		var title = $('#title-value-'+id).html();
		$('section.box .title h4.group-title-'+id).html(title);
		updateEvents();
	});
}

function changeRadio(item){
	$(item).parents('ul').find('#navigation-' + $(item).val())

	// Show only the selected type
	.show().siblings().hide()

	// Reset values when switched
	.find('input:not([value="http://"]), select').val('');

// Trigger default checked
}

function updateEvents() {
	$('a.ajax').on('click', function(){ ajaxForm(this); return false; });
	// Pick a rule type, show the correct field
	$('input[name="link_type"]').on('change',function(){ changeRadio(this); } ).filter(':checked').change();

	try {
        new SlimSelect({
            select: 'select[multiple]',
            closeOnSelect: false,
            allowDeselectOption: true
        })
    }catch(err){}
}

function updateOrder(item){

	var url	= 'admin/navigation/order';

	var order = $(item).nestedSortable('toHierarchy');
	var data = {group: $(item).parents('section.main-card').attr('rel'),order}

	console.log(order);
	$.get(url, data );			
}