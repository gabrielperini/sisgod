
<section class="main-card card mb-3">
	<div class="card-header">
		<h4><?php echo lang('nav:group_create_title');?></h4>
	</div>
	
	<?php echo form_open('admin/navigation/groups/create', 'class="crud"') ?>
		<div class="card-body">
			
			<div class="position-relative form-group">
				<label for="title"><?php echo lang('global:title');?> <span>*</span></label>
				<?php echo form_input('title', $navigation_group['title'], 'class="text form-control"') ?>
			</div>
		
			<div class="position-relative form-group">
				<label for="url"><?php echo lang('global:slug');?> <span>*</span></label>
				<?php echo form_input('abbrev', $navigation_group['abbrev'], 'class="text form-control"') ?>
			</div>

		</div>
		<div class="card-footer">       
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
		</div>
        
    <?php echo form_close() ?>
	
</section>
    
