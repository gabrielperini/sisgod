<?php if ( ! empty($groups)): ?>
	<?php foreach ($groups as $group): ?>

		<section rel="<?php echo $group->id ?>" class="group-<?php echo $group->id ?> main-card mb-3 card">

			<section class="card-header-tab card-header">
					<div class="card-header-title font-size-lg text-capitalize font-weight-normal" >
						<?php echo $group->title;?>
					</div>
					<div class="btn-actions-pane-right">
						<?php echo anchor('admin/navigation/groups/delete/'.$group->id, lang('global:delete'), array('class' => 'tooltip-e btn-hover-shine btn btn-danger mr-1',  'title' => lang('nav:group_delete_confirm'),'data-toggle' => "tooltip", 'data-placement' => "left")) ?>
						<?php echo anchor('admin/navigation/create/'.$group->id, lang('nav:link_create_title'), 'rel="'.$group->id.'" class="btn-hover-shine btn btn-dark ajax"') ?>
					</div>
			</section>

			<?php if ( ! empty($navigation[$group->id])): ?>

			<section class="card-body row">
					<div id="link-list" class="col-12 col-md-6">
						<ul class="list-group list-unstyled ml-3 nestedSortable cursor-move">
							<?php echo tree_builder($navigation[$group->id], '<li id="link_{{ id }}" ><div class="list-group-item py-1 my-1 br-a bg-light" ><a href="admin/navigation/ajax_link_details/{{ id }}" class="text-dark ajax cursor-pointer" rel="'.$group->id.'" alt="{{ id }}">{{ title }}</a></div>{{ children }}</li>') ?>
						</ul>
					</div>

					<div id="link-details" class="col-12 col-md-6 group-<?php echo $group->id ?>">

						<p>
							<?php echo lang('navs.tree_explanation') ?>
						</p>

					</div>
			</section>

			<?php else:?>

			<section class="card-body row">
					
				<p class="col-12 col-md-6"><?php echo lang('nav:group_no_links');?></p>

				<div class="col-12 col-md-6">
					<div id="link-details" class="group-<?php echo $group->id ?>">

						<p>
							<?php echo lang('navs.tree_explanation') ?>
						</p>

					</div>
				</div>
			</section>
			<?php endif ?>

		</section>

	<?php endforeach ?>

<?php else: ?>
	<div class="blank-slate">
		<p><?php echo lang('nav:no_groups');?></p>
	</div>
<?php endif ?>