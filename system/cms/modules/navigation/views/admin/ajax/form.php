<div id="details-container">
	<?php if ($this->method == 'create'): ?>
		<div class="font-size-lg font-weight-bold" id="title-value-<?php echo $link->navigation_group_id ?>">
			<?php echo lang('nav:link_create_title');?>
		</div>
	<?php else: ?>
		<div class="font-size-lg font-weight-bold" id="title-value-<?php echo $link->navigation_group_id ?>">
			<?php echo sprintf(lang('nav:link_edit_title'), $link->title);?>
		</div>
	<?php endif ?>
	
	<?php echo form_open(uri_string(), 'id="nav-' . $this->method . '" class="form_inputs"') ?>
	
		<ul class="list-unstyled">
<?php if ($this->method == 'edit'): ?>
			<?php echo form_hidden('link_id', $link->id) ?>
<?php endif ?>

			<?php echo form_hidden('current_group_id', $link->navigation_group_id) ?>
			
			<li class="position-relative form-group">
				<label class="font-weight-bold" for="title"><?php echo lang('global:title');?> <span>*</span></label>
				<?php echo form_input('title', $link->title, 'maxlength="50" class="form-control"') ?>
			</li>
			
			<?php if ($this->method == 'edit'): ?>
				<li class="position-relative form-group">
					<label class="font-weight-bold" for="navigation_group_id"><?php echo lang('nav:group_label');?></label>
					<?php echo form_dropdown('navigation_group_id', $groups_select, $link->navigation_group_id,"class='form-control'") ?>
				</li>
			<?php else: ?>
				<?php echo form_hidden('navigation_group_id', $link->navigation_group_id) ?>
			<?php endif ?>
	
			<li class="position-relative form-group">
				<label class="font-weight-bold" for="link_type"><?php echo lang('nav:type_label');?></label>
				<span class="spacer-right">
					<?php echo form_radio('link_type', 'url', $link->link_type == 'url') ?> <?php echo lang('nav:url_label');?>
					<?php echo form_radio('link_type', 'uri', $link->link_type == 'uri') ?> <?php echo lang('nav:uri_label');?>
					<?php echo form_radio('link_type', 'module', $link->link_type == 'module') ?> <?php echo lang('nav:module_label');?>
					<?php echo form_radio('link_type', 'page', $link->link_type == 'page') ?> <?php echo lang('nav:page_label');?>
				</span>
			</li>

			<li class="position-relative form-group">
	
				<p style="<?php echo ! empty($link->link_type) ? 'display:none' : '' ?>">
					<?php echo lang('nav:link_type_desc') ?>
				</p>
	
				<div id="navigation-url" class="position-relative form-group" style="<?php echo @$link->link_type == 'url' ? '' : 'display:none' ?>">
					<label class="label" for="url"><?php echo lang('nav:url_label');?></label>
					<input type="text" id="url" class='form-control' name="url" value="<?php echo empty($link->url) ? 'http://' : $link->url ?>" />
				</div>
	
				<div id="navigation-module" class="position-relative form-group" style="<?php echo @$link->link_type == 'module' ? '' : 'display:none' ?>">
					<label class="label" for="module_name"><?php echo lang('nav:module_label');?></label>
					<?php echo form_dropdown('module_name', array(lang('nav:link_module_select_default'))+$modules_select, $link->module_name,"class='form-control'") ?>
				</div>
	
				<div id="navigation-uri" class="position-relative form-group" style="<?php echo @$link->link_type == 'uri' ? '' : 'display:none' ?>">
					<label class="label" for="uri"><?php echo lang('nav:uri_label');?></label>
					<input type="text" id="uri" class='form-control' name="uri" value="<?php echo $link->uri ?>" />
				</div>
	
				<div id="navigation-page" class="position-relative form-group" style="<?php echo @$link->link_type == 'page' ? '' : 'display:none' ?>">
					<label class="label" for="page_id"><?php echo lang('nav:page_label');?></label>
					<select name="page_id" class='form-control'>
						<option value=""><?php echo lang('global:select-pick');?></option>
						<?php echo $tree_select ?>
					</select>
				</div>
			</li>

			<li class="position-relative form-group">
				<label class="font-weight-bold" for="target"><?php echo lang('nav:target_label') ?></label>
				<?php echo form_dropdown('target', array(''=> lang('nav:link_target_self'), '_blank' => lang('nav:link_target_blank')), $link->target,"class='form-control'") ?>
			</li>

			<li class="position-relative form-group">
				<label class="font-weight-bold" for="restricted_to[]"><?php echo lang('nav:restricted_to');?></label>
				<?php echo form_multiselect('restricted_to[]', array(0 => lang('global:select-any')) + $group_options, (strpos($link->restricted_to, ",") ? explode(",", $link->restricted_to) : $link->restricted_to), 'size="'.(($count = count($group_options)) > 1 ? $count : 2).'"') ?>
			</li>
	
			<li class="position-relative form-group">
				<label class="font-weight-bold" for="class"><?php echo lang('nav:class_label') ?></label>
				<?php echo form_input('class', $link->class,"class='form-control'") ?>
			</li>
		</ul>
	
	<?php echo form_close() ?>
	<div class="buttons float-left padding-top">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array(['button'=>'save','form' => 'nav-'.$this->method], 'cancel') )) ?>
	</div>
</div>
