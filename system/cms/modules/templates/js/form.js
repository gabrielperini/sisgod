(function($) {
	$(function(){

		// Generate a slug from the title
		lazzo.generate_slug('input[name="name"]', 'input[name="slug"]');
	
	});

})(jQuery);