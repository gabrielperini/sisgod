
<section class="main-card card mb-3">
	
	<div class="card-header">
		<h4>
			<?= ($this->method == 'edit' and ! empty($email_template)) ? 
			sprintf(lang('templates:edit_title'), $email_template->name):
			lang('templates:create_title') ?>
		</h4>
	</div>

	<?php echo form_open(current_url()) ?>
	
		<div class="card-body">
			
			<?php if ( ! $email_template->is_default): ?>
			<div class="position-relative form-group ">
				<label for="name"><?php echo lang('name_label') ?> <span>*</span></label>
				<?php echo form_input('name', $email_template->name,'class="form-control"') ?>
			</div>
			
			<div  class="position-relative form-group ">
				<label for="slug"><?php echo lang('templates:slug_label') ?> <span>*</span></label>
				<?php echo form_input('slug', $email_template->slug,'class="form-control"') ?>
			</div>
			
			<div class="position-relative form-group ">
				<label for="lang"><?php echo lang('templates:language_label') ?></label>
				<?php echo form_dropdown('lang', $lang_options, array($email_template->lang),'class="form-control"') ?>
			</div>
			
			<div class="position-relative form-group ">
				<label for="description"><?php echo lang('desc_label') ?> <span>*</span></label>
				<?php echo form_input('description', $email_template->description,'class="form-control"') ?>
			</div>
			
			<?php endif ?>
			<div class="position-relative form-group ">
				<label for="subject"><?php echo lang('templates:subject_label') ?> <span>*</span></label>
				<?php echo form_input('subject', $email_template->subject,'class="form-control"') ?>
			</div>
		
			<div class="position-relative form-group ">
				<label for="body"><?php echo lang('templates:body_label') ?> <span>*</span></label>
				<?php echo form_textarea('body', $email_template->body, 'class="templates ckeditor"') ?>
			</div>
			
		</div>

		<div class="card-footer">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
		</div>
				
	<?php echo form_close() ?>
</section>