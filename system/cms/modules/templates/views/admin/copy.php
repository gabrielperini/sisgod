<section class="main-card card mb-3">
	<div class="card-header">
		<h4><?php echo sprintf(lang('templates:clone_title'), $template_name) ?></h4>
	</div>
	<?php echo form_open(current_url()) ?>
	<div class="card-body">
		<div class="position-relative form-group">
			<label for="lang"><?php echo lang('templates:choose_lang_label') ?></label>
			<?php echo form_dropdown('lang', $lang_options, false,'class="form-control"') ?>
		</div>
	</div>
	<div class="card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
	</div>
	<?php echo form_close() ?>
</section>