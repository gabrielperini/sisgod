<?php if(!empty($templates)): ?>

<section class="main-card mb-3 card">
	<section class="card-header">
		<h4><?php echo lang('templates:default_title') ?></h4>
	</section>
	<?php echo form_open('admin/templates/delete') ?>
	<div class="card-body">

	
		<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" >
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('name_label') ?></th>
					<th><?php echo lang('global:description') ?></th>
					<th><?php echo lang('templates:language_label') ?></th>
					<th width="220">Ações</th>
				</tr>
			</thead>
	
			<tbody>
			
			<?php foreach ($templates as $template): ?>
			<?php if($template->is_default): ?>
				<tr role="row">
					<td><?php echo form_checkbox('action_to[]', $template->id);?></td>
					<td><?php echo $template->name ?></td>
					<td><?php echo $template->description ?></td>
					<td><?php echo $template->lang ?></td>
					<td class="actions">
					<div class="buttons buttons-small align-center">
						<?php //echo anchor('admin/templates/preview/' . $template->id, lang('buttons:preview'), 'class="button preview modal"') ?>
						<?php echo anchor('admin/templates/edit/' . $template->id, lang('buttons:edit'), 'class="btn-hover-shine btn btn-dark "') ?>
						<?php echo anchor('admin/templates/create_copy/' . $template->id, lang('buttons:clone'), 'class="btn-hover-shine btn btn-dark "') ?>
					</div>
					</td>
				</tr>
			<?php endif ?>
			<?php endforeach ?>
		</tbody>
		</table>
		
	</div>
	<div class="table_action_buttons card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )) ?>
	</div>
	<?php echo form_close() ?>
</section>

<section class="main-card mb-3 card">
	<section class="card-header">
		<h4><?php echo lang('templates:user_defined_title') ?></h4>
	</section>
	<?php echo form_open('admin/templates/delete') ?>
	<div class="card-body">
		<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" >
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th class="sorting"><?php echo lang('name_label') ?></th>
					<th ><?php echo lang('global:description') ?></th>
					<th><?php echo lang('templates:language_label') ?></th>
					<th width="220"></th>
				</tr>
			</thead>
	
			<tbody>
		
		<?php foreach ($templates as $template): ?>
			<?php if(!$template->is_default): ?>
				<tr role="row">
					<td><?php echo form_checkbox('action_to[]', $template->id);?></td>
					<td><?php echo $template->name ?></td>
					<td><?php echo $template->description ?></td>
					<td><?php echo $template->lang ?></td>
					<td class="actions">
					<div class="buttons buttons-small align-center">
						<?php //echo anchor('admin/templates/preview/' . $template->id, lang('buttons:preview'), 'class="button preview modal"') ?>
						<?php echo anchor('admin/templates/edit/' . $template->id, lang('buttons:edit'), 'class="btn-hover-shine btn btn-dark "') ?>
						<?php echo anchor('admin/templates/delete/' . $template->id, lang('buttons:delete'), 'class="btn-hover-shine btn btn-danger"') ?>
					</div>
					</td>
				</tr>
			<?php endif ?>
		<?php endforeach ?>
		
		
			</tbody>
		</table>
	
	</div>
	<div class="table_action_buttons card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )) ?>
	</div>

	<?php echo form_close() ?>
</section>
	
<?php else: ?>

<section class="main-card mb-3 card">
	<div class="card-body">
		<p><?php echo lang('templates:currently_no_templates') ?></p>
	</div>
</section>

<?php endif ?>