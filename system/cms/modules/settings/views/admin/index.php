<section class="main-card mb-3 card">
	<div class="card-body">
		<?php if ($setting_sections): ?>
			<?php echo form_open('admin/settings/edit');?>
		
				<ul class="nav nav-tabs">
					<?php 
					$secAct = true;
					foreach ($setting_sections as $section_slug => $section_name): ?>
					<li class='nav-item' >
						<a data-toggle="tab" href="#<?php echo $section_slug ?>" class="nav-link show <?=$secAct ? 'active' : null?>" >
							<?php echo $section_name ?>
						</a>
					</li>
					<?php 
					$secAct = false;
					endforeach ?>
				</ul>
	
				<div class="tab-content">
					<?php 
					$tabAct = true;
					foreach ($setting_sections as $section_slug => $section_name): ?>
					<div class="tab-pane show <?=$tabAct ? 'active' : null?>" role="tabpanel" id="<?php echo $section_slug;?>">
						<?php foreach ($settings[$section_slug] as $setting): ?>
							<div id="<?php echo $setting->slug ?>" class="position-relative form-group">
								<label for="<?php echo $setting->slug ?>">
									<?php echo $setting->title ?>
									<?php if($setting->description): echo '<small class="d-block" >'.$setting->description.'</small>'; endif ?>
								</label>
								<?php echo $setting->form_control ?>
							</div>
						<?php endforeach ?>
					</div>
					<?php 
					$tabAct = false;
					endforeach ?>
				</div>
		
				<div class="buttons padding-top">
					<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )) ?>
				</div>
		
			<?php echo form_close() ?>
		<?php else: ?>
			<div>
				<p><?php echo lang('settings:no_settings');?></p>
			</div>
		<?php endif ?>
	</div>
</section>