<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Lazzocms extends Theme
{
    public $name			= 'LazzoCMS Admin';
    public $author			= 'Gabriel Perini';
    public $author_website	= '';
    public $website			= '';
    public $description		= 'Template HTML5 for Admin';
    public $version			= '1.0.0';
    public $type			= 'admin';
	public $options 		= array(
							'fixed-header' => array(
								'title' 		=> 'Cabeçalho Fixo',
								'description'   => 'Faz o Cabeçalho ficar fixo e sempre aparecer',
								'default'       => 'fixed-header',
								'type'          => 'checkbox',
								'options'       => 'fixed-header=Fixo',
								'is_required'   => false
							),
							'fixed-sidebar' => array(
								'title' 		=> 'Barra Lateral Fixo',
								'description'   => 'Faz a Barra Lateral ficar fixa e sempre aparecer',
								'default'       => 'fixed-sidebar',
								'type'          => 'checkbox',
								'options'       => 'fixed-sidebar=Fixo',
								'is_required'   => false
							),
							'color-header' => array(
								'title'         => 'Cor do Cabeçalho',
								'description'   => 'Escolha a cor do cabeçalho',
								'default'       => 'bg-dark header-text-light',
								'type'          => 'select',
								'options'       => "header-text-dark=White|bg-secondary header-text-light=Secondary|bg-success header-text-light=Success|bg-info header-text-light=Info|bg-warning header-text-dark=Warning|bg-danger header-text-light=Danger|bg-light header-text-dark=Light|bg-dark header-text-light=Dark|bg-focus header-text-light=Focus|bg-alternate header-text-light=Alternate|bg-vicious-stance header-text-light=Vicious Stance|bg-midnight-bloom header-text-light=Midnight Bloom|bg-night-sky header-text-light=Night Sky|bg-slick-carbon header-text-light=Slick Carbon|bg-asteroid header-text-light=Asteroid|bg-royal header-text-light=Royal|bg-warm-flame header-text-dark=Warm Flame|bg-night-fade header-text-dark=Night Fade|bg-sunny-morning header-text-dark=Sunny Morning|bg-tempting-azure header-text-dark=Tempting Azure|bg-amy-crisp header-text-dark=Amy Crisp|bg-heavy-rain header-text-dark=Heavy Rain|bg-mean-fruit header-text-dark=Mean Fruit|bg-malibu-beach header-text-light=Malibu Beach|bg-deep-blue header-text-dark=Deep Blue|bg-ripe-malin header-text-light=Ripe Malin|bg-arielle-smile header-text-light=Arielle Smile|bg-plum-plate header-text-light=Plum Plate|bg-happy-fisher header-text-dark=Happy Fisher|bg-happy-itmeo header-text-light=Happy Itmeo|bg-mixed-hopes header-text-light=Mixed Hopes|bg-strong-bliss header-text-light=Strong Bliss|bg-grow-early header-text-light=Grow Early|bg-love-kiss header-text-light=Love Kiss|bg-premium-dark header-text-light=Premium Dark|bg-happy-green header-text-light=Happy Green",
								'is_required'   => true
							),
							'color-sidebar' => array(
								'title'         => 'Cor do Cabeçalho',
								'description'   => 'Escolha a cor do cabeçalho',
								'default'       => 'bg-dark sidebar-text-light',
								'type'          => 'select',
								'options'       => "sidebar-text-dark=White|bg-primary sidebar-text-light=Primary|bg-secondary sidebar-text-light=Secondary|bg-success sidebar-text-light=Success|bg-info sidebar-text-light=Info|bg-warning sidebar-text-dark=Warning|bg-danger sidebar-text-light=Danger|bg-light sidebar-text-dark=Light|bg-dark sidebar-text-light=Dark|bg-focus sidebar-text-light=Focus|bg-alternate sidebar-text-light=Alternate|bg-vicious-stance sidebar-text-light=Vicious Stance|bg-midnight-bloom sidebar-text-light=Midnight Bloom|bg-night-sky sidebar-text-light=Night Sky|bg-slick-carbon sidebar-text-light=Slick Carbon|bg-asteroid sidebar-text-light=Asteroid|bg-royal sidebar-text-light=Royal|bg-warm-flame sidebar-text-dark=Warm Flame|bg-night-fade sidebar-text-dark=Night Fade|bg-sunny-morning sidebar-text-dark=Sunny Morning|bg-tempting-azure sidebar-text-dark=Tempting Azure|bg-amy-crisp sidebar-text-dark=Amy Crisp|bg-heavy-rain sidebar-text-dark=Heavy Rain|bg-mean-fruit sidebar-text-dark=Mean Fruit|bg-malibu-beach sidebar-text-light=Malibu Beach|bg-deep-blue sidebar-text-dark=Deep Blue|bg-ripe-malin sidebar-text-light=Ripe Malin|bg-arielle-smile sidebar-text-light=Arielle Smile|bg-plum-plate sidebar-text-light=Plum Plate|bg-happy-fisher sidebar-text-dark=Happy Fisher|bg-happy-itmeo sidebar-text-light=Happy Itmeo|bg-mixed-hopes sidebar-text-light=Mixed Hopes|bg-strong-bliss sidebar-text-light=Strong Bliss|bg-grow-early sidebar-text-light=Grow Early|bg-love-kiss sidebar-text-light=Love Kiss|bg-premium-dark sidebar-text-light=Premium Dark|bg-happy-green sidebar-text-light=Happy Green",
								'is_required'   => true
							),
	);
                                   
    /**
	 * Run() is triggered when the theme is loaded for use
	 *
	 * This should contain the main logic for the theme.
	 *
	 * @access	public
	 * @return	void
	 */
	public function run()
	{
		
	}
	
}

/* End of file theme.php */