(function($) {
	$(function(){

		$('table tbody').sortable({
			handle: 'td',
			helper: 'clone',
			update: function() {
				order = new Array();
				$('tr', this).each(function(){
					order.push( $(this).find('input[name="action_to[]"]').val() );
				});
				order = order.join(',');
	
				$.ajax({
					dataType: 'text',
					type: 'POST',
					data: 'order='+order+'&offset='+fields_offset+'&csrf_hash_name='+$.cookie(lazzo.csrf_cookie_name),
					url:  SITE_URL+'streams_core/ajax/update_field_order',
					success: function() {
						$('tr').removeClass('alt');
						$('tr:even').addClass('alt');
					}
				});

			},
			
		}).disableSelection();

	});
})(jQuery);
