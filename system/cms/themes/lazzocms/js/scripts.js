// It may already be defined in metadata partial
if (typeof(lazzo) == 'undefined') {
	var lazzo = {};
}

jQuery(function($) {

	// Set up an object for caching things
	lazzo.cache = {
		// set this up for the slug generator
		url_titles	: {}
	}

	// Is Mobile?
	lazzo.is_mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);

	/**
	 * Overload the json converter to avoid error when json is null or empty.
	 */
	$.ajaxSetup({
		converters: {
			'text json': function(text) {
				var json = $.parseJSON(text);
				if (!$.ajaxSettings.allowEmpty && (json == null || $.isEmptyObject(json)))
				{
					$.error('The server is not responding correctly, please try again later.');
				}
				return json;
			}
		},
		data: {
			csrf_hash_name: $.cookie(lazzo.csrf_cookie_name)
		}
	});
	

	/**
	 * This initializes all JS goodness
	 */
	lazzo.init = function() {

	};

	lazzo.clear_notifications = function()
	{
		$('#toast-container').html('');

		return lazzo;
	};

	lazzo.add_notification = function(menssage, type = 'success',time = 8000, callback = function(){} ) {
		var containerChildren = $('#toast-container').children()
		if(containerChildren.length >= 5){
			var first = containerChildren.filter( ":first:not(.deleting)" )
			first.addClass('deleting').animate({
				opacity: 0
			},400,'swing',() => {first.remove()} )
		}

		var toast = $('<div>',{
			class: "toast toast-" + type,
			'aria-live': "polite",
			style: 'opacity: 0'
		}).append($('<div>',{
			class:"toast-message",
			html: menssage
		}))
	
		toast.appendTo('#toast-container').animate({
			opacity: 1
		},300);
		setTimeout(() => toast.animate({
			opacity: 0
		},400,'swing',() => toast.remove() ),time);

		callback();

		return lazzo;
	};

	// Used by Pages and Navigation and is available for third-party add-ons.
	// Module must load jquery/jquery.ui.nestedSortable.js and jquery/jquery.cooki.js
	lazzo.sort_tree = function($item_list, $url, $cookie, data_callback, post_sort_callback, sortable_opts)
	{
		// set options or create a empty object to merge with defaults
		sortable_opts = sortable_opts || {};
		
		// collapse all ordered lists but the top level
		$item_list.find('ul').children().hide();

		// this gets ran again after drop
		var refresh_tree = function() {

			// add the minus icon to all parent items that now have visible children
			$item_list.find('li:has(li:visible)').removeClass().addClass('minus');

			// add the plus icon to all parent items with hidden children
			$item_list.find('li:has(li:hidden)').removeClass().addClass('plus');
			
			// Remove any empty ul elements
			$('.plus, .minus').find('ul').not(':has(li)').remove();
			
			// remove the class if the child was removed
			$item_list.find("li:not(:has(ul li))").removeClass();

			// call the post sort callback
			post_sort_callback && post_sort_callback();
		}
		refresh_tree();

		// set the icons properly on parents restored from cookie
		$($.cookie($cookie)).has('ul').toggleClass('minus plus');

		// show the parents that were open on last visit
		$($.cookie($cookie)).children('ul').children().show();

		// show/hide the children when clicking on an <li>
		$item_list.find('li').live('click', function()
		{
			$(this).children('ul').children().slideToggle('fast');

			$(this).has('ul').toggleClass('minus plus');

			var items = [];

			// get all of the open parents
			$item_list.find('li.minus:visible').each(function(){ items.push('#' + this.id) });

			// save open parents in the cookie
			$.cookie($cookie, items.join(', '), { expires: 1 });

			 return false;
		});
		
		// Defaults for nestedSortable
		var default_opts = {
			delay: 100,
			disableNesting: 'no-nest',
			forcePlaceholderSize: true,
			handle: 'div',
			helper:	'clone',
			items: 'li',
			opacity: .4,
			placeholder: 'placeholder',
			tabSize: 25,
			listType: 'ul',
			tolerance: 'pointer',
			toleranceElement: '> div',
			update: function(event, ui) {

				post = {};
				// create the array using the toHierarchy method
				post.order = $item_list.nestedSortable('toHierarchy');

				// pass to third-party devs and let them return data to send along
				if (data_callback) {
					post.data = data_callback(event, ui);
				}

				// Refresh UI (no more timeout needed)
				refresh_tree();

				$.post(SITE_URL + $url, post );
			}
		};

		// init nestedSortable with options
		$item_list.nestedSortable($.extend({}, default_opts, sortable_opts));
	}

	// Create a clean slug from whatever garbage is in the title field
	lazzo.generate_slug = function(input_form, output_form, space_character, disallow_dashes)
	{
		space_character = space_character || '-';

		$(input_form).slugify({ slug: output_form, type: space_character });
	}

	$(document).ajaxError(function(e, jqxhr, settings, exception) {
		$.unblockUI()
		if (exception != 'abort' && exception.length > 0) {
			lazzo.add_notification(exception,'error');
		}
	});

	$(document).ready(function() {
		lazzo.init();
	});

	// Title toggle
	$('a.toggle').click(function() {
	   $(this).parent().next('.item').slideToggle(500);
	});

	// Draggable / Droppable
	$("#sortable").sortable({
		placeholder : 'dropzone',
	    handle : '.draggable',
	    update : function () {
	      var order = $('#sortable').sortable('serialize');
	    }
	});


	//functions for codemirror
	$('.html_editor').each(function() {
		CodeMirror.fromTextArea(this, {
		    mode: 'text/html',
		    tabMode: 'indent',
			height : '500px',
			width : '500px',
		});
	});

	$('.css_editor').each(function() {
		CodeMirror.fromTextArea(this, {
		    mode: 'css',
		    tabMode: 'indent',
			height : '500px',
			width : '500px',
		});
	});

	$('.js_editor').each(function() {
		CodeMirror.fromTextArea(this, {
		    mode: 'javascript',
		    tabMode: 'indent',
			height : '500px',
			width : '500px',
		});
	});
});
