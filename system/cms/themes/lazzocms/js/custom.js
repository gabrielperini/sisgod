const Tables = [];
const Sortables = [];
const NestedSortables = [];


$(function(){ 

    try {
        new SlimSelect({
            select: 'select[multiple]',
            closeOnSelect: false,
            allowDeselectOption: true
        })
    }catch(err){}

    setTimeout(function(){
        $('.mm-collapse a.mm-active').parents('.mm-collapse').addClass('mm-show').parent('li').addClass('mm-active');
        // console.log($('.mm-collapse'));
    },200);

    $.blockUI.defaults = {
        message: $('.loader-blockui')
    }

    $('textarea.ckeditor').map(function(a){
        var name = $(a).attr('name');
        if(name){
            CKEDITOR.replace(name);
        }
    })

    if($('table.table').length){
        $('table.table').map(function(i,e){
            var option = {};
            var element = $(e);
            if(element.hasClass('no-search')) option['searching'] = false;
            if(element.hasClass('no-ordering')) option['ordering'] = false;
            if(element.hasClass('no-paging')) option['paging'] = false;
            if(element.attr('initComplete')) option['initComplete'] = eval(element.attr('initComplete'));
            option['scrollX'] = true;
            Tables.push(element.DataTable(option));
        })
    }

    $('[name=btnAction][value=delete]').click(function () {
        return confirm(lazzo.lang.dialog_message);
    })

    if($('.sortable').length){
        $('.sortable').map(function(i,e){
            var option = {};
            var element = $(e);
            Sortables.push(element.sortable(option));
        })
    }

    if($('input[type=checkbox][name="action_to_all"]')){
        $('input[type=checkbox][name="action_to_all"]').change(function(){
            var check = $(this).is(":checked");
            $(this).parents('table').find('tbody input[type=checkbox]').prop('checked',check)
        })
    }

    if($('#filters').length){
        $('#filters select[name=f_active]').change(function(){ filterUser(4,this) });
        $('#filters select[name=f_group]').change(function(){ filterUser(3,this) });
        $('#filters input').keyup(function(){ filterUser(1,this) });

        $("#cleanUserFilter").click(function(){
            Tables[0].columns()
                .search( '' )
                .draw();

            $('#filters select').val('0')
            $('#filters input').val('')
        });
    }

    /////dates pickers
    $('input[data-toggle="datepicker"]').attr('type','date');
    $('input[data-toggle="timepicker"]').attr('type','time');
    $('input[data-toggle="datetimepicker"]').attr('type','datetime-local');
    

    //// file custom change text label
    $('.custom-file-input').change(function(e){
        if(e.target.value.slice(12)){
            $(this).next().text(e.target.value.slice(12));
        }else{
            $(this).next().text("Selecione Algum Arquivo");
        }
    })

    function mediathumb(media,input) {
        var container = $('<div>',{class:"col-6 col-sm-4 col-md-3 col-lg-2 mb-3"})
        var media = $('<div>',{class: "media-thumb"})
                    .append(media.thumbtag)
                    .click(function(){
                        input.value(media.data.id)
                        .parent().append($(media.data.filetag))
                    })
                    .appendTo(container);
        return container;
    }

    //// MIDIA MODAL
    $('[data-toggle=modal][data-target="#media-modal-choose"]').click(function(){
        var input = $(this).siblings('input[type=hidden]')
    })

    $('#media-modal-choose').on('show.bs.modal',function(){
        $.get('admin/media/get_media',
            {csrf_hash_name: $.cookie(lazzo.csrf_cookie_name)},
            function(data){
                if(data.success){
                    var medias = data.data;
                    var html = medias.map(function(e){
                        return mediathumb(e);
                    })
                    var body = $('#media-modal-choose .modal-body');
                    body.html(html);
    
                    var w = body.find('.media-thumb').width();
                    body.find('.media-thumb').height(w).width(w);
    
                }
            }
        )
    })
});


filterUser = (column,obj) => {
    Tables[0].columns( column )
        .search( $(obj).val() !== '0' ? $(obj).val() : '' )
        .draw();
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }