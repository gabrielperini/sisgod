<?php
if($dirInstaler){
?>
<div class="alert alert-warning fade show d-flex align-items-center justify-content-between" ><?=$dirInstaler?></div>
<?php } ?>

<script type="text/javascript">
    $(function ($) {

        $('#remove_installer_directory').on('click', function (e) {
            e.preventDefault();
            var $parent = $(this).parent();
            $.get(SITE_URL + 'admin/remove_installer_directory', function (data) {
                $parent.removeClass('alert-warning').addClass(data.status).html(data.message);
            });
        });
    });
</script>