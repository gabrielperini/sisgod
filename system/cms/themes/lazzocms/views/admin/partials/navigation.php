<?php
$path = substr($_SERVER['REDIRECT_QUERY_STRING'],1);
$path = $path ? $path : '/';
?>
<li>
    <a class='<?=($path === 'admin') ? 'mm-active' : '' ;?>' href="<?=site_url('admin')?>">
        <i class="metismenu-icon pe-7s-home"></i>
        Painel
    </a>
</li>
<?php

foreach ($menu_items as $key => $menu_item)
{
    if ($menu_item['itens'])
    {
        if($menu_item['section']){
            echo '<li class="app-sidebar__heading">'.$key.'</li>';
        }else{

            echo '<li><a href="'.current_url().'#">
                <i class="metismenu-icon '.$menu_item['icon'].'"></i>'
                .lang_label($key).
                '<i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a><ul>';
        }
        

        foreach ($menu_item['itens'] as $lang_key => $data)
        {
            $uri = $data['uri'];
            $icon = $data['icon'];
            $active = ($path === $uri) ? 'mm-active' : '' ;
            echo '<li><a class="'.$active.'" href="'.site_url($uri).'"><i class="metismenu-icon '.$icon.'"></i>'.lang_label($lang_key).'</a></li>';
        }

        if(!$menu_item['section']) echo '</ul></li>';

    }
    else
    {
        if($menu_item['section']){
            echo '<li class="app-sidebar__heading">'.$key.'</li>';
        }else{
            $active = ($path === $menu_item['uri']) ? 'mm-active' : '' ;
            echo '<li><a class="'.$active.'" href="'.site_url($menu_item['uri']).'">
                    <i class="metismenu-icon '.$menu_item['icon'].'"></i>'.lang_label($key).'</a></li>';
        }
    }

}

?>