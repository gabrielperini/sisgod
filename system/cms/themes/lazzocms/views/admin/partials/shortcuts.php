<?php if ( ! empty($module_details['sections'][$active_section]['shortcuts']) ||  ! empty($module_details['shortcuts'])): ?>
<div class="page-title-actions">
	<?php if ( ! empty($module_details['sections'][$active_section]['shortcuts'])): ?>
		<?php foreach ($module_details['sections'][$active_section]['shortcuts'] as $shortcut):
			$name 	= $shortcut['name'];
			$uri	= $shortcut['uri'];
			unset($shortcut['name']);
			unset($shortcut['uri']); ?>

			<a <?php foreach ($shortcut as $attr => $value) echo $attr.'="'.$value.'"';?> href="<?=site_url($uri)?>">
				<button type="button" class="btn-shadow mr-3 btn btn-dark">
					<i class="fa fa-plus"></i>
					<?=lang($name)?>
				</button>
			</a>
			
		<?php endforeach; ?>
	<?php endif; ?>
	
	<?php if ( ! empty($module_details['shortcuts'])): ?>
		<?php foreach ($module_details['shortcuts'] as $shortcut):
			$name 	= $shortcut['name'];
			$uri	= $shortcut['uri'];
			unset($shortcut['name']);
			unset($shortcut['uri']); ?>

			<a <?php foreach ($shortcut as $attr => $value) echo $attr.'="'.$value.'"';?> href="<?=site_url($uri)?>">
				<button type="button" class="btn-shadow mr-3 btn btn-dark">
					<i class="fa fa-plus"></i>
					<?=lang($name)?>
				</button>
			</a>
		
		<?php endforeach; ?>
	<?php endif; ?>
</div>
<?php endif; ?>
            
        