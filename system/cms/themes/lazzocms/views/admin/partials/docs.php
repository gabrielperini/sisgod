<?php

Asset::css(array(
	'main.css',
	'custom.css',
	'slimselect.css',
	'uploadfile.css',
));

Asset::js('jquery/jquery.js');
Asset::js_inline('jQuery.noConflict();');
Asset::js('jquery/jquery.cooki.js');
Asset::js('jquery/jquery.slugify.js');
Asset::js('ckeditor/ckeditor.js');

Asset::js(array('codemirror/codemirror.js',
'codemirror/mode/css/css.js',
'codemirror/mode/htmlmixed/htmlmixed.js',
'codemirror/mode/javascript/javascript.js',
'codemirror/mode/markdown/markdown.js',
));

Asset::js('scripts.js');

Asset::js(array(
	'jquery/jquery.ui.js',
	'jquery/jquery.blockUI.js',
	'jquery/jquery.uploadfile.min.js',
	// 'jquery/jquery.touch-dnd.js',
	'main.js',
	'bootstrap.min.js',
	'plugins.js',
	'tableJs.js',
	'tableJSBootstrap.js',
	'slimselect.js',
	'nestedSort.js',
	'custom.js',
),false,'footer');
echo Asset::render('global');
echo $template['metadata'];
?>