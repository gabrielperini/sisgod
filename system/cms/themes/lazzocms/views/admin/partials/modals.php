<?php foreach ($modals as $key => $m) {?>
<div class="modal fade" id="<?=$m['id']?>" role="dialog" aria-hidden="true">
    <div class="modal-dialog <?=$m['size']?>" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><?=$m['title']?></h3>
                <?php if(!$m['noCloseBtn']){?>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">×</span>
                    </button>
                <?php }?>
            </div>
            <div class="modal-body">
                <?=$m['body']?>
            </div>
            <div class="modal-footer">
                <?php
                foreach ($m['footer'] as $b) {
                    $button = '';
                    $text = $b['text'];
                    unset($b['text']);
                    $props = '';
                    foreach ($b as $prop => $value) {
                        $props .= ' '.$prop.'="'. $value .'" ';
                    }
                    if(is_array($b)){
                        $button .= "<button $props >\n";
                        $button .= lang($text)."\n";
                        $button .= "</button>\n";
                    }
                    echo $button;
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>