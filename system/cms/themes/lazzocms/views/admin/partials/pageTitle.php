<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="<?=$module_details['icon'] ? $module_details['icon'] : 'pe-7s-home' ?> icon-gradient bg-dark">
                </i>
            </div>
            <div><?php echo $module_details['name'] ? $module_details['name'] : lang('global:dashboard') ?>
                <div class="page-title-subheading">
                    <?php echo $module_details['description'] ? $module_details['description'] : '' ?>
                </div>
            </div>
        </div>
        <?php file_partial('shortcuts') ?>
    </div>
</div>