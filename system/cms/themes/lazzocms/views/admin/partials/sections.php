<ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
	<?php foreach ($module_details['sections'] as $name => $section): ?>
	<?php if(isset($section['name']) && isset($section['uri'])): ?>
	<li class="nav-item">
		<?php echo anchor($section['uri'], 
			'<span>' . (lang($section['name']) ? lang($section['name']) : ucfirst($section['titulo'])) . '</span>', 'class="nav-link '. (($name === $active_section) ? 'active"' : '"' )
		); ?>
	</li>
	<?php endif; ?>
	<?php endforeach; ?>
</ul>