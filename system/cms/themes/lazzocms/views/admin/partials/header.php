<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Lazzo CMS</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">
    <?php
    $uriApp = substr(APPPATH_URI, 1 , -1) . '/';
    ?>
    <script type="text/javascript">
        lazzo = { 'lang' : <?php echo is_array($langs) ? json_encode($langs) : '{}' ?> };
        const APPPATH_URI					= "<?php echo $uriApp;?>";
        const SITE_URL					= "<?php echo rtrim(site_url(), '/').'/';?>";
        const BASE_URL					= "<?php echo BASE_URL;?>";
        const BASE_URI					= "<?php echo BASE_URI;?>";
        const UPLOAD_PATH					= "<?php echo UPLOAD_PATH;?>";
        const DEFAULT_TITLE				= "<?php echo addslashes($this->settings->site_name); ?>";
        lazzo.admin_theme_url			= "<?php echo BASE_URL . $this->admin_theme->path; ?>";
        lazzo.apppath_uri				= "<?php echo $uriApp; ?>";
        lazzo.base_uri					= "<?php echo BASE_URI; ?>";
        lazzo.lang.remove				= "<?php echo lang('global:remove'); ?>";
        lazzo.lang.dialog_message 		= "<?php echo lang('global:dialog:delete_message'); ?>";
        lazzo.csrf_cookie_name			= "<?php echo config_item('cookie_prefix').config_item('csrf_cookie_name'); ?>";
    </script>

    <base href="<?php echo base_url(); ?>" />
    <?php
    file_partial('docs');
    ?>

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow <?=$this->theme_options->{'fixed-header'}?> <?=$this->theme_options->{'fixed-sidebar'}?>">
        <div class="app-header header-shadow <?=$this->theme_options->{'color-header'}?>">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                            data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button"
                        class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="app-header__content">
                <div class="app-header-right">
                    <div class="header-btn-lg px-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                            class="p-0 btn">
                                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true"
                                            class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                            <div class="dropdown-menu-header">
                                                <div class="dropdown-menu-header-inner bg-dark">
                                                    <div class="menu-header-image opacity-2"
                                                        style="background-image: url('<?=base_url()?>system/cms/themes/lazzocms/img/dropdown-header/city3.jpg');">
                                                    </div>
                                                    <div class="menu-header-content text-left">
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left">
                                                                    <div class="widget-heading"><?=$this->current_user->first_name .' '. $this->current_user->last_name ?></div>
                                                                </div>
                                                                <div class="widget-content-right mr-2">
                                                                    <a href="admin/logout">
                                                                        <button class="btn-pill btn-shadow btn-shine btn btn-light">
                                                                            Sair
                                                                        </button>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ps scrollbar-container">
                                                <ul class="nav flex-column">
                                                    <li class="nav-item-header nav-item">Minha Conta</li>
                                                    <li class="nav-item">
                                                        <a href="javascript:void(0);" class="nav-link">Dados</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="javascript:void(0);" class="nav-link">Mudar Senha</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content-left  ml-3 header-user-info">
                                    <div class="widget-heading">
                                        <?=$this->current_user->first_name .' '. $this->current_user->last_name ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="app-main">
            <div class="app-sidebar sidebar-shadow <?=$this->theme_options->{'color-sidebar'}?>">
                <div class="app-header__logo">
                    <div class="logo-src"></div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                                data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button"
                            class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">
                            <li class="app-sidebar__heading">Menu</li>
                            <?php file_partial('navigation') ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="app-main__outer">
                <div class="app-main__inner">
                <?php file_partial('pageTitle') ?>
                <?php if ( ! empty($module_details['sections'])) file_partial('sections') ?>