<script>
$(function(){
<?php if ($this->session->flashdata('error')): ?>
	lazzo.add_notification('<?php echo $this->session->flashdata('error'); ?>','error')
<?php endif; ?>

<?php if (validation_errors()): ?>
	lazzo.add_notification('<?php echo validation_errors(); ?>','error')
<?php endif; ?>

<?php if ( ! empty($messages['error'])): ?>
	lazzo.add_notification('<?php echo $messages['error']; ?>','error')
<?php endif; ?>

<?php if ($this->session->flashdata('notice')): ?>
	lazzo.add_notification('<?php echo $this->session->flashdata('notice');?>','warning')
<?php endif; ?>

<?php if ( ! empty($messages['notice'])): ?>
	lazzo.add_notification('<?php echo $messages['notice']; ?>','warning')
<?php endif; ?>

<?php if ($this->session->flashdata('success')): ?>
	lazzo.add_notification('<?php echo $this->session->flashdata('success'); ?>')
<?php endif; ?>

<?php if ( ! empty($messages['success'])): ?>
	lazzo.add_notification('<?php echo $messages['success']; ?>')
<?php endif; ?>
})
</script>
<?php 
	unset($messages,$_SESSION['error'],$_SESSION['notice'],$_SESSION['success']);
	/**
	 * Admin Notification Event
	 */
	Events::trigger('admin_notification');
	
?>