<div class="modal fade" id="media-modal-choose" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><?=lang('media-modal:title')?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="justify-content-around modal-body row">
                <div class="loader">
                    <div class="line-scale-pulse-out">
                        <div class="<?=$this->theme_options->{'color-header'}?>"></div>
                        <div class="<?=$this->theme_options->{'color-header'}?>"></div>
                        <div class="<?=$this->theme_options->{'color-header'}?>"></div>
                        <div class="<?=$this->theme_options->{'color-header'}?>"></div>
                        <div class="<?=$this->theme_options->{'color-header'}?>"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>