<?php if (isset($buttons) && is_array($buttons)): ?>

	<?php foreach ($buttons as $key => $button): ?>
		<?php
		/**
		 * @var		$extra	array associative
		 * @since	1.2.0-beta2
		 */ ?>
		<?php $extra	= NULL; ?>
		<?php 
		
		$button	= ! is_numeric($key) && ($extra = $button) ? $key : $button; 
		
		if(is_array($button)){
			$extra = $button;
			$button = $extra['button'];
			unset($extra["button"]);
			$str = "";
			foreach($extra as $attr => $value){
				$str .= $attr . '="' . $value . '" ';
			}
			$extra = $str;
		}

		switch ($button) :
			case 'delete': 
			?>
				<button class="mb-2 mr-2 btn btn-danger btn-hover-shine" type="submit" name="btnAction" value="delete" ><?=$extra?> 
					<?php echo lang('buttons:delete'); ?>
                </button>
				<?php break;
			case 're-index': ?>
				<button class="mb-2 mr-2 btn btn-warning btn-hover-shine" type="submit" name="btnAction" value="re-index" <?=$extra?> >
					<?php echo lang('buttons:re-index'); ?>
                </button>
				<?php break;
			case 'activate':
			case 'deactivate':
			case 'approve':
			case 'publish':
			case 'save':
			case 'save_exit':
			case 'unapprove':
			case 'upload': ?>
				<button class="mb-2 mr-2 btn btn-primary btn-hover-shine" type="submit" name="btnAction" value="<?php echo $button ?>" <?=$extra?> >
					<?php echo lang('buttons:' . $button); ?>
                </button>
				<?php break;
			case 'cancel':
			case 'close':
			case 'preview':
				$uri = 'admin/' . $this->module_details['slug'];
				$active_section = $this->load->get_var('active_section');

				if ($active_section && isset($this->module_details['sections'][$active_section]['uri']))
				{
					$uri = $this->module_details['sections'][$active_section]['uri'];
				}
				
				echo anchor($uri, lang('buttons:' . $button),'class="mb-2 mr-2 btn btn-dark btn-hover-shine '.$button.'" '.$extra);
				break;

			/**
			 * @var		$id scalar - optionally can be received from an associative key from array $extra
			 * @since	1.2.0-beta2
			 */
			case 'edit':
				$id = is_array($extra) && array_key_exists('id', $extra) ? '/' . $button . '/' . $extra['id'] : NULL;

				echo anchor('admin/' . $this->module_details['slug'] . $id, '<button class=" mb-2 mr-2 btn btn-dark btn-hover-shine" '.$extra.' >'.lang('buttons:' . $button).'</button>','class="edit"');
				break; ?>

		<?php endswitch; ?>
	<?php endforeach; ?>
<?php endif; ?>
