                            </div>
                        </div>
                        <div class="text-center text-white opacity-8 mt-3">Copyright © LazzoCMS</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="toast-container" class="toast-top-right"></div>
    <?= Asset::render_js('footer');?>
    <?php file_partial('notices');?>
</body>
</html>