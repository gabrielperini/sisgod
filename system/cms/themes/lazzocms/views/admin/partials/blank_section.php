<section class="main-card mb-3 card">
	<div class="card-header">
		<h4><?php if(isset($template['page_title'])) { echo '<h4>'.lang_label($template['page_title']).'</h4>'; } ?></h4>
	</div>
	<div class="card-body">
		<?php echo $content; ?>
	</div>
</section>