<div class="modal-body">
    <div class="h5 modal-title text-center">
        <h4 class="mt-2">
            <div>Bem vindo</div>
            <span>Por favor insira seus dados abaixo</span>
        </h4>
    </div>
    <?=form_open('admin/login',['id' =>"login-form"]);?>
        <div class="form-row">
            <div class="col-md-12">
                <div class="position-relative form-group"><input name="email" id="email" placeholder="Email aqui..." type="text" class="form-control"></div>
            </div>
            <div class="col-md-12">
                <div class="position-relative form-group"><input name="password" id="senha" placeholder="Senha aqui..." type="password" class="form-control"></div>
            </div>
        </div>
        <div class="position-relative form-check"><input name="remember" id="save" type="checkbox" class="form-check-input"><label for="save" class="form-check-label">Manter-me conectado</label></div>
    <?=form_close()?>
</div>
<div class="modal-footer clearfix">
    <div class="float-right">
        <button type="submit" form="login-form" class="btn btn-dark btn-lg">Entrar</button>
    </div>
</div>