<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Dbsisdm extends MY_Model {

    protected $gcedb;

    private $tableMembers;
    private $tableCapitulos;
    
    private $localhost = true;

    private $cache = ['user' => [], 'capitulo' => []];

    public function __construct()
    {
        if(!$this->localhost){
            $db = $this->load->database('gce', TRUE);
            $this->gcedb = $db;
        }

        $this->tableMembers = 'gcers_members';
        $this->tableCapitulos = 'gcers_capitulos';

    }

    public function is_user($id)
    {
        if($this->localhost){

            if($id !== "56431"){
                return ['success' => false, 'message' => 'Usuário não encontrado!'];
            }else{
                return ['success' => true];
            }
        }

        if(!$this->cache['user'][$id]){
            $user = $this->gcedb->where([
                'id' => $id
            ])->get($this->tableMembers)->result();
            $this->cache['user'][$id] = $user[0];
        }else{
            $user = [$this->cache['user'][$id]];
        }

        if(empty($user)){
            return ['success' => false, 'message' => 'Usuário não encontrado!'];
        }else if( $user[0]->perfil_bloqueado === '1'){
            return ['success' => false, 'message' => 'Usuário bloqueado, acesse o SISDM para mais informações!'];
        }else{
            return ['success' => true];
        }
    }

    public function get_user($id)
    {
        if($this->localhost){
            return $this->localhostGet(['method' => 'user', 'id' => $id])[0];
        }

        if(!$this->cache['user'][$id]){
            $user = $this->gcedb->where([
                'id' => $id
            ])->get($this->tableMembers)->result();
            $this->cache['user'][$id] = $user[0];
        }else{
            $user = [$this->cache['user'][$id]];
        }

        return (is_array($user) and $user[0]->perfil_bloqueado === '0') ? $user[0] : false;
    }

    public function get_capitulo($numero)
    {
        if($this->localhost){
            return $this->localhostGet(['method' => 'capitulo', 'id' => $numero]);
        }

        if(!$this->cache['capitulo'][$numero]){
            $capitulo = $this->gcedb->where([
                'numero_capitulo' => $numero
            ])->get($this->tableCapitulos)->result();
            $this->cache['capitulo'][$numero] = $capitulo[0];
        }else{
            $capitulo = [$this->cache['capitulo'][$numero]];
        }

        return $capitulo;
    }

    private function localhostGet($get){
        if(!$this->cache[$get['method']][$get['id']]){
            $url = "https://segce.demolayrs.com/sisdm.php?";
            $getStr = http_build_query($get);
            $return = file_get_contents($url . $getStr);
            $return = json_decode($return);
            
            $this->cache[$get['method']][$get['id']] = $return;
        }else{
            $return = $this->cache[$get['method']][$get['id']];
        }
        return [$return];
    }
	
}
