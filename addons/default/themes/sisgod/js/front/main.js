/*
	Introspect by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
*/

(function ($) {

	skel.breakpoints({
		xlarge: '(max-width: 1680px)',
		large: '(max-width: 1280px)',
		medium: '(max-width: 980px)',
		small: '(max-width: 736px)',
		xsmall: '(max-width: 480px)'
	});

	$(function () {

		var $window = $(window),
			$body = $('body');

		// Disable animations/transitions until the page has loaded.
		$body.addClass('is-loading');

		$window.on('load', function () {
			window.setTimeout(function () {
				$body.removeClass('is-loading');
			}, 100);
		});

		// Fix: Placeholder polyfill.
		$('form').placeholder();

		// Prioritize "important" elements on medium.
		skel.on('+medium -medium', function () {
			$.prioritize(
				'.important\\28 medium\\29',
				skel.breakpoint('medium').active
			);
		});

		// Off-Canvas Navigation.

		// Navigation Panel Toggle.
		$('<a href="#navPanel" class="navPanelToggle"></a>')
			.appendTo($body);

		// Navigation Panel.
		$(
			'<div id="navPanel">' +
			$('#nav').html() +
			'<a href="#navPanel" class="close"></a>' +
			'</div>'
		)
			.appendTo($body)
			.panel({
				delay: 500,
				hideOnClick: true,
				hideOnSwipe: true,
				resetScroll: true,
				resetForms: true,
				side: 'left'
			});

		// Fix: Remove transitions on WP<10 (poor/buggy performance).
		if (skel.vars.os == 'wp' && skel.vars.osVersion < 10)
			$('#navPanel')
				.css('transition', 'none');

	});

})(jQuery);

$(function () {
	//função animação de rolagem
	$("#nav a[href^=#]").on('click', function (event) {
		event.preventDefault();
		var hash = this.hash;
		$('html, body').animate({
			scrollTop: $(hash).offset().top - 80
		}, 1500, function () {
			window.location.hash = hash;
		});
	});

	var currimg = 0;

	function loadimg() {
		$('#banner').animate({ opacity: 1 }, 500, function () {
			//finished animating, minifade out and fade new back in
			$('#banner').animate({ opacity: 0.7 }, 400, function () {
				currimg++;
				if (currimg > images.length - 1) {
					currimg = 0;
				}
				var newimage = images[currimg];
				//swap out bg src
				$('#banner').css("background-image", "url(" + newimage + ")");
				//animate fully back in
				$('#banner').animate({ opacity: 1 }, 600, function () {
					//set timer for next
					setTimeout(loadimg, 5000);
				});
			});
		});
	}
	setTimeout(loadimg, 5000);
})