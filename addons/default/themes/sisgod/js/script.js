$(function(){
    var path = window.location.pathname;
    path = path.substring(((path.indexOf(BASE_URI) >= 0) ? BASE_URI.length : 1 ),path.length)
    if(MENU_BUTTON){
        path = MENU_BUTTON
    }

    var btn = $("ul.nav a[href='"+path+"']");
    btn.parent().addClass("active")
        .parents("div.collapse").addClass("show")
        .siblings("a.nav-link").attr("aria-expanded","true")
})