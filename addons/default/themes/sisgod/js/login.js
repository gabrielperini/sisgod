$(function(){
    setTimeout(function() {
        $('.card').removeClass('card-hidden');
    }, 700);

    $.blockUI.defaults = {
        message: $('#loader-blockui')
    }

    md.initFormExtendedDatetimepickers();
})