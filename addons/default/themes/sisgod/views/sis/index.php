<div class="header text-center ml-auto mr-auto">
    <h3 class="title">Bem Vindo ao SISGOD!</h3>
    <p class="category">Uma plataforma desenvolvida pelo Gabinete Estadual para todo o Estado do Rio Grande do Sul</p>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-danger card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Atalhos Rápidos</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="my-5 w-100">
                        <ul class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-around ">
                            <li class="nav-item col-12 col-md-3 ml-0">
                                <a class="nav-link show" href="/sis/blog">
                                    <i class="material-icons">auto_stories</i> Blog
                                </a>
                            </li>
                            <li class="nav-item col-12 col-md-3 ml-0">
                                <a class="nav-link" href="/sis/downloads/gce">
                                    <i class="material-icons">download_for_offline</i> Central de Downloads GCE
                                </a>
                            </li>
                            <li class="nav-item col-12 col-md-3 ml-0">
                                <a class="nav-link" href="/sis/downloads/gabinete">
                                    <i class="material-icons">download_for_offline</i> Central de Downloads Gabinete
                                </a>
                            </li>
                            <li class="nav-item col-12 col-md-3 ml-0">
                                <a class="nav-link" href="/sis/imersao">
                                    <i class="material-icons"> space_dashboard</i> Imersão Ritualística
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>