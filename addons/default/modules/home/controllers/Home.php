<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $virtudes = $this->db->get('infos_virtudes')->result();
        $banners = array_map(function($item){
            $path = (new DateTime($item->date))->format("Y/m/");
            return UPLOAD_PATH . "images/" . $path .$item->file;
        },$this->db->get('media')->result());

		$this->template
            ->title("Home")
            ->set(array(
                'virtudes' => $virtudes,
                'banners' => $banners,
            ))
            ->build('index');
    }

    public function sobre()
    {
		$this->template
            ->title("Sobre o Gabinete")
            ->build('sobre');
    }

    public function contato()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST' &&
            $this->input->post('nome') &&
            $this->input->post('email') &&
            $this->input->post('mensagem') 
        )
        {
            Events::trigger('email', array(
                'name' => $this->input->post('nome'),
                'email_to_send' => $this->input->post('email'),
                'message' => $this->input->post('mensagem'),
                'slug' => 'contact',
                'email' => Settings::get('contact_email'),
            ), 'array');

            $this->session->set_flashdata('message', "Mensagem Enviada");

            redirect($this->input->post('redirect_to') ? $this->input->post('redirect_to') : "");
        }
    }
    
}
?>