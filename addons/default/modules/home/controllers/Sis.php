<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sis extends Sis_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function downloads($path)
    {
        $this->config->load('files/files');
        $this->load->library('files/files');

        $subpath = $_GET["path"];
        $uriSubpath = $uriBack = explode("/",$subpath);
        array_pop($uriBack);
        $names = array(
            "gce" => "Grande Conselho Estadual",
            "gabinete" => "Gabinete Estadual",
        );

        $this->template
            ->title("Central de Downloads")
            ->set("files", Files::folder_contents("#" . $path . "/" . $subpath)["data"])
            ->set([
                "path" => $path,
                "parent" => $names[$path],
                "parentFolder" => $subpath ? $subpath . "/" : "",
                "backLink" => implode("/",$uriBack)
            ])
            ->build("files");
        // ->build_json(Files::folder_contents("#".$path."/".$subpath));
    }
}
