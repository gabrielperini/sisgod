<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is a util class for run in LazzoCMS
 *
 * @author 		Rico Vilela - BF2 Tecnologia
 * @website		http://bf2tecnologia.com.br
 * @package 	LazzoCMS
 * @subpackage 	Util Module
 */
class Funutil
{
	
	public function index(){  }
	
	// SHOW LANGS INTO SUP BAR MENU
	public function showLang() {
		$rtn = ''; $rt = '';
		$this->db->order_by('name','ASC');
		$query = $this->db->get('languages');
		foreach ($query->result() as $rs){
			if($rs->prefix == 'br'): $rt = $rs->thumbnail;
			else: $rtn .= '<li style="float: right;"><a href="#produto-'.$rs->prefix.'-tab"><img src="'.base_url().'uploads/default/files/languages/'.$rs->thumbnail.'" height="20" /></a></li>'; endif;
		}
		return array($rt,$rtn);
	}
	
	//MOUNT ARRAY OPTIONS FIELD IN SETTINGS
	public function makeArrays($array,$module){
	
		$rtn = null; $i = null;
		$arropt = array(
				'VARCHAR',
				'CHAR',
				'TEXT',
				'LONGTEXT',
		);
		
		foreach($array[0][$module] as $reg => $vals):
			if(in_array($vals['type'],$arropt)): if($reg != 'id' && $reg != 'thumbnail' && $reg != 'image'): if($i): $rtn .= '|'; endif; $rtn .= $reg.'='.substr($this->modifyLetter($reg),0,-1); $i=1; endif; endif;
		endforeach;

		return $rtn;
	}
	
	//CREATE DEFAULT TITLE BASED ON MODULE NAME
	public function modifyLetter($letter){
		$rtn='';
		$exp = explode("_",$letter);
		foreach($exp as $rt):
		$rtn .= ucfirst($rt).' ';
		endforeach;
		return $rtn;
	}
	
	//DEFINE FIELDS TYPE FOR MOUNT FORMS DEFAULTS
	public function defineField($field){
		$rtn = '';
		//UPDATE - N�COLAS
		/* Caso o campo esteja em min�sculo */
		$field = strtoupper($field);
		//UPDATE
		switch ($field){
			case 'VARCHAR' : { $rtn = 'form_input'; break; }
			case 'CHAR' : { $rtn = 'form_input'; break; }
			case 'INT' : { $rtn = 'form_input'; break; }
			case 'TIMESTAMP' : { $rtn = 'form_input'; break; }
			case 'DATE' : { $rtn = 'form_input'; break; }
			case 'TIME' : { $rtn = 'form_input'; break; }
			case 'DATETIME' : { $rtn = 'form_input'; break; }
			case 'LONGTEXT' : { $rtn = 'form_textarea'; break; }
			case 'TEXT' : { $rtn = 'form_textarea'; break; }
			default : {$rtn = ''; break; }
		}

		return $rtn;
	}
	
	//CHANGE FILES CONTENTS
	public function changeFiles($pathmodule,$filename,$arrfind,$arrreplace){
		$path = FCPATH.ADDON_FOLDER.'default/'.$pathmodule.'/'.$filename;
		$file_contents = file_get_contents($path);
		$file_contents = str_replace($arrfind,$arrreplace,$file_contents);
		file_put_contents($path,$file_contents);
	}
	
	//CHANGE CONTENS AND STRUCTURES OF DEFAULTS FILES
	public function choiceChange($type=null,$form=null,$module){
		if($type):
			//$this->changeFiles('modules/'.$module.'/language/brazilian/',$module.'_lang.php',$module,'BASEMODELADMIN'); DELETADO - N�COLAS
			$this->changeFiles('modules/'.$module.'/config','routes.php',$module,'BASEMODELADMIN');
			$this->changeFiles('modules/'.$module.'/models',$module.'_m.php',$module,'BASEMODELADMIN');
			$this->changeFiles('modules/'.$module.'/controllers','admin.php',$module,'BASEMODELADMIN');
			$this->changeFiles('modules/'.$module.'/views/admin/'.$module,'form.php',$module,'BASEMODELADMIN');
			$this->changeFiles('modules/'.$module.'/views/admin/'.$module,'items.php',$module,'BASEMODELADMIN');
			$this->changeFiles('modules/'.$module.'/js','image.js',$module,'BASEMODELADMIN');
			$this->changeFiles('modules/'.$module.'/libraries','Files.php',$module,'BASEMODELADMIN');
			//rename(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/language/brazilian/'.$module.'_lang.php', FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/language/brazilian/default_lang.php');
			unlink(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/language/brazilian/'.$module.'_lang.php'); // CORRIGIDO - N�COLAS
			unlink(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/models/'.$module.'_m.php');
			unlink(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/views/admin/'.$module.'/form.php'); //CORRIGIDO - N�COLAS
			rename(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/views/admin/'.$module, FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/views/admin/default');
			unlink(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/controllers/admin.php');
			unlink(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/language/english/'.$module.'_lang.php');
			//UPDATE - N�COLAS
			/*
			* Desinstala os arquivos de FRONTEND do m�dulo
			*/
			$this->changeFiles('modules/'.$module.'/controllers',$module.'.php',$module,'BASEMODELFRONT');
			rename(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/controllers/'.$module.'.php', FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/controllers/BASEMODELFRONT.php');
			//UPDATE
		else:
			copy(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/language/brazilian/default_lang.php', FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/language/brazilian/'.$module.'_lang.php');
			copy(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/models/origi_default_m.php', FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/models/'.$module.'_m.php');
			copy(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/views/admin/default/origi_form.php', FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/views/admin/default/form.php');
			rename(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/views/admin/default', FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/views/admin/'.$module);
			copy(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/controllers/origi_admin.php', FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/controllers/admin.php');
			copy(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/language/brazilian/'.$module.'_lang.php', FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/language/english/'.$module.'_lang.php');
			$this->changeFiles('modules/'.$module.'/language/brazilian/',$module.'_lang.php','BASEMODELADMIN',$module);
			$this->changeFiles('modules/'.$module.'/config','routes.php','BASEMODELADMIN',$module);
			$this->changeFiles('modules/'.$module.'/models',$module.'_m.php','BASEMODELADMIN',$module);
			$this->changeFiles('modules/'.$module.'/controllers','admin.php','BASEMODELADMIN',$module);			
			$this->changeFiles('modules/'.$module.'/views/admin/'.$module,'form.php','BASEMODELADMIN',$module);
			$this->changeFiles('modules/'.$module.'/views/admin/'.$module,'items.php','BASEMODELADMIN',$module);
			$this->changeFiles('modules/'.$module.'/js','image.js','BASEMODELADMIN',$module);
			$this->changeFiles('modules/'.$module.'/libraries','Files.php','BASEMODELADMIN',$module);
			if($form[0]): $this->changeFiles('modules/'.$module.'/views/admin/'.$module,'form.php','BASEFORMADMIN',$form[0]); endif;
			if($form[1]): $this->changeFiles('modules/'.$module.'/controllers','admin.php','BASEFORMADMIN',$form[1]); endif;
			if($form[2]): $this->changeFiles('modules/'.$module.'/models',$module.'_m.php','BASEFORMADMIN',$form[2]); endif;
			//UPDATE - N�COLAS
			/*
			* Instala os arquivos de FRONTEND do m�dulo
			*/
			rename(FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/controllers/BASEMODELFRONT.php', FCPATH.ADDON_FOLDER.'default/modules/'.$module.'/controllers/'.$module.'.php');
			$this->changeFiles('modules/'.$module.'/controllers',$module.'.php','BASEMODELFRONT',$module);
			//UPDATE
		endif;
	}
	
	//MAKE FORMS STRUCTURES
	public function makeForms($array,$module){
	
		$rtn = ''; $rr = ''; $ctl = ''; $obr = ''; $req = '';
		$node = $array[1];
		$array = $array[0][$module];
		
		foreach($array as $rs => $val){
			$tit = $rs; $i = 1;
			$val['NULL'] = isset($val['NULL']) ? $val['NULL'] : '';
			if($tit != 'id_users' && $tit != 'id_lang'): $trim = 'trim'; if(!$val['NULL']): $obr = '<span>*</span>'; $req = '|required'; endif; endif;
			$val['constraint'] = isset($val['constraint']) ? $val['constraint'] : '';
			if($val['constraint']): $cont = '|max_length['.$val['constraint'].']'; endif;
			
			foreach($val as $reg => $vals){
				
				if(array_key_exists($tit,$node)):
					if($node[$tit] == 'hidden' && $i < 2):	
						
						$rr .= "array(
		        				'field' => '".$tit."',
		        				'label' => '".ucfirst($tit)."',
		        				'rules' => '".$trim.$cont.$req."'
		        		),";
			
						if($tit == 'id_lang'): $ctl .= "'$tit' => \$input['$tit'][0],
						";
						else: $ctl .= "'$tit' => \$input['$tit'],
						";
						endif;
						
						if($tit != 'id_users' && $tit != 'id_lang'): $rtn .= '<?php $'.$tit.' = isset($'.$tit.') ? $'.$tit.' : 1; echo form_hidden(\''.$tit.'\', set_value(\''.$tit.'\', $'.$tit.'), \'class="width-15"\'); ?>
								'; endif;
						$i++;
					endif;
				else:
					if($i < 2):
						$rr .= "array(
		        				'field' => '".$tit."',
		        				'label' => '".ucfirst($tit)."',
		        				'rules' => 'trim".$cont.$req."'
		        		),";
			
						$ctl .= "'$tit' => \$input['$tit'],
						";
						
						if($tit != 'id_users' && $tit != 'id_lang' && $tit != 'thumbnail' && $tit != 'image'): $rtn .= '<li class="<?php echo alternator(\'\', \'even\'); ?>">
									<label for="name"><?php echo lang(\''.$module.':'.$tit.'\'); ?> '.$obr.'</label>
									<div class="input">
										<?php echo '.$this->defineField($vals).'(\''.$tit.'\', set_value(\''.$tit.'\', $'.$tit.'), \'class="width-15"\'); ?>
									</div>
								</li>
								'; endif;
					endif;
					$i++;
				endif;
			}
			$obr = ''; $req = ''; $cont = ''; $trim = '';
		}
		
		return array($rtn,$rr,$ctl);
	}
	
	//MAKE FORM LANGUAGE
	public function makeFormLang($id=null)
	{

		$rtn = ''; $rr = ''; $ctl = ''; $obr = ''; $req = '';
		$module = $this->uri->segment(2);
		$chmod = 'Module_'.$this->uri->segment(2);
		
		$mod = new $chmod();
		$array = $mod->tablesSettings();
		
		$node = $array[1];
		$array = $array[0][$module];
		
		$query = $this->db->get_where('settings',"module = '".$module."' AND slug = 'set_".$module."_language'");
		$rss = $query->result();
		$exp = explode(",",$rss[0]->value);

		
		$quer = $this->db->get_where('languages','prefix != "br"');
		foreach($quer->result() as $rsr):
		
			$rtn .= '<div class="form_inputs" id="produto-'.$rsr->prefix.'-tab">
					<fieldset>
						<input type="hidden" name="id_lang[]" value="'.$rsr->id.'" />
						<ul><li>'.$rsr->name.'</li>';
		
			if($this->uri->segment(4)):	
				$qry = $this->db->get_where($module,"id_lang = '".$rsr->id."' AND id_mod = '".$id."'")->result();
				$valss = isset($qry[0]->id) ? $qry[0]->id : '';
				if($valss): $rtn .= '<input type="hidden" name="id[]" value="'.$valss.'" />'; else: $rtn .= '<input type="hidden" name="id[]" value="x" />'; endif;
			endif;
			
			foreach($array as $rs => $val){
				$tit = $rs; $i = 1;

				$val['NULL'] = isset($val['NULL']) ? $val['NULL'] : '';
				if(!$val['NULL']): $obr = '<span>*</span>'; $req = '|required'; endif;
				$val['constraint'] = isset($val['constraint']) ? $val['constraint'] : '';
				if($val['constraint']): $cont = '|max_length['.$val['constraint'].']'; endif;
				
				foreach($val as $reg => $vals){
					if($i < 2): 
						
						if(array_key_exists($tit,$node) && in_array($tit,$exp)):
							if($node[$tit] == 'hidden'):	
								
								$rtn .= '<?php $'.$tit.' = isset($'.$tit.') ? $'.$tit.' : 1; echo form_hidden(\''.$tit.'_lang[]\', set_value(\''.$tit.'\', $'.$tit.'), \'class="width-15"\'); ?>
										';
								$i++;
							endif;
						elseif(in_array($tit,$exp)):
							$rtn .= '<li class="'.alternator('', 'even').'">
										<label for="name">'.lang($module.':'.$tit).' '.$obr.'</label>
										<div class="input">';
							if($this->uri->segment(4)):
								$titt = isset($qry[0]->$tit) ? $qry[0]->$tit : '';	
								if($titt): $rtn .= form_input($tit.'_lang[]', set_value($tit, $titt), 'class="width-15"'); else: $rtn .= form_input($tit.'_lang[]', set_value($tit, ''), 'class="width-15"'); endif;
							else: 
								$rtn .= form_input($tit.'_lang[]', set_value($tit, ''), 'class="width-15"');
							endif;
							$rtn .=	'	</div>
									</li>
									';
							$i++;
						endif;
						
					endif;
				}
				$obr = ''; $req = ''; $cont = '';
			}
		
			$rtn .= '</ul></field></div>';
		endforeach;
		
		return $rtn;
	}
	
}