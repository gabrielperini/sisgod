<!-- Main -->
<section id="main" >
    <div class="inner">
        <header class="major special">
            <h1>{{ infos:info titulo="chapa" }}</h1>
        </header>
        <a class="image fit"><img src="{{ infos:img titulo="chapa" }}" alt="" /></a>
        {{ infos:formatado titulo="chapa" }}
    </div>
</section>