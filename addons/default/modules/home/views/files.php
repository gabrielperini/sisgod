<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-icon card-header-rose">
                <div class="card-icon">
                    <i class="material-icons">
                        download_for_offline
                    </i>
                </div>
                <h4 class="card-title ">Downloads do <?= $parent ?></h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>
                                Nome
                            </th>
                        </thead>
                        <tbody>
                            <?php 
                            if($parentFolder){ 
                            ?>
                            <tr>
                                <td>
                                    <a href="sis/downloads/<?=$path?>?path=<?=$backLink?>">
                                        <i class="material-icons mr-2">
                                            arrow_back
                                        </i>
                                        Voltar
                                    </a>
                                </td>
                            </tr>
                            <?php 
                            }
                            foreach ($files["folder"] as $folder) { ?>
                                <tr>
                                    <td>
                                        <a href="sis/downloads/<?=$path?>?path=<?= $parentFolder.$folder->slug ?>">
                                            <i class="material-icons mr-2">
                                                folder
                                            </i>
                                            <?= $folder->name ?>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php foreach ($files["file"] as $file) { ?>
                                <tr>
                                    <td>
                                        <a target="_blanck" href="<?=$file->path ?>">
                                            <i class="material-icons mr-2">
                                            insert_drive_file
                                            </i>
                                            <?= $file->description ? $file->description : $file->name ?>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>