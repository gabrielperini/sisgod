<!-- Banner -->
<div class="bg-black">
    <section id="banner" style="background-image: url('<?=$banners[0]?>')">
        <div class="inner">
            <h1>{{ infos:formatado titulo="titulo home page" }}</h1>
        </div>
    </section>
</div>
<script>
    var images = <?=json_encode($banners)?>;
</script>

<!-- One -->
<section id="one">
    <div class="inner">
        <header>
            <h2>{{ infos:info titulo="chapa" }}</h2>
        </header>
        <p>{{ infos:info titulo="introducao chapa" }}</p>
        <ul class="actions">
            <li><a href="sobre" class="button alt">Leia Mais</a></li>
        </ul>
    </div>
</section>

<!-- Two -->
<section id="two">
    <div class="inner">
        <article>
            <div class="content">
                <header>
                    <h3>{{ infos:info titulo="ordem demolay" }}</h3>
                </header>
                <div class="image fit">
                    <img src="{{ infos:img titulo="ordem demolay" }}" alt="" />
                </div>
                <div>{{ infos:formatado titulo="ordem demolay" }}</div>
            </div>
        </article>
        <article class="alt">
            <div class="content">
                <header>
                    <h3>{{ infos:info titulo="ordem demolay rs" }}</h3>
                </header>
                <div class="image fit">
                    <img src="{{ infos:img titulo="ordem demolay rs" }}" alt="" />
                </div>
                <p>{{ infos:formatado titulo="ordem demolay rs" }}</p>
            </div>
        </article>
    </div>
</section>

<!-- Three -->
<section id="three">
    <header class="text-center">
        <h2>{{ infos:info titulo="titulo virtudes" }}</h2>
    </header>
    <div class="inner">
        <?php foreach ($virtudes as $v) {?>
        <article>
            <div class="content">
                <span class="icon fas fa-menorah"></span>
                <header class="text-center">
                    <h3><?=$v->titulo?></h3>
                </header>
                <div><?=$v->texto?></div>
            </div>
        </article>
        <?php }?>
    </div>
</section>