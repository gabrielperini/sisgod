<?php 
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Módulo Home Padrão.
 *
 * @author      BF2 Tecnologia
 * @copyright   Copyright (c) 2013, BF2 Tecnologia
 * @package	 	modules
 * @created		2013-07-01
 */
class Module_home extends Module {

	public $version = 1;
	public $module = 'home';

	public function __construct()
	{
		//if(!is_file(FCPATH.APPPATH.'libraries/funutil.php')): copy(FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/libraries/default_funutil.php',FCPATH.APPPATH.'libraries/funutil.php'); endif;
		parent::__construct();
		//$this->load->library('funutil');
	}
	
	public function info()
	{
		return array (
			'name' => 
				array (
			 		'en' => 'Home',
			 	 	'br' => 'Home'
				),
			'description' => 
				array (
					'en' => 'Manager home',
					'br' => 'Gerenciamento de home'
				),
			'backend' => false,
			'frontend' => true,
			'menu' => 'content'
		);
	}
	
	public function install()
	{

		return true;

	}

	public function uninstall(){
	
		return true;
	}

	public function upgrade($old_version) { return TRUE; }

	public function help() { return "Help não encontrado para este módulo"; }
	
}