<?php
//messages
$lang['BASEMODELADMIN:success']					=	'Concluido';
$lang['BASEMODELADMIN:error']					=	'Ocorreu um erro';
$lang['BASEMODELADMIN:no_items']					=	'Sem BASEMODELADMIN';
$lang['BASEMODELADMIN:create_BASEMODELADMIN']			=	'Criar';

//page titles
$lang['BASEMODELADMIN:create']					=	'Criar BASEMODELADMIN';
$lang['BASEMODELADMIN:name']						=	'Lista de BASEMODELADMIN';
$lang['BASEMODELADMIN:title']					=	'BASEMODELADMIN';
$lang['BASEMODELADMIN:list']						=	'BASEMODELADMIN';

//buttons
$lang['BASEMODELADMIN:save_button']				=	'Salvar';
$lang['BASEMODELADMIN:cancel_button']			=	'Cancelar';
$lang['BASEMODELADMIN:items']					=	'Items';
$lang['BASEMODELADMIN:edit']						=	'Editar';
$lang['BASEMODELADMIN:delete']					=	'Deletar';

//imagem
$lang['BASEMODELADMIN:imagem']					=	'Imagem';
$lang['BASEMODELADMIN:image']					=	'Imagem';
$lang['BASEMODELADMIN:delete_image']				=	'X';
$lang['BASEMODELADMIN:upload_image']				=	'Enviar Imagem';

//custom fields
$lang['BASEMODELADMIN:nome']						 = 'Nome';
$lang['BASEMODELADMIN:descricao']				 = 'Descrição';
$lang['BASEMODELADMIN:ficha_tecnica']			 = 'Ficha Técnica';
$lang['BASEMODELADMIN:data_inicio']				 = 'Data de Início';
$lang['BASEMODELADMIN:data_fim']					 = 'Data Final';
$lang['BASEMODELADMIN:marca_id']					 = 'Marca';
$lang['BASEMODELADMIN:video']					 = 'Vídeo';
$lang['BASEMODELADMIN:meta_description']			 = 'Meta Description';
$lang['BASEMODELADMIN:meta_keywords']			 = 'Meta Keywords';
$lang['BASEMODELADMIN:tag']						 = 'Tags';