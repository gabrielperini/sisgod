<?php
//messages
$lang['MODULENAME_SECTIONNAME:success']						=	'Concluido';
$lang['MODULENAME_SECTIONNAME:error']						=	'Ocorreu um erro';
$lang['MODULENAME_SECTIONNAME:no_items']					=	'Sem MODSDEC';
$lang['MODULENAME_SECTIONNAME:create_MODULENAME_SECTIONNAME'] = 'Criar';

//page titles
$lang['MODULENAME_SECTIONNAME:create']						=	'Criar MODSDEC';
$lang['MODULENAME_SECTIONNAME:name']						=	'Lista de MODSDEC';
$lang['MODULENAME_SECTIONNAME:title']						=	'MODSDEC';
$lang['MODULENAME_SECTIONNAME:list']						=	'MODSDEC';

//buttons
$lang['MODULENAME_SECTIONNAME:save_button']					=	'Salvar';
$lang['MODULENAME_SECTIONNAME:cancel_button']				=	'Cancelar';
$lang['MODULENAME_SECTIONNAME:items']						=	'Items';
$lang['MODULENAME_SECTIONNAME:edit']						=	'Editar';
$lang['MODULENAME_SECTIONNAME:delete']						=	'Deletar';
$lang['MODULENAME_SECTIONNAME:options']						= 	'Opções';
$lang['MODULENAME_SECTIONNAME:choose']						= 	'Choose one file';

//imagem
$lang['MODULENAME_SECTIONNAME:image']						=	'Imagem';
$lang['MODULENAME_SECTIONNAME:delete_image']				=	'X';
$lang['MODULENAME_SECTIONNAME:upload_image']				=	'Enviar Imagem';
$lang['MODULENAME_SECTIONNAME:gallery']						= 	'Galeria de Imagens';