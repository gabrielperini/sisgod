$( function() {
    $( ".sort" ).sortable({
		update: function(event, ui) {
			var order = $(this).sortable("toArray");
			var id = $(this).data('grupo');
			$(id).val(order.join(','));
		}
	});
	$( ".sort" ).disableSelection();

});