<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Fields Modules
 *
 * simple library of fields to forms in the modules
 *
 * @version		1.0
 * @author		Gabriel Perini
 * @copyright	2021 Gabriel Perini
 */

class Fields_modules extends MY_Fields_modules
{

    /**
	 * Constructor
	 */
	public function __construct()
	{
        parent::__construct();
	}

    // /**
    //  * 
    //  * Field Text
    //  * 
    //  */
    // public function field_text($field,$value)
    // {
    //     $input = form_input($field->field_slug,set_value($field->field_slug,$value),'class="form-control" id="'.$field->field_slug.'"');

    //     $rtn = $this->default_container($field,$input);

    //     return $rtn;
    // }

}