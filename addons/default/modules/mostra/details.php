<?php 
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * The custom module LazzoCMS.
 *
 * @author      Agência Lazzo - http://agencialazo.com.br
 * @copyright   Copyright (c) 2019, Agência Lazzo
 * @package	 	modules
 * @developer   Rico Vilela
 * @created		2014-11-11
 * @developer   Gabriel Perini
 * @updated		2019-12-01
 **/

class Module_mostra extends Module {

	public $version = 2;
	public $module = 'mostra';
	public $items = 'items';
	public $descricaolab = 'Módulo de Demonstração';
	//////////////////////////
	public $categoria = 'categorias';
	public $subcategoria = 'subcategorias';
	public $ordem = 'ordem';
	// public $ordemGrupo = 'categorias';
	private $complete_install = true;
	public $skip_xss = true;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('funutil');
		$this->load->library('MY_Fields_modules');
		$this->load->library($this->module.'/fields_modules');
	}
	
	public function info()
	{
		$descricaolab = $this->descricaolab;
		$result = array ( 'icon' => 'pe-7s-box1','skip_xss' => $this->skip_xss, 'name' => array ( 'en' => $descricaolab, 'br' => $descricaolab, 'es' => $descricaolab, ), 'description' => array ( 'en' => $descricaolab, 'br' => $descricaolab, 'es' => $descricaolab ), 'backend' => true, 'frontend' => true, 'menu' => 'content', 'sections' => array( $this->items =>  array( 'name' => $this->module.'_'.$this->items.':name', 'titulo' => $this->items, 'uri' => 'admin/'.$this->module.'/'.$this->items, 'shortcuts' =>  array( array( 'name' => $this->module.'_'.$this->items.':create', 'uri' => 'admin/'.$this->module.'/'.$this->items.'/create', 'class' => 'add' ), ), ), ), );
		if (isset($this->categoria)) { $result['sections'][$this->categoria] = array( 'name' => $this->module.'_'.$this->categoria.':name', 'titulo' => $this->categoria, 'uri' => 'admin/'.$this->module.'/'.$this->categoria, 'shortcuts' => array( array( 'name' => $this->module.'_'.$this->categoria.':create', 'uri' => 'admin/'.$this->module.'/'.$this->categoria.'/create', 'class' => 'add' ) ) ); }
		if (isset($this->subcategoria)) { $result['sections'][$this->subcategoria] = array( 'name' => $this->module.'_'.$this->subcategoria.':name', 'titulo' => $this->subcategoria, 'uri' => 'admin/'.$this->module.'/'.$this->subcategoria, 'shortcuts' => array( array( 'name' => $this->module.'_'.$this->subcategoria.':create', 'uri' => 'admin/'.$this->module.'/'.$this->subcategoria.'/create', 'class' => 'add' ) ) ); }
		if (isset($this->ordem)) { $result['sections'][$this->ordem] = array( 'name' => $this->module.'_'.$this->ordem.':name', 'titulo' => $this->ordem, 'uri' => 'admin/'.$this->module.'/'.$this->ordem ); }
		return $result;
	}
	
	public function tablesSettings()
	{	

		$arrDataDrop = array(
			'table' => $this->module .'_'. $this->categoria,
			// 'where' => array('id >'=> '1'),
			'key' => 'id',
			'value' => 'titulo'
		);
		$arrDataMulti = "1=primeiro|2=segundo|3=terceiro|4=quarto|5=quinto";

		$tables[$this->module.'_'.$this->items] = array(
			'id' => array('form' => 'id'),
			'titulo' => array('form' => 'text','constraint' => 100),
			'subtitulo' => array('form' => 'text', 'NULL' => true),
			'descricao' => array('form' => 'ckeditor', 'NULL' => true),
			'categorias' => array('form' => 'dropdown','options' => $arrDataDrop, 'NULL' => true), 
			'multiselect' => array('form' => 'multiselect','options' => $arrDataMulti,  'NULL' => true),
			'checkbox' => array('form' => '	', 'checkLabel' => "CHECK LABEL", 'NULL' => true),
			'data' => array('form' => 'date', 'NULL' => true),
			'datatime' => array('form' => 'datetime','NULL' => true),
			'time' => array('form' => 'time',  'NULL' => true),
			'arquivo' => array('form' => 'file', 'NULL' => true), /* type default será VARCHAR[255] */
			//IMAGE FIELDS PROBABLY USED 
			'thumbnail' => array('form' => 'image'),
			'image' => array('form' => 'image'),
		);
		
		$languagesTexts[$this->items] = array(
			'titulo' => 'Título',
			'subtitulo' => 'Sub-Título',
			'descricao' => 'Descrição',
			'categorias' => 'Dropdown',
			'multiselect' => 'Multiselect',
			'checkbox' => 'Checkbox',
			'data' => 'Data',
			'datatime' => 'Data e Tempo',
			'time' => 'Tempo',
			'arquivo' => 'Arquivo',
		);
		
		if ($this->categoria){
			$tables[$this->module.'_'.$this->categoria] = array(
				'id' => array('form' => 'id'),
				'titulo' => array('form' => 'text', 'NULL' => true),
				//'texto' => array('form' => 'textarea'),
				//IMAGE FIELDS PROBABLY USED 
				//'image' => array('form' => 'image'),
			);

			$languagesTexts[$this->categoria] = array(
				'titulo' => 'Título'
			);
		}
		
		if ($this->subcategoria){
			$tables[$this->module.'_'.$this->subcategoria] = array(
				'id' => array('form' => 'id'),
				'titulo' => array('form' => 'text', 'NULL' => true),
				//'texto' => array('form' => 'textarea'),
				//IMAGE FIELDS PROBABLY USED 
				//'image' => array('form' => 'image'),
			);

			$languagesTexts[$this->subcategoria] = array(
				'titulo' => 'Título'
			);
			
		}
		
		if ($this->ordem){
			$tables[$this->module.'_'.$this->ordem] = array(
				'id' => array('form' => 'id'),
				'grupo' => array('type' => 'VARCHAR', 'constraint' => 100),
				'ordem' => array('type' => 'TEXT', 'NULL' => true),
			);
		}

		/*echo '<pre>';
		print_r($tables);
		echo '</pre>';*/

		// ARRAY DEFAULT TYPES
		$defaultTypes = array(
			'text' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
			),
			'textarea' => array(
				'type' => 'LONGTEXT',
			),
			'ckeditor' => array(
				'type' => 'LONGTEXT',
			),
			'dropdown' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
			),
			'multiselect' => array(
				'type' => 'LONGTEXT',
			),
			'checkbox' => array(
				'type' => 'INT', 
				'constraint' => 11, 
			),
			'date' => array(
				'type' => 'DATE',
			),
			'datetime' => array(
				'type' => 'DATETIME',
			),
			'time' => array(
				'type' => 'TIME',
			),
			'file' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
			),
			'image' => array(
				'type' => 'VARCHAR', 
				'constraint' => 100, 
				'default' => '', 
				'NULL' => true
			),
			'id' => array(
				'type' => 'INT', 
				'constraint' => 11, 
				'auto_increment' => true, 
				'primary' => true
			),
		);

		// DEFINE DEFAULT TYPES
		foreach ($tables as $key => $table) {
			$tables[$key] = $this->funutil->defaultTypes($table,$defaultTypes);
		}
		
		//FIXED FIELDS PROBABLY USED
		$fixed = array( 'update' => array('form' => 'none','type' => 'TIMESTAMP'), 'id_users' => array('form' => 'none','type' => 'INT'), 'id_lang' => array('form' => 'none','type' => 'INT', 'default' => 1), 'id_mod' => array('form' => 'none','type' => 'INT', 'NULL' => true) );	
		foreach ($tables as $key => $val) {
			$tables[$key] = array_merge($val, $fixed);
		}
		// echo '<pre>';
		// print_r($tables);
		// echo '</pre>';
		// exit;
		
		return array($tables,$languagesTexts);
	}
	
	public function filesToChange()
	{
		$result = array(
			//ROUTES
			'cfg_routes' => array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/config/default_routes.php', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/config/routes.php', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME'), 'replace' => array( $this->module, $sect ) ),
			//CONTROLLER - FRONT
			'ctl_front' => array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/controllers/Default_front.php', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/controllers/'.ucfirst($this->module).'.php', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME'), 'replace' => array( $this->module, $sect ) ),
			//CONTROLLER - FILES
			'ctl_files' => array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/controllers/Default_files_front.php', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/controllers/Files_front.php', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME'), 'replace' => array( $this->module, $sect ) ),
		);

		if($this->ordem){ 
			//VIEW - ADMIN
			$result[] = array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/views/admin/defaultOrdem', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/views/admin/'. $this->ordem , 'isdir' => true, 'find' => array( 'MODULENAME', 'SECTIONNAME'), 'replace' => array( $this->module, $this->ordem ) );
			//CONTROLLER - ADMIN
			$result[] = array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/controllers/Default_ordem_admin.php', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/controllers/Admin_'.$this->ordem.'.php', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME','SECTIONCATEGORIE','SECTIONITEMS'), 'replace' => array( $this->module, $this->ordem, $this->ordemGrupo ? $this->ordemGrupo : $this->ordem, $this->items ) );
			//MODEL
			$result[] = array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/models/DefaultOrdem_m.php', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/models/'.ucfirst($this->module).'_'.$this->ordem.'_m.php', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME'), 'replace' => array( $this->module, $this->ordem ) );
		}

		$infos = $this->info();
		
		foreach($infos['sections'] as $sect => $val) {
			//LANGUAGE - BRAZILIAN
			$result[] = array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/language/brazilian/default_lang.php', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/language/brazilian/'.$sect.'_lang.php', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME', 'MODSDEC'), 'replace' => array( $this->module, $sect, ucfirst($sect) ) );
			//LANGUAGE - ENGLISH
			$result[] = array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/language/english/default_lang.php', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/language/english/'.$sect.'_lang.php', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME', 'MODSDEC'), 'replace' => array( $this->module, $sect, ucfirst($sect) ) );
			//SCRIPTS - IMAGE.JS
			$result[] = array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/js/default_image.js', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/js/image_'.$sect.'.js', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME'), 'replace' => array( $this->module, $sect ) );
			
			if($sect === $this->ordem) continue;
			
			//MODEL
			$result[] = array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/models/Default_m.php', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/models/'.ucfirst($this->module).'_'.$sect.'_m.php', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME','SECTIONCATEGORIE','SECTIONORDEM'), 'replace' => array( $this->module, $sect, $this->ordemGrupo ? $this->ordemGrupo : 'false', $this->ordem ) );
			//CONTROLLER - ADMIN
			$result[] = array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/controllers/Default_admin.php', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/controllers/Admin_'.$sect.'.php', 'isdir' => false, 'find' => array( 'MODULENAME', 'SECTIONNAME'), 'replace' => array( $this->module, $sect ) );
			//VIEW - ADMIN
			$result[] = array('default' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/views/admin/default', 'new' => FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/views/admin/'.$sect, 'isdir' => true, 'find' => array( 'MODULENAME', 'SECTIONNAME'), 'replace' => array( $this->module, $sect ) );
		}
		
		return $result;
	}

	public function install()
	{
		if($this->db->table_exists('languages')):
			
			//LOADS AND VARIABLES
			//LOAD FILES LIBRARY
			$this->load->library('files',array( 'module' => $this->module ));
			//GET MODULE INFO
			$infos = $this->info();
			//GET TABLE DEFINITIONS
			$tables = $this->tablesSettings();
			//GET THE FILES THAT NEED TO BE CHANGED
			$files = $this->filesToChange();
			
			//DATABASE UNINSTALL
			//REMOVE THE OLD SETTINGS
			$this->db->delete('settings', array('module' => $this->module));			 			 			
			//DROP OLD TABLES
			foreach ($infos['sections'] as $sect => $val) {
				$this->dbforge->drop_table($this->module.'_'.$sect, true);
			}
			
			
			//FILES CREATION AND CHANGES INSIDE THE MODULE
			if ($this->complete_install) {
				//MAKE FORMS FOR THE FILES
				$forms = array();
				
				foreach($infos['sections'] as $sect => $val) {
					if($sect === $this->ordem) continue;
					$forms[$sect] = $this->funutil->makeForms($tables, $this->module.'_'.$sect);
				}
				
				//CHANGE THE FILES
				$this->funutil->choiceChange(true, $files);

				//INSERT THE FORMS AND VALIDATION RULES OF TABLES INTO THE FILES
				foreach($forms as $key => $form){
   /* controller */ if ($form[0]) $this->funutil->changeFiles(FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/controllers/Admin_'.$key.'.php', 'BASEFORMADMIN', $form[0]);
		 /* model */if ($form[1]) $this->funutil->changeFiles(FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/models/'.ucfirst($this->module).'_'.$key.'_m.php', 'BASEFORMADMIN', $form[1]);
  /* files model */ $this->funutil->changeFiles(FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/models/'.ucfirst($this->module).'_'.$key.'_m.php', 'BASEFILEADMIN', $form[2]);
				}

				//INSERT THE FIELDS IN THE DATA FIELDS
				foreach($tables[0] as $namespace => $fields){
					if($namespace === $this->module.'_'.$this->ordem) continue;
					$this->fields_modules->delete_fields($namespace);
					$this->fields_modules->insert_fields($fields,$namespace);
				}
				
				//INSERT THE LANGUAGES IN FILES
				foreach($tables[1] as $section => $arr) {
					foreach ($arr as $sect => $val) {
						$this->funutil->endFiles(FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/language/brazilian/'. $section .'_lang.php','
						$lang[\''.$this->module.'_'. $section .':'.$sect.'\'] = \''.$val.'\';');
						$this->funutil->endFiles(FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/language/english/'. $section .'_lang.php','
						$lang[\''.$this->module.'_'. $section .':'.$sect.'\'] = \''.$val.'\';');
					}
				}
				
				//CONFIGURE THE ROUTES TO THE SECTIONS
				$routes = "";
				foreach ($infos['sections'] as $sect => $val) {
					$routes .= "\$route['".$this->module."/admin/".$sect."(:any)?'] = 'admin_".$sect."$1';
					";
				}
				$routes .= "\$route['".$this->module."/admin(:any)?'] = 'admin_".$this->items."$1';
				";
				$this->funutil->changeFiles(FCPATH.ADDON_FOLDER.'default/modules/'.$this->module.'/config/routes.php', 'ROUTES', $routes);
			}
			
			//FOLDERS FOR UPLOADS
			//CREATE THE FOLDER OF THE MODULE
			if ( ! is_dir($this->upload_path.'/files/'.$this->module) AND ! @mkdir($this->upload_path.'/files/'.$this->module,0777,TRUE)): return FALSE; endif;
			/** CRIA A PASTA FISICAMENTE E LOGICAMENTE **/
			$upload_path = Files::$path."/".$this->module;
			// Verifica se existe a pasta, senao cria a pasta física
			$path_check = Files::check_dir($upload_path);
				
			if (!$path_check['status']){
				$this->template->set('messages', array('error' => $path_check['message']));
			} else {
				// Cria a pasta lógica
				$result = Files::create_folder(0, $this->module);
			}
			
			
			//DATABASE INSTALL
			//INSERT SETTINGS OF THE MODULE
			$settings = array();
			$i = 100;
			$settings[] = array(
					'slug' => $this->module.'_pasta_raiz',
					'title' => 'Raiz dos Arquivos de '.$this->module,
					'description' => 'Pasta onde ficam armazenados todos os arquivos do módulo '.$this->module,
					'`default`' => $this->module,
					'options' => '',
					'`value`' => $this->module,
					'type' => 'text',
					'is_required' => 0,
					'is_gui' => 1,
					'module' => $this->module,
					'order'	=> $i
			);
			foreach($infos['sections'] as $sect => $val) {
				
				$settings[] = array(
						'slug' => $this->module.'_'.$sect.'_thumb',
						'title' => ucfirst($this->module).' - '.$sect.' - Imagem Destaque',
						'description' => 'Selecione esta opção se o módulo '.$this->module.'_'.$sect.' terá imagem destaque',
						'`default`' => '1',
						'`value`' => '0',
						'type' => 'radio',
						'`options`' => '1=Enabled|0=Disabled',
						'is_required' => 1,
						'is_gui' => 1,
						'module' => $this->module,
						'order'	=> $i--
				);
				
				$settings[] = array(
						'slug' => $this->module.'_'.$sect.'_thumbw',
						'title' => ucfirst($this->module).' - '.$sect.' - Largura da Imagem Destaque',
						'description' => 'Defina a largura para a imagem',
						'`default`' => '150',
						'`value`' => '150',
						'type' => 'text',
						'`options`' => '',
						'is_required' => 0,
						'is_gui' => 1,
						'module' => $this->module,
						'order'	=> $i--
				);
				$settings[] = array(
						'slug' => $this->module.'_'.$sect.'_thumbh',
						'title' => ucfirst($this->module).' - '.$sect.' - Altura da Imagem Destaque',
						'description' => 'Defina a altura para a imagem',
						'`default`' => '150',
						'`value`' => '150',
						'type' => 'text',
						'`options`' => '',
						'is_required' => 0,
						'is_gui' => 1,
						'module' => $this->module,
						'order'	=> $i--
				);
				
			}
			foreach ($settings as $setting) if( ! $this->db->insert('settings', $setting)): return FALSE; endif;
			//INSTALL THE TABLES
			return $this->install_tables($tables[0]);
			
			/*echo '<pre>';
			print_r($tables[0]);
			echo '</pre>';
			exit;*/
				
		else:
			$this->session->set_flashdata('error', 'O Módulo de Linguas deve ser instalado primeiro.');
			$_SESSION['errormsg'] = 1;
		endif;

	}

	public function uninstall(){
		
		//DATABASE UNINSTALL
		//DROP TABLES
		$infos = $this->info();
		foreach ($infos['sections'] as $sect => $val) {
			$this->dbforge->drop_table($this->module.'_'.$sect, true);
			$this->fields_modules->delete_fields($this->module.'_'.$sect);
		}
		//REMOVE THE SETTINGS
		$this->db->delete('settings', array('module' => $this->module));
		
		//UPLOAD FOLDERS UNINSTALL
		//DATABASE FOLDERS
		$folder = $this->db->get_where('file_folders', array('slug' => $this->module))->row();
		$this->db->delete('file_folders', array('parent_id' => $folder->id));
		$this->db->delete('file_folders', array('slug' => $this->module));
		//DELETE FOLDERS PHISICALLY
		$this->funutil->deleteFiles('uploads/default/files/'.$this->module);
		
		//DELETE CREATED FILES INSIDE THE MODULE
		if ($this->complete_install) {
			//DELETE CREATED FILES
			$this->funutil->choiceChange(false, $this->filesToChange());
		}
		
		return true;
	}

	public function upgrade($old_version) 
	{ 
		//GET TABLE DEFINITIONS
		$tables = $this->tablesSettings();
		
		//INSERT THE FIELDS IN THE DATA FIELDS
		foreach($tables[0] as $namespace => $fields){
			if($namespace === $this->module.'_'.$this->ordem) continue;
			$this->fields_modules->delete_fields($namespace);
			$this->fields_modules->insert_fields($fields,$namespace);
		}
		return TRUE; 
	}

	public function help() { return "Help não encontrado para este módulo"; }
	
}