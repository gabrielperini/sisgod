<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for LazzoCMS
 *
 * @author 		Rico Vilela - BF2 Tecnologia
 * @website		http://bf2tecnologia.com.br
 * @package 	LazzoCMS
 * @subpackage 	Sample Module
 */
class imersao_items_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$this->_table = 'imersao_items';
	}
	
	//create a new item
	public function create($input)
	{	
		$to_insert = array(
			'titulo' => $input['titulo'],
			'texto' => $input['texto'],
			'youtube' => $input['youtube'],
			'categorias' => $input['categorias'],
			'id_users' => $input['id_users'],
			'id_lang' => $input['id_lang'][0],
			
		);

		if($_FILES["arquivo"]):
			$upload_path = UPLOAD_PATH.'files/imersao/items/';
			mkdir($upload_path, 0755);
			if($_FILES["arquivo"]['tmp_name']){
				copy($_FILES["arquivo"]['tmp_name'],$upload_path.$_FILES["arquivo"]['name']);
				if($input["arquivoOld"]){
					unlink($upload_path.$input["arquivoOld"]);
				}
				$to_insert["arquivo"] = $_FILES["arquivo"]['name'];
			}
		endif;

		

		$this->db->insert($this->_table, $to_insert);
		
		return $this->insertLangs($input);
	}
	
	public function update($id, $input,$skip_validation = false)
	{
		
		$to_insert = array(
			'titulo' => $input['titulo'],
			'texto' => $input['texto'],
			'youtube' => $input['youtube'],
			'categorias' => $input['categorias'],
			'id_users' => $input['id_users'],
			'id_lang' => $input['id_lang'][0],
			
		);

		if($_FILES["arquivo"]):
			$upload_path = UPLOAD_PATH.'files/imersao/items/';
			mkdir($upload_path, 0755);
			if($_FILES["arquivo"]['tmp_name']){
				copy($_FILES["arquivo"]['tmp_name'],$upload_path.$_FILES["arquivo"]['name']);
				if($input["arquivoOld"]){
					unlink($upload_path.$input["arquivoOld"]);
				}
				$to_insert["arquivo"] = $_FILES["arquivo"]['name'];
			}
		endif;

		
		
		$this->db
			->where('id', $id)
			->update($this->_table, $to_insert);
			
		return $this->updateLangs($input);
	}
	
	public function  get_all_by(){
		
		$this->db->select($this->_table.'.*');
		
		return $this->db->get_where($this->_table,"id_mod = NULL")->result();
	}
	
	public function get_by_id($id){
		$this->db
		->select($this->_table.'.*')
		->where('id',$id);
		
		return $this->db->get($this->_table)->result();
	}
	
	public function get_imersao_category($id){
		
		$this->db
		->select($this->_table.'.*');
		//->where('categoria',$id);
		
		return $this->db->get($this->_table)->result();
	}
	
	public function get_next_id(){
	
		$result = $this->db->query("SHOW TABLE STATUS LIKE 'default_".$this->_table."' ")->result();
		
		foreach ($result as $objeto){	}
		
		return $objeto->Auto_increment;
	}
	
	public function insertLangs($input){
	
		$id = $this->db->insert_id();
	
		$count = count($input['id_lang']) - 1;
	
		$query = $this->db->get_where('settings',"module = '".$this->_table."' AND slug = 'set_".$this->_table."_language' AND value != '0'");
		if($query->num_rows() > 0):
			
			$rs = $query->result();
			$rt = explode(',',$rs[0]->value);
		
			for($i=0;$i<$count;$i++){
				$to_insert = '';
				foreach($rt as $ret){
					$to_insert[$ret] = $input[$ret.'_lang'][$i];
				}
		
				$to_insert['id_users'] = $input['id_users'];
				$to_insert['id_lang'] = $input['id_lang'][$i+1];
				$to_insert['id_mod'] = $id;
		
				$this->db->insert($this->_table, $to_insert);
			}
		
			if($i>0): return true; else: return false; endif;
		else:
			return true;
		endif;
	}
	
	public function updateLangs($input){
	
		$count = count($input['id_lang']) - 1;
	
		$query = $this->db->get_where('settings',"module = '".$this->_table."' AND slug = 'set_".$this->_table."_language' AND value != '0'");
		if($query->num_rows() > 0):
			
			$rs = $query->result();
			$rt = explode(',',$rs[0]->value);
		
			for($i=0;$i<$count;$i++){
				$to_insert = '';
				foreach($rt as $ret){
					$to_insert[$ret] = $input[$ret.'_lang'][$i];
				}
		
				$to_insert['id_users'] = $input['id_users'];
		
				if($input['id'][$i] == 'x'):
					$to_insert['id_users'] = $input['id_users'];
					$to_insert['id_lang'] = $input['id_lang'][$i+1];
					$to_insert['id_mod'] = $this->uri->segment(4);
					$this->db->insert($this->_table, $to_insert);
				else:
					$this->db
					->where('id', $input['id'][$i])
					->update($this->_table, $to_insert);
				endif;
			}
		
			if($i>0): return true; else: return false; endif;
			
		else:
			return true;
		endif;
	}
	
	private function getGaleryImages($folder_id)
	{
		$this->load->model('file_m');
	
		return $this->file_m->get_files('folder_id',$folder_id);
	}
	
	public function getAll()
	{
		$results = $this->db->get($this->_table)->result();
	
		foreach ($results as $result) $result->images = $this->getGaleryImages($result->pasta_imagens);
	
		return $results;
	}
	
	public function getOne()
	{
		$result = $this->db->get($this->_table)->row();
		
		if ($result) $result->images = $this->getGaleryImages($result->pasta_imagens);
		
		return $result;
	}

	public function get_all_ordered()
	{
		if(!'ordem'){
			throw new Exception("Essa Função não pode ser usada pois não há tabela de ordem", 1);
		}
		$this->load->model('imersao/imersao_ordem_m');
		
		$itemsDB = $this->get_all();
		$ordemDB = $this->imersao_ordem_m->get_all();
		
		$condition = 'categorias';
		
		if($condition !== 'false'){
			$this->load->model('imersao/imersao_categorias_m');
			$grupoDB = $this->imersao_categorias_m->get_all();

			$items = [];
			foreach ($itemsDB as $key => $v) {
				if(!is_array($items[$v->categorias])){
					$items[$v->categorias] = [];
				}
				array_push($items[$v->categorias], $v);
			}
			$items = grupoOrder($items,$ordemDB);
		}else{
			$items = order_by_arr($itemsDB,$ordemDB[0]->ordem,true);
		}
		return $items;
	}

	public function get_group_ordered($grupo)
	{
		$items = $this->get_all_ordered();
		return $items[$grupo];
	}
	
}
