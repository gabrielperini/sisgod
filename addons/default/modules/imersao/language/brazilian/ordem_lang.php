<?php
//messages
$lang['imersao_ordem:success']						=	'Concluido';
$lang['imersao_ordem:error']						=	'Ocorreu um erro';
$lang['imersao_ordem:no_items']					=	'Sem Ordem';
$lang['imersao_ordem:create_imersao_ordem'] = 'Criar';

//page titles
$lang['imersao_ordem:create']						=	'Criar Ordem';
$lang['imersao_ordem:name']						=	'Lista de Ordem';
$lang['imersao_ordem:title']						=	'Ordem';
$lang['imersao_ordem:list']						=	'Ordem';

//buttons
$lang['imersao_ordem:save_button']					=	'Salvar';
$lang['imersao_ordem:cancel_button']				=	'Cancelar';
$lang['imersao_ordem:items']						=	'Items';
$lang['imersao_ordem:edit']						=	'Editar';
$lang['imersao_ordem:delete']						=	'Deletar';
$lang['imersao_ordem:options']						= 	'Opções';
$lang['imersao_ordem:choose']						= 	'Escolha algum arquivo';

//imagem
$lang['imersao_ordem:image']						=	'Imagem';
$lang['imersao_ordem:delete_image']				=	'X';
$lang['imersao_ordem:upload_image']				=	'Enviar Imagem';
$lang['imersao_ordem:gallery']						= 	'Galeria de Imagens';