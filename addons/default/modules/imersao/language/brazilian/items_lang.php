<?php
//messages
$lang['imersao_items:success']						=	'Concluido';
$lang['imersao_items:error']						=	'Ocorreu um erro';
$lang['imersao_items:no_items']					=	'Sem Items';
$lang['imersao_items:create_imersao_items'] = 'Criar';

//page titles
$lang['imersao_items:create']						=	'Criar Items';
$lang['imersao_items:name']						=	'Lista de Items';
$lang['imersao_items:title']						=	'Items';
$lang['imersao_items:list']						=	'Items';

//buttons
$lang['imersao_items:save_button']					=	'Salvar';
$lang['imersao_items:cancel_button']				=	'Cancelar';
$lang['imersao_items:items']						=	'Items';
$lang['imersao_items:edit']						=	'Editar';
$lang['imersao_items:delete']						=	'Deletar';
$lang['imersao_items:options']						= 	'Opções';
$lang['imersao_items:choose']						= 	'Escolha algum arquivo';

//imagem
$lang['imersao_items:image']						=	'Imagem';
$lang['imersao_items:delete_image']				=	'X';
$lang['imersao_items:upload_image']				=	'Enviar Imagem';
$lang['imersao_items:gallery']						= 	'Galeria de Imagens';
						$lang['imersao_items:titulo'] = 'Título';
						$lang['imersao_items:texto'] = 'Texto';
						$lang['imersao_items:categorias'] = 'Categoria';
						$lang['imersao_items:youtube'] = 'Youtube Link';
						$lang['imersao_items:arquivo'] = 'Arquivo';