<?php
//messages
$lang['imersao_categorias:success']						=	'Concluido';
$lang['imersao_categorias:error']						=	'Ocorreu um erro';
$lang['imersao_categorias:no_items']					=	'Sem Categorias';
$lang['imersao_categorias:create_imersao_categorias'] = 'Criar';

//page titles
$lang['imersao_categorias:create']						=	'Criar Categorias';
$lang['imersao_categorias:name']						=	'Lista de Categorias';
$lang['imersao_categorias:title']						=	'Categorias';
$lang['imersao_categorias:list']						=	'Categorias';

//buttons
$lang['imersao_categorias:save_button']					=	'Salvar';
$lang['imersao_categorias:cancel_button']				=	'Cancelar';
$lang['imersao_categorias:items']						=	'Items';
$lang['imersao_categorias:edit']						=	'Editar';
$lang['imersao_categorias:delete']						=	'Deletar';
$lang['imersao_categorias:options']						= 	'Opções';
$lang['imersao_categorias:choose']						= 	'Choose one file';

//imagem
$lang['imersao_categorias:image']						=	'Imagem';
$lang['imersao_categorias:delete_image']				=	'X';
$lang['imersao_categorias:upload_image']				=	'Enviar Imagem';
$lang['imersao_categorias:gallery']						= 	'Galeria de Imagens';
						$lang['imersao_categorias:titulo'] = 'Título';
						$lang['imersao_categorias:grau'] = 'Grau';