<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class sis extends Sis_Controller
{
    public function __construct()
    {
        parent::__construct();

        if(!$this->permissions_sis->verify(["D","C"])){
            redirect("sis");
        }
        
        $this->load->model('imersao_items_m');
        $this->load->model('imersao_categorias_m');
		$this->load->library('funutil');
		$this->funutil->setLang();
    }

    public function index()
    {
        $this->db->select("id,titulo,grau");
        if($this->permissions_sis->verify("D","1")){
            $this->db->where("grau","0");
        }
    	$categoriasDB = $this->imersao_categorias_m->get_all();
    	foreach ($categoriasDB as $c) {
            $categorias[$c->id] = $c;
            $categorias[$c->id]->items = [];
        }
        
        $items = $this->imersao_items_m->get_all();
        foreach($items as $i){
            if(is_object($categorias[$i->categorias])){
                $categorias[$i->categorias]->items[] = $i;
            }
        }
        
    	
    	$this->template->title($this->module_details['name'])
			    	   ->set('categorias',$categorias)
			    	   ->build('index');
			    	//    ->build_json($categorias);
    }


    public function artigo($id = null)
    {
        if(!$id) redirect("/sis/imersao");
        $artigo = $this->imersao_items_m->get_by_id($id)[0];

    	$this->template->title($this->module_details['name'])
			    	   ->set([
                           'artigo' => $artigo,
                           'youtubeId' => substr(strstr($artigo->youtube,"be/"),3)
                        ])
			    	   ->build('artigo');
    }

    public function download($id = 0)
	{
        $this->config->load('files');
		$this->load->helper('download');

        $this->db->select("arquivo");
		$arquivo = $this->imersao_items_m->get_by_id($id)[0]->arquivo;

        $path = FCPATH.rtrim($this->config->item('files:path'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR. "imersao".DIRECTORY_SEPARATOR. "items".DIRECTORY_SEPARATOR;
		$data = file_get_contents($path. $arquivo);

		force_download($arquivo , $data);
	}
    
}
