<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin_SECTIONNAME extends Admin_Controller
{
	protected $section = 'SECTIONNAME';
	private $_folders	= array();
	private $_type 		= null;
	private $_ext 		= null;
	private $_filename	= null;
	private $OrdemGrupo	= null;
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MODULENAME_SECTIONNAME_m');
        $this->load->model('MODULENAME_SECTIONCATEGORIE_m');
        $this->load->model('MODULENAME_SECTIONITEMS_m');
		$this->load->model('file_folder_m');
        $this->load->library('form_validation');
        $this->lang->load('SECTIONNAME');
		$this->lang->load('files');
		$this->config->load('files');
		$this->load->library('files',array(
        		'module' => 'MODULENAME'
		));
		
		$this->OrdemGrupo = isset($this->module_details['module']->ordemGrupo);
        
        $allowed_extensions = '';
        
        foreach (config_item('files:allowed_file_ext') as $type)
        {
        	$allowed_extensions .= implode('|', $type).'|';
        }
        
        $this->template->append_metadata(
        		"<script>
				lazzo.lang.fetching = '".lang('files:fetching')."';
				lazzo.lang.fetch_completed = '".lang('files:fetch_completed')."';
				lazzo.lang.start = '".lang('files:start')."';
				lazzo.lang.width = '".lang('files:width')."';
				lazzo.lang.height = '".lang('files:height')."';
				lazzo.lang.ratio = '".lang('files:ratio')."';
				lazzo.lang.full_size = '".lang('files:full_size')."';
				lazzo.lang.cancel = '".lang('buttons:cancel')."';
				lazzo.lang.synchronization_started = '".lang('files:synchronization_started')."';
				lazzo.lang.untitled_folder = '".lang('files:untitled_folder')."';
				lazzo.lang.exceeds_server_setting = '".lang('files:exceeds_server_setting')."';
				lazzo.lang.exceeds_allowed = '".lang('files:exceeds_allowed')."';
				lazzo.files = { permissions : ".json_encode(Files::allowed_actions())." };
				lazzo.files.max_size_possible = '".Files::$max_size_possible."';
				lazzo.files.max_size_allowed = '".Files::$max_size_allowed."';
				lazzo.files.valid_extensions = '/".trim($allowed_extensions, '|')."$/i';
				lazzo.lang.file_type_not_allowed = '".addslashes(lang('files:file_type_not_allowed'))."';
				lazzo.lang.new_folder_name = '".addslashes(lang('files:new_folder_name'))."';
				lazzo.lang.alt_attribute = '".addslashes(lang('files:alt_attribute'))."';
        
				// deprecated
				//lazzo.files.initial_folder_contents = ".(int)$this->session->flashdata('initial_folder_contents').";
			</script>");
    }

    
	public function index()
    {
		$itemsDB = $this->MODULENAME_SECTIONITEMS_m->get_all();
		$ordemDB = $this->MODULENAME_SECTIONNAME_m->get_all();

		if(count($itemsDB) === 0){
			$this->session->set_flashdata('error', 'Você deve criar algum(a) SECTIONITEMS para ordenar');
		}

		if($this->OrdemGrupo){
			$grupoDB = $this->MODULENAME_SECTIONCATEGORIE_m->get_all();
			$ordemDB = $this->groupsFixOrder($ordemDB,$grupoDB);
	
			if(count($grupoDB) === 0){
				$this->session->set_flashdata('error', 'Você deve criar algum(a) SECTIONCATEGORIE para ordenar');
			}

			$items = [];
			foreach ($itemsDB as $key => $v) {
				if(!is_array($items[$v->SECTIONCATEGORIE])){
					$items[$v->SECTIONCATEGORIE] = [];
				}
				array_push($items[$v->SECTIONCATEGORIE], $v);
			}
			foreach ($grupoDB as $key => $g) $grupo[$g->id] = $g->titulo;

			$arrSet = [
				'items' => grupoOrder($items,$ordemDB),
				'grupo' => $grupo,
			];
		}else{
			$ordemDB = $this->createOrder($ordemDB);
			$arrSet = [
				'items' => [ 1 => order_by_arr($itemsDB,$ordemDB[0]->ordem,true)],
				'grupo' => array(1 => 'MODULENAME SECTIONITEMS'),
			];
		}

		$this->template->title($this->module_details['name'])
						->append_js('module::ordem.js')
						->append_css('module::ordem.css')
						->set($arrSet)
						->build('admin/SECTIONNAME/ordem');
	}
	
    public function up()
    {
		if($_SERVER['REQUEST_METHOD'] === 'POST'){
			if($this->OrdemGrupo){	
				$grupoDB = $this->MODULENAME_SECTIONCATEGORIE_m->get_all();
			}else{
				$obj = new stdClass;
				$obj->id = '1';
				$grupoDB = array($obj);
			}
			
			foreach ($grupoDB as $g) {
				$arrPost[$g->id] = $_POST["ordem".$g->id];
			}
			
			foreach ($arrPost as $k => $p) {
				if($p){
					$ordem = explode(',',$p);
					// print_r($ordem);
					$this->db->where('grupo',$k)->update('MODULENAME_SECTIONNAME',[
						'ordem' => serialize($ordem),
						'id_users' => 1 
					]);
				}
			}
			$this->session->set_flashdata('success', 'Ordem Alterada');
			redirect('admin/MODULENAME/SECTIONNAME');
		}
    }
    
	public function groupsFixOrder($ordemDB,$grupoDB)
	{
		$t = false;

		foreach ($ordemDB as $key => $value) {
			$gruposOrdenados[] = $value->grupo;
		}

		foreach ($grupoDB as $g) {
			if(!in_array($g->id,$gruposOrdenados)){
				$new_values = $this->db->where('SECTIONCATEGORIE',$g->id)->get('MODULENAME_SECTIONITEMS')->result();
				$new_order = [];
				foreach ($new_values as $key => $a) { 
					array_push($new_order,$a->id);
				}
				$this->db->insert('MODULENAME_SECTIONNAME',[
					'ordem' => serialize($new_order),
					'grupo' => $g->id,
					'id_users' => 1 
				]);
				$t = true;
			}
			$ordensValidas[] = $g->id;
		}

		foreach ($ordemDB as $k => $o) {
			if(!in_array($o->grupo,$ordensValidas)){
				$this->MODULENAME_SECTIONNAME_m->delete($o->id);
				$t = true;
			}
		}
		return $t ? $this->MODULENAME_SECTIONNAME_m->get_all() : $ordemDB;
	}

	public function createOrder($ordem)
	{
		if(empty($ordem)){
			$arr = ['0','1','2','3','4','5'];
			$obj = new stdClass;
			$obj->ordem = $arr;
			$ordem = array(0 => $obj);
			$this->db->insert('MODULENAME_SECTIONNAME',[
				'ordem' => serialize($arr),
				'grupo' => 1,
				'id_users' => 1 
			]);
		}

		return $ordem;
	}
    
}