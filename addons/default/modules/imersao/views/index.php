<?php 
foreach ($categorias as $cat) {
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-icon card-header-rose">
                <div class="card-icon">
                    <i class="material-icons">
                        space_dashboard
                    </i>
                </div>
                <h4 class="card-title "><?= $cat->titulo ?></h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <?php 
                            foreach ($cat->items as $item) {
                            ?>
                            <tr>
                                <td>
                                    <a href="sis/imersao/artigo/<?=$item->id?>">
                                        <?=$item->titulo?>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>