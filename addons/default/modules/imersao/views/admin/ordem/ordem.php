<section class="main-card mb-3 card">
	<div class="card-header">
		<h4>Ordem dos imersao</h4>
	</div>
	<div class="card-header card-header-tab-animation">
		<ul class="nav nav-justified">
			<?php 
			$f = 'active';
			foreach ($grupo as $k => $g) { ?>
			<li class="nav-item">
				<a data-toggle="tab" class="nav-link <?=$f?>" href="#grupo<?=$k?>">
					<span><?php echo ucfirst($g); ?></span>
				</a>
			</li>
			<?php $f = ''; } ?>
		</ul>
	</div>
	<?php echo form_open('admin/imersao/ordem/up');?>
	<div class="card-body">
		<div class="tab-content">
			<?php
			$f = 'active';
			foreach ($items as $g => $i) {
			?>
			<div class="tab-pane <?=$f?>" role="tabpanel" id="grupo<?=$g?>">
				<ul class="sort list-group list-unstyled " data-grupo='#ordem<?=$g?>'>
					<?php
					foreach ($i as $k => $v) {
					?>
					<li class="ui-state-default ui-sortable-handle bg-light br-a list-group-item my-1 py-1" id='<?=($v->id)?>'><?=$v->titulo?></li>
					<?php
					}
					?>
				</ul>
				<input type="hidden" name="ordem<?=$g?>" id='ordem<?=$g?>' >
			</div>
			<?php $f = ''; } ?>
		</div>

	</div>
	<div class="card-footer justify-content-end">
		<button type="submit" name="btnAction" value="save" class="mb-2 mr-2 btn btn-primary btn-hover-shine" >
			<span><?=lang('imersao_ordem:save_button')?></span>
		</button>
	</div>
	<?= form_close() ?>
</section>