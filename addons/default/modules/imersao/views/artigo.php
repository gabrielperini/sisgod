<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header card-header-danger card-header-text">
                <div class="card-text">
                    <h3 class="card-title"><?= ucwords($artigo->titulo) ?></h3>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 my-5 mx-1 mx-md-4">
                        <?= $artigo->texto ?>
                    </div>
                    <div class="col-12 mb-3">
                        <div class="img-container d-flex justify-content-center">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $youtubeId ?>" title="<?= ucwords($artigo->titulo) ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-12 mb-3 mx-1 mx-md-4">
                        <h3>Downloads</h3>
                        <div class="ml-4">
                            <a target="_blanck" href="sis/imersao/download/<?=$artigo->id?>"><?=$artigo->arquivo?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="sis/imersao">
                    <button class="btn btn-rose">Voltar</button>
                </a>
            </div>
        </div>
    </div>
</div>