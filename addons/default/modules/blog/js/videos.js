$(document).ready(function() {
    
	/* CASES */
	/*http://youtu.be/j048fWbA1Mo*/
	/*http://www.youtube.com/watch?v=j048fWbA1Mo*/
	/*http://vimeo.com/20918007*/
	
	$('[name="videoSubmit"]').live("click", function(e){
		e.preventDefault();
		
		var link = $('input#video1').val();
		var div = $(".video_watch");
		var height = div.height();
		var width = div.width();
		var code = "";
		var tag = "";
		var n = 0;
		
		div.empty();
				
		if (!link || link == "") {	
			tag = "Nenhuma visualização";
		}
		else if (link.search("youtu.be/") > -1) {
			n = link.search("youtu.be/");
			code = link.substr(n+9);
			tag = '<iframe width="'+width+'" height="'+height+'" src="http://www.youtube.com/embed/'+code+'" frameborder="0" allowfullscreen></iframe>';
		}
		else if (link.search("youtube.com/") > -1) {
			n = link.search("youtube.com/");
			code = link.substr(n+20);
			tag = '<iframe width="'+width+'" height="'+height+'" src="http://www.youtube.com/embed/'+code+'" frameborder="0" allowfullscreen></iframe>';
		}
		else if (link.search("vimeo.com/") > -1) {
			n = link.search("vimeo.com/");
			code = link.substr(n+10);
			tag = '<iframe src="http://player.vimeo.com/video/'+code+'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="'+width+'" height="'+height+'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		}
		else {
			tag = "ERROR";
		}
			
		div.append(tag);
			
	});
	
	$('[name="videoDelete"]').live("click", function(e){
		e.preventDefault();
		var link = $('input#video1');
		var div = $(".video_watch");
		var input = $("input[name='video']");
		link.val("");
		div.empty();
		input.val("");
	});

	$('[name="videoSubmit"]').click();
	
});

function videoHidden(obj) {
	$("input[name='video']").val($(obj).val());
}