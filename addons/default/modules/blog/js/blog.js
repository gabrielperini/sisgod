$(function(){
    $("#search-filter, #categoria-filter").on("keyup change",function(){
        var query = $("#search-filter").val(),
            categoria = $("#categoria-filter").val();

        blogResult = blog.filter((article) => {
            let matches = true;

            var alltext = [article.titulo,article.autor,removeTags(article.introducao),removeTags(article.texto)].join(" ").toLowerCase();
        
            if (query && !alltext.includes(query.toLowerCase())) {
                matches = false;
            }

            if (categoria && categoria !== "*" && categoria !== article.categoria) {
                matches = false;
            }
            
            return matches;
        }).map(blogComponent);

        $("#blog-list").html(blogResult)
    })

    const blogComponent = ({
        id,
        titulo,
        introducao,
        autor,
        thumbnail
    }) =>  $(`<div class="col-lg-4 col-md-6">
                <div class="card card-product">
                    <div class="card-header card-header-image" >
                        <a href="sis/blog/article/`+id+`">
                            <img class="img" src="` + UPLOAD_PATH + `files/blog/` + thumbnail + `" alt="">
                        </a>
                    </div>
                    <div class="card-body">
                        <h4 class="card-title mb-2">
                            <a href="sis/blog/article/`+id+`">`+titulo+`</a>
                        </h4>
                        <div class="card-description">
                        `+introducao+`
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <p class="card-category"><i class="material-icons">person</i> Por `+autor+`</p>
                        </div>
                        <a href="sis/blog/article/`+id+`" class="btn btn-link btn-primary">Ler Mais +</a>
                    </div>
                </div>
            </div>`);


    ///////////////////////////////////////////////// WRITE
    CKEDITOR.replace( "introducao" );
    CKEDITOR.replace( "texto" );
    $("#form-article").ajaxForm({
        beforeSend: (arr, $form, options) => {
            var formData = new FormData($("#form-article").get(0));
            if(formData.get("titulo") && formData.get("categoria") && formData.get("texto") && formData.get("introducao") && formData.get("thumbnail")){
                $("button[form='form-article']").attr("disabled",true).text("Loading...");
                return true
            }else{
                $.notify('Está Faltando alguma coisa!',{type:"danger"})
                return false
            }
        },
        success: (res) => {
            if(res.success){
                $("#form-article").clearForm();
                CKEDITOR.instances.introducao.setData("")
                CKEDITOR.instances.texto.setData("")
                $("#form-article .fileinput a[href='#remove']").click();
                $.notify("Artigo Enviado com sucesso!",{type:"success"})
            }else{
                res.errors.map(function(e){
                    $.notify(e.message,{type:"danger"})
                })
            }
            $("button[form='form-article']").attr("disabled",false).text("Enviar para análise");
        }
    })
})


function removeTags(str) {
    if ((str===null) || (str===''))
        return false;
    else
        str = str.toString();
          
    // Regular expression to identify HTML tags in 
    // the input string. Replacing the identified 
    // HTML tag with a null string.
    return str.replace( /(<([^>]+)>)/ig, '');
}