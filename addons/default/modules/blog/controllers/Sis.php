<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sis extends Sis_Controller
{

    private $permission;
    private $categorias;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('blog_items_m');
		$this->load->library('funutil');
		$this->funutil->setLang();
        $this->permission = [
            'D' => [
                'default' => ["0","1"],
                '1' => ["0","1"],
                '2' => ["0","1","2"]
            ],
            "C" => ["0","1","2"],
            "M" => ["0"],
            "E" => ["0"],
            "default" => ["0"]
        ];
        $this->categorias = [
            'D' => [
                'default' => ["*" => "Todas","0" => "Público","1" => "Grau Inciático"],
                '1' => ["*" => "Todas","0" => "Público","1" => "Grau Inciático"],
                '2' => ["*" => "Todas","0" => "Público","1" => "Grau Inciático","2" => "Grau DeMolay"]
            ],
            "C" => ["*" => "Todas","0" => "Público","1" => "Grau Inciático","2" => "Grau DeMolay"],
            "M" => ["*" => "Todas","0" => "Público"],
            "E" => ["*" => "Todas","0" => "Público"],
            "default" => ["*" => "Todas","0" => "Público"]
        ];
    }

    public function index()
    {
    	$this->db->where([
            'aprovado' => "1",
            'datatime <=' => (new DateTime())->format("Y-m-d H:i"),
        ]);
        
        $this->db->where_in("categoria",$this->permissions_sis->replace($this->permission));
        $blog =  $this->db->get("blog_items")->result();
    	
    	$this->template->title($this->module_details['name'])
                        ->append_js("module::blog.js")
			    	    ->set('blog',$blog)
                        ->set('categorias',$this->permissions_sis->replace($this->categorias))
			    	    ->build('sis/list');
    }
    
    public function article($id = null)
    {
        if(!$id) redirect("sis/blog");

    	$blog = $this->blog_items_m->get_blog_article($id,$this->permissions_sis->replace($this->permission));

        if(empty($blog))  redirect("sis/blog");
        $blog = $blog[0];

        $blog->data = (new Datetime($blog->datatime))->format('d/m/Y');
    	
    	$this->template->title($blog->titulo)
                        ->set("menuButton","sis/blog")
                        ->set("blog",$blog)
                        ->build('sis/article');
                        // ->build_json($blog);
    }
    
    public function write()
    {
        $cat = $this->permissions_sis->replace($this->categorias);
        unset($cat["*"]);
    	$this->template->title("Escrever Artigo")
                        ->append_js("ckeditor/ckeditor.js")
                        ->append_js("module::blog.js")
                        ->set('categorias',$cat)
			    	   ->build('sis/write');
                    // ->build_json($blog);
    }

    public function sendarticle()
    {
        if($this->input->is_ajax_request()){
            $img = $this->receiveThumbnail();

            $to_insert = [
                "titulo" => $_POST["titulo"],
                "categoria" => $_POST["categoria"],
                "introducao" => $_POST["introducao"],
                "autor" => $_POST["autor"] ? $_POST["autor"] : $this->current_user->display_name,
                "datatime" => (new DateTime())->format("Y-m-d H:i:s"),
                "createdby" => $this->current_user->id,
                "aprovado" => "0",
                "thumbnail" => $img["thumbnail"]
            ];

            $this->db->insert("blog_items", $to_insert);
            $to_insert["id"] = $this->db->insert_id();

            Events::trigger('email', array(
				"artigoId" => $to_insert["id"],
				'slug' => 'artigo-enviado-admin',
				'email' => Settings::get('contact_email'),
			), 'array');

            Events::trigger('email', array(
				'slug' => 'artigo-enviado-user',
				'email' => $this->current_user->email,
			), 'array');

            $this->template->build_json([
                "success" => true,
                "errors" => [],
                "content" => $to_insert,
            ]);
        }else{
            $this->template->build_json([
                "success" => false,
                "errors" => [["message" => "unauthorized"]],
                "content" => null,
            ]);
        }
    }
    
    private function receiveThumbnail()
    {
        $this->load->library('upload');
        $upload_path = UPLOAD_PATH.'files/blog/';
    
        $config['upload_path'] 		=  $upload_path;
        $config['allowed_types'] 	= 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
        $config['remove_spaces']	= TRUE;
        $config['overwrite']		= FALSE;
        $config['max_size']			= 0;
        $config['encrypt_name']		= TRUE;

        $this->upload->initialize($config);
            
        if($this->upload->do_upload('thumbnail')):

            $upload_data = $this->upload->data();
        
            $this->load->library('image_lib');

            return array(
                'raw_name' => $upload_data['raw_name'],
                'thumbnail' => $upload_data['raw_name'].$upload_data['file_ext'],
                'ext' => $upload_data['file_ext'],
                'upload_path' => base_url().'uploads/'.SITE_REF.'/files/blog/'
            );
            
        endif;
    }
}
?>