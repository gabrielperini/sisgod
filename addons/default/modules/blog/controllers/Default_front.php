<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MODULENAME extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MODULENAME_items_m');
		$this->load->library('funutil');
		$this->funutil->setLang();
    }

    public function index()
    {
    	$blog = $this->db->where([
            'aprovado' => "1",
            'datatime <=' => (new DateTime())->format("Y-m-d H:i"),
            'categoria' => '0'
        ])->get("blog_items")->result();
    	
    	$this->template->title($this->module_details['name'])
			    	   ->set('blog',$blog)
			    	   ->build('index');
    }
    
    public function article($id = null)
    {
        if(!$id) redirect("/blog");

    	$blog = $this->blog_items_m->get_blog_article($id);

        if(empty($blog))  redirect("/blog");
        $blog = $blog[0];
    	
    	$this->template->title($blog->titulo)
			    	   ->set("blog",$blog)
			    	   ->build('article');
                    // ->build_json($blog);
    }
    
}
?>