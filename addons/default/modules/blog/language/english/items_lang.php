<?php
//messages
$lang['blog_items:success']						=	'Concluido';
$lang['blog_items:error']						=	'Ocorreu um erro';
$lang['blog_items:no_items']					=	'Sem Items';
$lang['blog_items:create_blog_items'] = 'Criar';

//page titles
$lang['blog_items:create']						=	'Criar Items';
$lang['blog_items:name']						=	'Lista de Items';
$lang['blog_items:title']						=	'Items';
$lang['blog_items:list']						=	'Items';

//buttons
$lang['blog_items:save_button']					=	'Salvar';
$lang['blog_items:cancel_button']				=	'Cancelar';
$lang['blog_items:items']						=	'Items';
$lang['blog_items:edit']						=	'Editar';
$lang['blog_items:delete']						=	'Deletar';
$lang['blog_items:options']						= 	'Opções';
$lang['blog_items:choose']						= 	'Choose one file';

//imagem
$lang['blog_items:image']						=	'Imagem';
$lang['blog_items:delete_image']				=	'X';
$lang['blog_items:upload_image']				=	'Enviar Imagem';
$lang['blog_items:gallery']						= 	'Galeria de Imagens';
						$lang['blog_items:titulo'] = 'Título';
						$lang['blog_items:categoria'] = 'Categoria';
						$lang['blog_items:introducao'] = 'Introdução';
						$lang['blog_items:texto'] = 'Texto';
						$lang['blog_items:datatime'] = 'Data de Publicação';
						$lang['blog_items:aprovado'] = 'Aprovado';
						$lang['blog_items:autor'] = 'Autor';
						$lang['blog_items:createdby'] = 'Criado por';