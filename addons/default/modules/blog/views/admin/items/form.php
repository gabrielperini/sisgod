<section class="main-card mb-3 card">
	<div class="card-header card-header-tab-animation">
		<ul class="nav nav-justified">
			<li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#section-content-tab"><span><?php echo ucfirst(lang('blog_items:name')); ?></span></a></li>
			<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#section-image-tab"><span><?php echo lang('blog_items:image'); ?></span> </a></li>
			<?php echo $rt[1] = isset($rt[1]) ? $rt[1] : ''; ?>
		</ul>
	</div>
	<div class="card-body">
		<div class="tab-content">
			<div class="tab-pane active" role="tabpanel" id="section-content-tab">
				<?php echo form_open_multipart($this->uri->uri_string(),'id="form"'); ?>
					<?php 
					$fields = $this->db->where('field_namespace','blog_items')->get('data_fields')->result();
					foreach ($fields as $f) {
						$f->field_data = unserialize($f->field_data);
						echo $this->fields_modules->{'field_'.$f->field_type}($f,${$f->field_slug});
					}
					?>
					<input type="hidden" name="id_users" value="<?php echo $this->session->userdata('id');?>" />
					<input type="hidden" name="id_lang[]" value="1" />
				<?php echo form_close(); ?>
			</div>

			<!-- start image tab -->
			<div class="tab-pane" role="tabpanel" id="section-image-tab">
				<div class="position-relative form-group">
					<label for="file">
						Imagem
						<small class="d-block">*A imagem deve estar no padrão de tamanho, <?php echo $this->settings->blog_items_thumbw;?>px de largura por <?php echo $this->settings->blog_items_thumbh;?>px de altura.</small>
					</label>
					<div id="thumbnail" style="border: 1px solid #D3D3D3; border-radius: 5px 5px 5px 5px; padding: 5px; width: min-content;">
						<img
							src="<?php if($thumbnail): echo base_url().'uploads/default/files/blog/'.$thumbnail; else: echo 'http://placehold.it/'.$this->settings->blog_items_thumbw.'x'.$this->settings->blog_items_thumbh; endif; ?>"
							alt="<?php echo lang('blog_items:image'); ?>"
							style="height: <?php echo $this->settings->blog_items_thumbh;?>px;" />

					</div>
					<a id="delete-image-button" class="my-2 mr-2 btn btn-danger btn-hover-shine" href="#">Apagar Imagem</a>
				</div>
				<div class="position-relative form-group">
					<?php echo form_open_multipart(base_url().'admin/blog/items/ajax_upload_image', 'id="ajax-form-upload" name="form"'); ?>
					<input  class="form-control-file"  type="file" name="file">
					<input class="my-2 btn btn-dark btn-hover-shine" type="submit" value="<?php echo lang('blog_items:upload_image'); ?>">
					<?php echo form_close(); ?>
					<div class="erroimg" style="color: red;"></div>
					<div class="progress">
						<div class="bar progress-bar progress-bar-animated bg-success progress-bar-striped"></div>
					</div>
				</div>
			</div>
			<!-- end image tab -->
		</div>

	</div>
	<div class="card-footer">
		<div class="buttons actions">
			<button type="submit" name="btnAction" value="save" class="mb-2 mr-2 btn btn-primary btn-hover-shine" form="form">
				<span><?=lang('blog_items:save_button')?></span>
			</button>
			<a href="<?=base_url()?>admin/blog/items" class="mb-2 mr-2 btn btn-secondary btn-hover-shine"><?=lang('blog_items:cancel_button')?></a>
		</div>
	</div>
</section>
