<!-- Main -->
<section id="main" >
    <div class="inner">
        <header class="major special">
            <h1><?=ucwords($blog->titulo)?></h1>
        </header>
        <a class="image fit"><img src="<?=UPLOAD_PATH. "files/blog/" .$blog->thumbnail?>" alt="<?=$blog->titulo?>" /></a>
        <div class="my-2">
            <p>
            Publicado em: <b><?=$blog->data?></b>
            <br/>
            Autor: <b><?=$blog->autor?></b>
            </p>
        </div>
        <div>
            <?=$blog->texto?>
        </div>
    </div>
</section>