<!-- Main -->
<section id="main" >
    <div class="inner">
        <header class="major special">
            <h1>Blog</h1>
        </header>
        <div class="row 50% uniform">
            <?php foreach ($blog as $b) {?>
            <div class="4u 6u(medium) 12u(small) mt-5">
                <a href="blog/article/<?=$b->id?>"><h4><?=ucwords($b->titulo)?></h4></a>
                <span class="image fit mb-0"><img src="<?=UPLOAD_PATH ."files/blog/". $b->thumbnail ?>" alt=""></span>
                <div class="mb-2">
                    Autor: <b><?=$b->autor?></b>
                </div>
                <div><?=$b->introducao?></div>
                <ul class="actions">
                    <li><a href="blog/article/<?=$b->id?>" class="button alt">Leia Mais</a></li>
                </ul>
            </div>
            <?php } ?>
        </div>
    </div>
</section>