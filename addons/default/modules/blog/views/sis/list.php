<div class="row mb-3">
    <div class="col-12">
        <div class="card">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">filter_list</i>
                </div>
                <h4 class="card-title">Filtros</h4>
            </div>
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6 ">
                        <div class="input-group form-control-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                <i class="material-icons">search</i>
                                </span>
                            </div>
                            <div class="form-group bmd-form-group w-75">
                                <?=form_input("search","",'class="form-control" id="search-filter" ');?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mt-4 ml-3 mt-md-0 ml-md-0">
                        <div class="form-group bmd-form-group d-flex flex-column">
                            <label for="categoria">Categoria</label>
                            <?=form_dropdown("categoria",$categorias,"*",'class="selectpicker" data-size="7" data-style="btn btn-danger btn-round" title="Select Categoria" id="categoria-filter"');?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="blog-list">
    <?php foreach ($blog as $b) {?>
    <div class="col-lg-4 col-md-6">
        <div class="card card-product">
            <div class="card-header card-header-image" >
                <a href="sis/blog/article/<?=$b->id?>">
                    <img class="img" src="<?=UPLOAD_PATH ."files/blog/". $b->thumbnail ?>" alt="">
                </a>
            </div>
            <div class="card-body">
                <h4 class="card-title mb-2">
                    <a href="sis/blog/article/<?=$b->id?>"><?=ucwords($b->titulo)?></a>
                </h4>
                <div class="card-description">
                <?=$b->introducao?>
                </div>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <p class="card-category"><i class="material-icons">person</i> Por <?=$b->autor?></p>
                </div>
                <a href="sis/blog/article/<?=$b->id?>" class="btn btn-link btn-primary">Ler Mais +</a>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<script>
    const blog = <?=json_encode($blog);?>;
</script>