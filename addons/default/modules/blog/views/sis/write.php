<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header card-header-danger card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Escrever Artigo</h4>
                </div>
            </div>
            <div class="card-body ">
                <form id="form-article" method="post" action="sis/blog/sendarticle" class="form-horizontal">
                    <div class="row">
                        <label for='titulo' class="col-sm-1 col-form-label">Título</label>
                        <div class="col-sm-11">
                            <div class="form-group bmd-form-group">
                                <input type="text" name="titulo" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label for='categoria' class="col-sm-1 col-form-label">Categoria</label>
                        <div class="col-sm-11">
                            <div class="form-group bmd-form-group">
                                <?=form_dropdown("categoria",$categorias,"0",'class="selectpicker" data-size="7" required data-style="btn btn-danger btn-round" title="Select Categoria"');?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label for='introducao' class="col-sm-1 col-form-label">Introdução</label>
                        <div class="col-sm-11">
                            <div class="form-group bmd-form-group">
                                <textarea id="introducao" class="form-control" name="introducao" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label for='texto' class="col-sm-1 col-form-label">Texto</label>
                        <div class="col-sm-11">
                            <div class="form-group bmd-form-group">
                                <textarea id="texto" name="texto" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label for='autor' class="col-sm-1 col-form-label">Autor</label>
                        <div class="col-sm-11">
                            <div class="form-group bmd-form-group">
                                <input type="text" name="autor" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <label for='autor' class="col-sm-1 col-form-label">Image</label>
                        <div class="col-sm-11">
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                    <img src="<?=$this->template->get_theme_path()?>/img/image_placeholder.jpg" alt="thumbnail">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                <div>
                                    <span class="btn btn-danger btn-round btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="thumbnail" required />
                                    </span>
                                    <a href="#remove" class="btn btn-danger btn-round fileinput-exists"
                                        data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer ">
                <button type="submit" form="form-article" class="btn btn-fill btn-danger">Enviar Para Análise</button>
            </div>
        </div>
    </div>
</div>