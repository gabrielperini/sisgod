<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header card-header-danger card-header-text">
                <div class="card-text">
                    <h3 class="card-title"><?=ucwords($blog->titulo)?></h3>
                </div>

            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="img-container">
                            <img src="<?=UPLOAD_PATH. "files/blog/" .$blog->thumbnail?>" />
                        </div>
                    </div>
                    <div class="col-12 my-2">
                        <p>
                        Publicado em: <b><?=$blog->data?></b>
                        <br/>
                        Autor: <b><?=$blog->autor?></b>
                        </p>
                    </div>
                    <div class="col-12 ">
                        <?=$blog->texto?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>