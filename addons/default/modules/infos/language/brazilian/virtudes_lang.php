<?php
//messages
$lang['infos_virtudes:success']						=	'Concluido';
$lang['infos_virtudes:error']						=	'Ocorreu um erro';
$lang['infos_virtudes:no_items']					=	'Sem Virtudes';
$lang['infos_virtudes:create_infos_virtudes'] = 'Criar';

//page titles
$lang['infos_virtudes:create']						=	'Criar Virtudes';
$lang['infos_virtudes:name']						=	'Lista de Virtudes';
$lang['infos_virtudes:title']						=	'Virtudes';
$lang['infos_virtudes:list']						=	'Virtudes';

//buttons
$lang['infos_virtudes:save_button']					=	'Salvar';
$lang['infos_virtudes:cancel_button']				=	'Cancelar';
$lang['infos_virtudes:items']						=	'Items';
$lang['infos_virtudes:edit']						=	'Editar';
$lang['infos_virtudes:delete']						=	'Deletar';
$lang['infos_virtudes:options']						= 	'Opções';
$lang['infos_virtudes:choose']						= 	'Escolha algum arquivo';

//imagem
$lang['infos_virtudes:image']						=	'Imagem';
$lang['infos_virtudes:delete_image']				=	'X';
$lang['infos_virtudes:upload_image']				=	'Enviar Imagem';
$lang['infos_virtudes:gallery']						= 	'Galeria de Imagens';
						$lang['infos_virtudes:titulo'] = 'Título';
						$lang['infos_virtudes:texto'] = 'Texto';