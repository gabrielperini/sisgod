<?php
//messages
$lang['infos_items:success']						=	'Concluido';
$lang['infos_items:error']						=	'Ocorreu um erro';
$lang['infos_items:no_items']					=	'Sem Items';
$lang['infos_items:create_infos_items'] = 'Criar';

//page titles
$lang['infos_items:create']						=	'Criar Items';
$lang['infos_items:name']						=	'Lista de Items';
$lang['infos_items:title']						=	'Items';
$lang['infos_items:list']						=	'Items';

//buttons
$lang['infos_items:save_button']					=	'Salvar';
$lang['infos_items:cancel_button']				=	'Cancelar';
$lang['infos_items:items']						=	'Items';
$lang['infos_items:edit']						=	'Editar';
$lang['infos_items:delete']						=	'Deletar';
$lang['infos_items:options']						= 	'Opções';
$lang['infos_items:choose']						= 	'Choose one file';

//imagem
$lang['infos_items:image']						=	'Imagem';
$lang['infos_items:delete_image']				=	'X';
$lang['infos_items:upload_image']				=	'Enviar Imagem';
$lang['infos_items:gallery']						= 	'Galeria de Imagens';
						$lang['infos_items:titulo'] = 'Título';
						$lang['infos_items:valor'] = 'Valor';
						$lang['infos_items:formatado'] = 'Texto Formatado';