<div id="filter-stage">
	<?php if( $galleries['total'] > 0): ?>
		<table>
			<thead>
					<tr>
						<th><?php echo lang('pro_galleries:fields:cover'); ?></th>
						<th><?php echo lang('pro_galleries:fields:name'); ?></th>
						<th><?php echo lang('pro_galleries:fields:category'); ?></th>
						<th><?php echo lang('pro_galleries:fields:enable_comments'); ?></th>
						<th><?php echo lang('pro_galleries:fields:published'); ?></th>
						<th><?php echo lang('global:actions'); ?></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="6">
							<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
						</td>
					</tr>
				</tfoot>
				<tbody>
				<?php 
					foreach($galleries['entries'] as $gallery): 
					
						$id 		= $gallery['id'];
						$name		= $gallery['pro_galleries_name'];
						$slug		= $gallery['pro_galleries_slug'];
						$category 	= $gallery['pro_galleries_category']['pro_galleries_categories_name'];
						$comments 	= $gallery['pro_galleries_comments_enabled']['key'];
						$published 	= $gallery['pro_galleries_is_published']['key'];
						$cover		= $gallery['pro_galleries_cover'];

				?>
					<tr>
						<td><img src="<?php echo site_url('files/thumb/'.$cover['id']); ?>" alt="<?php echo $cover['name']; ?>"></td>
						<td><?php echo $name; ?></td>
						<td><?php echo ( $category != ""	 ) ? $category : "N/D"; ?></td>
						<td><?php echo ( $comments == "yes"	 ) ? lang('global:yes') : lang('global:no'); ?></td>
						<td><?php echo ( $published == "yes" ) ? lang('global:yes') : lang('global:no'); ?></td>
						<td>
							<?php 
								echo
								      anchor('admin/pro_galleries/create/'.$id, lang('global:edit'), 'class="btn orange"').' '.
								      anchor('admin/pro_galleries/delete/'.$id, lang('global:delete'), 'class="confirm btn red" title="'.lang('pro_galleries:view:categories:index:delete:popup').'"');
						    ?>						
						</td>
					</tr>		
				<?php endforeach; ?>
				</tbody>
			</table>	
	<?php else: ?>
		<div class="no_data"><?php echo lang('pro_galleries:view:index:no_data'); ?></div>
	<?php endif; ?>
</div>