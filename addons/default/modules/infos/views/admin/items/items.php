<section class="main-card mb-3 card">
	<?php echo form_open('admin/infos/items/delete');?>
	<?php if (!empty($items)): ?>
	<div class="card-header">
		<h4><?php echo lang('infos_items:list') ?></h4>
	</div>
	<div class="card-body">
		<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
			<thead>
				<tr>
					<th width="15"><?php echo form_checkbox(array('nome' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('infos_items:titulo'); ?></th>
					<th width="20"><?php echo lang('infos_items:image') ?></th>
					<th width="130"><?=lang('infos_items:options')?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach( $items as $item ): if(!$item->id_mod): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
					<td><?php echo $item->titulo; ?></td>
					<td><?php if($item->thumbnail):?><img src="<?php echo base_url().'uploads/default/files/infos/'.$item->thumbnail; ?>" height="40" /><?php endif;?></td>
					<td class="buttons buttons-small align-center actions">
						<?php echo
						anchor('admin/infos/items/edit/'.$item->id, lang('infos_items:edit'), 'class="btn btn btn-dark btn-hover-shine"').' '.
						anchor('admin/infos/items/delete/'.$item->id, lang('infos_items:delete'),array('name'=>'btnAction','value'=>'delete','class'=>'btn btn btn-danger btn-hover-shine')); ?>
					</td>
				</tr>
				<?php endif; endforeach; ?>
			</tbody>
		</table>
	</div>
	
	<div class="table_action_buttons card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
	</div>
	
	<?php else: ?>
	<div class="no_data card-body"><?php echo lang('infos_items:no_items'); ?></div>
	<?php endif;?>
	
	<?php echo form_close(); ?>
</section>