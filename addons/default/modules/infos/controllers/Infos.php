<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class infos extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('infos_items_m');
		$this->load->library('funutil');
		$this->funutil->setLang();
    }

    public function index()
    {
    	$items = $this->infos_items_m->get_all();
    	
    	$this->template->title($this->module_details['name'])
			    	   ->set('items',$items)
			    	   ->build('index');
    }
    
}
?>