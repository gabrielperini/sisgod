<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin_virtudes extends Admin_Controller
{
	protected $section = 'virtudes';
	private $_folders	= array();
	private $_type 		= null;
	private $_ext 		= null;
	private $_filename	= null;
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('infos_virtudes_m');
		$this->load->model('file_folder_m');
        $this->load->library('form_validation');
        $this->lang->load('virtudes');
		$this->lang->load('files');
		$this->config->load('files');
		$this->load->library('files',array(
        		'module' => 'infos'
        ));
        
        $allowed_extensions = '';
        
        foreach (config_item('files:allowed_file_ext') as $type)
        {
        	$allowed_extensions .= implode('|', $type).'|';
        }
        
        $this->template->append_metadata(
        		"<script>
				lazzo.lang.fetching = '".lang('files:fetching')."';
				lazzo.lang.fetch_completed = '".lang('files:fetch_completed')."';
				lazzo.lang.start = '".lang('files:start')."';
				lazzo.lang.width = '".lang('files:width')."';
				lazzo.lang.height = '".lang('files:height')."';
				lazzo.lang.ratio = '".lang('files:ratio')."';
				lazzo.lang.full_size = '".lang('files:full_size')."';
				lazzo.lang.cancel = '".lang('buttons:cancel')."';
				lazzo.lang.synchronization_started = '".lang('files:synchronization_started')."';
				lazzo.lang.untitled_folder = '".lang('files:untitled_folder')."';
				lazzo.lang.exceeds_server_setting = '".lang('files:exceeds_server_setting')."';
				lazzo.lang.exceeds_allowed = '".lang('files:exceeds_allowed')."';
				lazzo.files = { permissions : ".json_encode(Files::allowed_actions())." };
				lazzo.files.max_size_possible = '".Files::$max_size_possible."';
				lazzo.files.max_size_allowed = '".Files::$max_size_allowed."';
				lazzo.files.valid_extensions = '/".trim($allowed_extensions, '|')."$/i';
				lazzo.lang.file_type_not_allowed = '".addslashes(lang('files:file_type_not_allowed'))."';
				lazzo.lang.new_folder_name = '".addslashes(lang('files:new_folder_name'))."';
				lazzo.lang.alt_attribute = '".addslashes(lang('files:alt_attribute'))."';
        
				// deprecated
				//lazzo.files.initial_folder_contents = ".(int)$this->session->flashdata('initial_folder_contents').";
			</script>");
			
			
        $this->item_validation_rules = array(
        		array(
					'field' => 'titulo',
					'label' => 'Titulo',
					'rules' => 'trim|max_length[255]'
				),array(
					'field' => 'texto',
					'label' => 'Texto',
					'rules' => 'trim|required'
				),array(
					'field' => 'id_users',
					'label' => 'Id_users',
					'rules' => ''
				),array(
					'field' => 'id_lang',
					'label' => 'Id_lang',
					'rules' => ''
				),
        );
    }

    public function index()
    {
    	$items = $this->infos_virtudes_m->get_all();
    	$this->template->title($this->module_details['name'])->set('items', $items)->build('admin/virtudes/items');
    }
    
    public function create()
    {
    	$this->form_validation->set_rules($this->item_validation_rules);
    
    	if($this->form_validation->run()):
    	
    		if($this->infos_virtudes_m->create($this->input->post())):
    			$this->session->set_flashdata('success', lang('infos_virtudes:success'));
    			redirect('admin/infos/virtudes');
    		else:
    			$this->session->set_flashdata('error', lang('general:error'));
    			redirect('admin/infos/virtudes/create');
    		endif;
    		
    	endif;
    
    	foreach ($this->item_validation_rules AS $rule):
    		$data->{$rule['field']} = $this->input->post($rule['field']);
    	endforeach;
    	
		$this->template
			->title($this->module_details['name'])
			->append_css('module::contents.css')
			->append_js('module::jquery.form.js')
			->append_js('module::image_virtudes.js');

    	$this->template->build('admin/virtudes/form', $data);
    }
    
    public function edit($id)
    {
    	if(!$id): show_404(); exit(); endif;
    
    	$this->data = $this->infos_virtudes_m->get($id);
    
    	$this->form_validation->set_rules($this->item_validation_rules);
    
    	if($this->form_validation->run()):
    		unset($_POST['btnAction']);
    
    		if($this->infos_virtudes_m->update($id, $this->input->post())):
    			$this->session->set_flashdata('success', lang('general:success'));
    			redirect('admin/infos/virtudes');
    		else:
    			$this->session->set_flashdata('error', lang('general:error'));
    			redirect('admin/infos/virtudes/edit'.$id);
    		endif;
    	endif;
    
    	
    	$this->template->title($this->module_details['name'], lang('virtudes:edit'))
    	 
		->append_css('module::contents.css')
    	->append_js('module::jquery.form.js')
    	->append_js('module::image_virtudes.js');
		
    	$this->template->build('admin/virtudes/form', $this->data);
    }
    
    
	/**
	 * Delete blog post
	 *
	 * @param int $id The ID of the blog post to delete
	 */
	public function delete($id = 0)
	{

         if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])):

         	if (!Settings::get('infos_virtudes_gly')):
         	
	        	$this->infos_virtudes_m->delete_many($this->input->post('action_to'));
         	
         	else:
         	
	         	foreach($this->input->post('action_to') as $key => $item_id):
	         	
	         		$item = $this->infos_virtudes_m->get($item_id);
					
	         		if (!Settings::get('infos_virtudes_gly')):
						if ( ! in_array('delete_folder', Files::allowed_actions())): show_error(lang('files:no_permissions')); endif;
			    		$result = Files::delete_folder_and_files($item->pasta_imagens);
					endif;
					
					if (isset($item->thumbnail) and !empty($item->thumbnail)) if (file_exists('uploads/default/files/infos/'.$item->thumbnail)) unlink('uploads/default/files/infos/'.$item->thumbnail);
					if (isset($item->image) and !empty($item->image)) if (file_exists('uploads/default/files/infos/'.$item->image)) unlink('uploads/default/files/infos/'.$item->image);
	         		$this->infos_virtudes_m->delete($item_id);
	         		
	         	endforeach;
	         	
         	endif;
         	
         elseif (is_numeric($id)):

            $item = $this->infos_virtudes_m->get($id);
			if (isset($item->thumbnail) and !empty($item->thumbnail)) if (file_exists('uploads/default/files/infos/'.$item->thumbnail)) unlink('uploads/default/files/infos/'.$item->thumbnail);
			if (isset($item->image) and !empty($item->image)) if (file_exists('uploads/default/files/infos/'.$item->image)) unlink('uploads/default/files/infos/'.$item->image);
            $this->infos_virtudes_m->delete($id);
             
         endif;
        
        $this->session->set_flashdata('success', lang('general:success'));
        redirect('admin/infos/virtudes');
     }
    
    public function ajax_upload_image()
    {
    	if($this->input->is_ajax_request()):

    		$this->load->library('upload');
    
    		$upload_path = UPLOAD_PATH.'files/infos/';

    		if(!is_dir($upload_path)): @mkdir($upload_path,0777,TRUE); endif;
    
    		if(is_dir($upload_path)):
    
	    		$config['upload_path'] 		=  $upload_path;
	    		$config['allowed_types'] 	= 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
	    		$config['remove_spaces']	= TRUE;
	    		$config['overwrite']		= FALSE;
	    		$config['max_size']			= 0;
	    		$config['encrypt_name']		= TRUE;
	    
	    		$this->upload->initialize($config);
	    		 
	    		if($this->upload->do_upload('file')):
	
	    			$upload_data = $this->upload->data();
	    		
	    			unset($erroimg) ; $erroimg = '';
	    			if($upload_data['image_width'] < $this->settings->infos_virtudes_thumbw): $erroimg = 'A largura da imagem é menor do que a largura mínina exigida pelo sistema.'; endif;
	    			if($upload_data['image_height'] < $this->settings->infos_virtudes_thumbh): if($erroimg): $erroimg .= '<br />'; endif; $erroimg .= 'A altura da imagem é menor do que a altura mínina exigida pelo sistema.'; endif;
	
	    			$this->load->library('image_lib');
	
	    			unset($config);
	    			$config['source_image'] = $upload_path.$upload_data['file_name'];
	    			$config['maintain_ratio'] = FALSE;
	    			$config['quality'] = '90%';
	    			$config['width'] = $this->settings->infos_virtudes_thumbw;
	    			$config['height'] = $this->settings->infos_virtudes_thumbh;
	    			$config['new_image'] = $upload_path.$upload_data['raw_name'].'_thumb'.$upload_data['file_ext'];
	
	    			if($upload_data['image_width'] > $upload_data['image_height']):
	    				$config['master_dim'] = 'height';
	    			elseif($upload_data['image_height'] > $upload_data['image_width']):
	    				$config['master_dim'] = 'width';
	    			else:
	    				$config['maintain_ratio'] = TRUE;
					endif;
	    
	    			$this->image_lib->initialize($config);
	    			$this->image_lib->resize();
	    			$this->image_lib->clear();
	
	    			unset($config);
	    			$config['image_library'] = 'ImageMagick';
	
	    			$image_size = getimagesize($upload_path.$upload_data['raw_name'].'_thumb'.$upload_data['file_ext']);
	    			$config['source_image'] = $upload_path.$upload_data['raw_name'].'_thumb'.$upload_data['file_ext'];
	
	    			$config['x_axis'] = (($image_size[0] - $this->settings->infos_virtudes_thumbw) / 2);
	    			$config['y_axis'] = (($image_size[1] - $this->settings->infos_virtudes_thumbh) / 2);
	    			$config['width'] = $this->settings->infos_virtudes_thumbw;
	    			$config['height'] = $this->settings->infos_virtudes_thumbh;
	    			$config['quality'] = '90%';
	    			$config['maintain_ratio'] = FALSE;
	
	    			$this->image_lib->initialize($config);
	    			$this->image_lib->crop();
	    			$this->image_lib->clear();
	
	    			$json_data = array(
	    					'raw_name' => $upload_data['raw_name'],
	    					'thumbnail' => $upload_data['raw_name'].'_thumb'.$upload_data['file_ext'],
	    					'image' => $upload_data['raw_name'].$upload_data['file_ext'],
	    					'ext' => $upload_data['file_ext'],
	    					'erroimg' => $erroimg,
	    					'upload_path' => base_url().'uploads/'.SITE_REF.'/files/infos/'
	    			);
	    			
					//DELETA IMAGEM ORIGINAL
	    			unlink($upload_path.$upload_data['raw_name'].$upload_data['file_ext']);
	
	    			$this->template->build_json($json_data);
	
	    		endif;
	    	endif;
    	endif;
    }
    
    public function ajax_delete_image()
    {
    	if($this->input->is_ajax_request()):
    		if($this->input->post('thumbnail')):
    			$image_size = getimagesize(UPLOAD_PATH.'files/infos/'.$this->input->post('thumbnail'));
    			unlink(UPLOAD_PATH.'files/infos/'.$this->input->post('thumbnail'));
    		endif;
    		
    		if($this->input->post('image')):
    			unlink(UPLOAD_PATH.'files/infos/'.$this->input->post('image'));
    		endif;
    		 
    		if(isset($image_size)):
    			$json_data = array(
    					'width' => $image_size[0],
    					'height' => $image_size[1]
    			);
    			$this->template->build_json($json_data);
    		endif;
    		
    	endif;
    }
    
}