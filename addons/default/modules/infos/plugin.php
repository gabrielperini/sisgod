<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Infos Plugin
 *
 * informações do site
 */

class Plugin_Infos extends Plugin
{
	public $version = '1.5.0';

	public $name = array(
		'en'	=> 'Infos',
		'br'	=> 'Infos',
	);

	public $description = array(
		'en'	=> 'Infos do Site',
		'br'	=> 'Infos do Site'
	);

	/**
	 * Returns a PluginDoc array that PyroCMS uses 
	 * to build the reference in the admin panel
	 *
	 * All options are listed here but refer 
	 * to the Blog plugin for a larger Infos
	 *
	 * @return array
	 */
	public function _self_doc()
	{
		$info = array(
			'info' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Get Infos',
					'br' => 'Pega as Infos',
				),
				'single' => true,// will it work as a single tag?
				'double' => false,// how about as a double tag?
				'variables' => 'titulo',// list all variables available inside the double tag. Separate them|like|this
				'attributes' => array(
					'titulo' => array(// this is the titulo="World" attribute
						'type' => 'text',// Can be: slug, number, flag, text, array, any.
						'flags' => '',// flags are predefined values like asc|desc|random.
						'default' => '',// this attribute defaults to this if no value is given
						'required' => true,// is this attribute required?
					),
				),
			),
			'formatado' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Get formatados',
					'br' => 'Pega as formatados',
				),
				'single' => true,// will it work as a single tag?
				'double' => false,// how about as a double tag?
				'variables' => 'titulo',// list all variables available inside the double tag. Separate them|like|this
				'attributes' => array(
					'titulo' => array(// this is the titulo="World" attribute
						'type' => 'text',// Can be: slug, number, flag, text, array, any.
						'flags' => '',// flags are predefined values like asc|desc|random.
						'default' => '',// this attribute defaults to this if no value is given
						'required' => true,// is this attribute required?
					),
				),
			),
			'img' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Get Images',
					'br' => 'Pega as Images',
				),
				'single' => true,// will it work as a single tag?
				'double' => false,// how about as a double tag?
				'variables' => 'titulo',// list all variables available inside the double tag. Separate them|like|this
				'attributes' => array(
					'titulo' => array(// this is the titulo="World" attribute
						'type' => 'text',// Can be: slug, number, flag, text, array, any.
						'flags' => '',// flags are predefined values like asc|desc|random.
						'default' => '',// this attribute defaults to this if no value is given
						'required' => true,// is this attribute required?
					),
				),
			),
			'array' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Get array and explode',
					'br' => 'Pega uma matriz e divide ela',
				),
				'single' => false,
				'double' => true,
				'variables' => 'titulo|item|demiliter',
				'attributes' => array(
                    'titulo' => array(// this is the titulo="World" attribute
						'type' => 'text',// Can be: slug, number, flag, text, array, any.
						'flags' => '',// flags are predefined values like asc|desc|random.
						'default' => '',// this attribute defaults to this if no value is given
						'required' => true,// is this attribute required?
					),
                    'item' => array(// this is the titulo="World" attribute
						'type' => 'text',// Can be: slug, number, flag, text, array, any.
						'flags' => '',// flags are predefined values like asc|desc|random.
						'default' => '{{item}}',// this attribute defaults to this if no value is given
					),
                    'demiliter' => array(// this is the titulo="World" attribute
						'type' => 'text',// Can be: slug, number, flag, text, array, any.
						'flags' => '',// flags are predefined values like asc|desc|random.
						'default' => ',',// this attribute defaults to this if no value is given
					),
                ),
			),
		);
	
		return $info;
	}

	private $infos;

	public function __construct() {
		$infos = $this->db->get('infos_items')->result();
		$arr = [];

		foreach ($infos as $i) {
			$arr[$i->titulo] = $i;
		}
		$this->infos = $arr;
	}

	/**
	 * Infos
	 *
	 * Usage:
	 * {{ infos:info titulo="" }}
	 *
	 * @return string
	 */
	function info()
	{
		$titulo = $this->attribute('titulo');
		return isset($this->infos[$titulo]) ? $this->infos[$titulo]->valor : '';
	}

	/**
	 * Formatado
	 *
	 * Usage:
	 * {{ infos:formatado titulo="" }}
	 *
	 * @return string
	 */
	function formatado()
	{
		$titulo = $this->attribute('titulo');
		return isset($this->infos[$titulo]) ? $this->infos[$titulo]->formatado : '';
	}

	/**
	 * IMG
	 *
	 * Usage:
	 * {{ infos:info titulo="" }}
	 *
	 * @return string
	 */
	function img()
	{
		$titulo = $this->attribute('titulo');
		
		return isset($this->infos[$titulo]) ? UPLOAD_PATH . "files/infos/" . $this->infos[$titulo]->thumbnail : '';
	}

	/**
	 * Array loop
	 *
	 * Usage:
	 * {{ infos:array item="{{item}}" demiliter=","  }}
	 * 	<tag>{{item}}<tag>
	 * {{ /infos:array }}
	 *
	 * @return string
	 */
	function array()
	{
        $titulo = $this->attribute('titulo');
        $item = $this->attribute('item');
        $demiliter = $this->attribute('demiliter');
		$array = explode($demiliter,$this->infos[$titulo]->valor);
		$content = $this->content();
		$return = "";
		foreach ($array as $t) {
			$return .= str_replace($item,$t,$content) . "\n";
		}
		return $return;
	}
}

/* End of file infos.php */