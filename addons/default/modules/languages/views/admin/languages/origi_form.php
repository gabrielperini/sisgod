
<section class="main-card card mb-3">
	
	<div class="card-header">
		<h4>
			<?php echo lang('BASEMODELADMIN:'.$this->method); ?>
		</h4>
	</div>
	<div class="card-header card-header-tab-animation">
		<ul class="nav nav-justified">
			<li class="nav-item"><a  data-toggle="tab" class="nav-link active" href="#produto-content-tab"><span><?php echo ucfirst(lang('BASEMODELADMIN:name')); ?></span> </a></li>
			<li class="nav-item"><a  data-toggle="tab" class="nav-link" href="#produto-imagem-tab"><span><?php echo lang('BASEMODELADMIN:imagem'); ?></span> </a></li>
		</ul>
	</div>
	
	<div class="card-body">

		<div class="tab-content">

			<div class="tab-pane active" role="tabpanel" id="produto-content-tab">
				<?php echo form_open_multipart($this->uri->uri_string(),'id="form"'); ?>
					<?php $thumbnail = isset($thumbnail) ? $thumbnail : ''; echo form_hidden('thumbnail', $thumbnail); ?>
					<?php $image = isset($image) ? $image : ''; echo form_hidden('image', $image); ?>
					<ul>
						BASEFORMADMIN
					</ul>
					<input type="hidden" name="id_users" value="<?php echo $this->session->userdata('id');?>" />
				<?php echo form_close(); ?>
			</div>

			<div class="tab-pane" role="tabpanel" id="produto-imagem-tab">
				<div class="position-relative form-group">
					<label for="file">
						Imagem
						<small class="d-block">*A imagem deve estar no padrão de tamanho, <?php echo $this->settings->thumb_BASEMODELADMIN_width;?>px de largura por <?php echo $this->settings->thumb_BASEMODELADMIN_height;?>px de altura.</small>
					</label>
					<div id="thumbnail" style="border: 1px solid #D3D3D3; border-radius: 5px 5px 5px 5px; padding: 5px; width: min-content;">
						<img
							src="<?php if($thumbnail): echo base_url().'uploads/default/files/BASEMODELADMIN/'.$thumbnail; else: echo 'http://placehold.it/'.$this->settings->thumb_BASEMODELADMIN_width.'x'.$this->settings->thumb_BASEMODELADMIN_height; endif; ?>"
							alt="<?php echo lang('BASEMODELADMIN:image'); ?>"
							style="height: <?php echo $this->settings->thumb_BASEMODELADMIN_height;?>px;" />

					</div>
					<a id="delete-image-button" class="my-2 mr-2 btn btn-danger btn-hover-shine" href="#">Apagar Imagem</a>
				</div>
				<div class="position-relative form-group">
					<?php echo form_open_multipart(base_url().'admin/'. $this->module .'/ajax_upload_image', 'id="ajax-form-upload" name="form"'); ?>
					<input  class="form-control-file"  type="file" name="file">
					<input class="my-2 btn btn-dark btn-hover-shine" type="submit" value="<?php echo lang('BASEMODELADMIN:upload_image'); ?>">
					<?php echo form_close(); ?>
					<div class="erroimg" style="color: red;"></div>
					<div class="progress">
						<div class="bar progress-bar progress-bar-animated bg-success progress-bar-striped"></div>
					</div>
				</div>

			</div>
		</div>
		
	</div>
	<div class="card-footer">
		<button type="submit" name="btnAction" value="save" class="mb-2 mr-2 btn btn-primary btn-hover-shine" form="form">
			<span><?=lang('BASEMODELADMIN:save')?></span>
		</button>
		<a href="<?=base_url()?>admin/BASEMODELADMIN" class="mb-2 mr-2 btn btn-secondary btn-hover-shine"><?=lang('BASEMODELADMIN:cancel')?></a>
	</div>
</section>
