
<section class="main-card card">
	

<?php if (!empty($items)): ?>

	<div class="card-header">
		<h4><?php echo lang('languages:items'); ?></h4>
	</div>
	
	<?php echo form_open('admin/languages/delete');?>
	<div class="card-body" >
		<table class="table table-hover table-striped min-width-100 table-bordered dataTable dtr-inline" role="grid">
			<thead>
				<tr role="row">
					<th width="15"><?php echo form_checkbox(array('nome' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('languages:name'); ?></th>
					<th width="20"><?php echo lang('languages:imagem'); ?></th>
					<th width="130"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach( $items as $item ): ?>
				<tr role="row">
					<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
					<td><?php echo $item->name; ?></td>
					<td><?php if($item->thumbnail):?><img src="<?php echo base_url().'uploads/default/files/languages/'.$item->thumbnail; ?>" height="40" /><?php endif;?></td>
					<td class="actions">
						<?php echo
						anchor('admin/languages/edit/'.$item->id, lang('languages:edit'), 'class="btn-hover-shine btn btn-dark"').' '.
						anchor('admin/languages/delete/'.$item->id, lang('languages:delete'), array('class'=>'btn-hover-shine btn btn-danger','name'=> 'btnAction','value'=> 'delete' )); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<div class="card-footer">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
	</div>
	<?php echo form_close(); ?>
	
<?php else: ?>
	<div class="card-body"><?php echo lang('languages:no_items'); ?></div>
<?php endif;?>
</section>