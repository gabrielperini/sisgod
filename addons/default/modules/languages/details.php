<?php 
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * The language module LazzoCMS.
 *
 * @author      Rico Vilela - BF2 Tecnologia
 * @copyright   Copyright (c) 2013, BF2 Tecnologia
 * @package	 	modules
 * @created		2013-05-13
 */
class Module_languages extends Module {

	public $version = 1.0;
	public $module = 'languages';

	public function __construct()
	{
		parent::__construct();
		unset($_SESSION['modlang']);
		$_SESSION['modlang'] = 'Module_'.$this->module;
		$this->load->library($this->module.'/funutillang');
	}
	
	public function info()
	{
		return array (
			'icon' => 'pe-7s-box1',
			'name' => 
				array (
			 		'en' => 'Languages Module',
			 	 	'br' => 'Modulo de Linguas'
				),
			'description' => 
				array (
					'en' => 'Languages Module CMS and Front',
					'br' => 'Modulo de Linguas CMS e Front'
				),
			'backend' => true,
			'frontend' => true,
			'menu' => 'content',
			'sections' => 
				array(
					'items' => 
						array(
							'name' => $this->module.':name',
							'uri' => 'admin/'.$this->module,
							'shortcuts' => 
								array(
									array(
										'name' => $this->module.':create',
										'uri' => 'admin/'.$this->module.'/create',
										'class' => 'add'
									),
								),
						),
				),
		);
	}
	
	public function tablesSettings()
	{	
		//SET FIELDS DBASE
		$tables = array(
				$this->module => array(
						'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
						'prefix' => array('type' => 'VARCHAR', 'constraint' => 2, 'default' => ''),
						'name' => array('type' => 'VARCHAR', 'constraint' => 100, 'default' => ''),
						'folder' => array('type' => 'VARCHAR', 'constraint' => 50, 'default' => ''),
						'direction' => array('type' => 'VARCHAR', 'constraint' => 4, 'default' => ''),
						'codes' => array('type' => 'VARCHAR', 'constraint' => 40, 'default' => '', 'NULL' => true),
						'ckeditor' => array('type' => 'VARCHAR', 'constraint' => 10, 'default' => '', 'NULL' => true),
						//IMAGE FIELDS
						'thumbnail' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => '', 'NULL' => true),
						'image' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => '', 'NULL' => true),
						//FIXED FIELDS
						'update' => array('type' => 'TIMESTAMP'),
						'id_users' => array('type' => 'INT')
				)
		);
		
		//SET FIELDS FORM VIEW NONE OR HIDDEN
		$tabless = array(
				'id' => 'none',
				'update' => 'none',
				'id_users' => 'hidden',
				'id_lang' => 'hidden',
				'id_mod' => 'none'
		);
		
		return array($tables,$tabless);
	}

	public function install()
	{

		$this->dbforge->drop_table($this->module, true);
			
		$tables = $this->tablesSettings();
		
		$form = $this->funutillang->makeForms($tables,$this->module);
		
		$this->funutillang->choiceChange(null,$form,$this->module);
		
		if ( ! is_dir($this->upload_path.'/files/'.$this->module) AND ! @mkdir($this->upload_path.'/files/'.$this->module,0777,TRUE)): return FALSE; endif;
		
		$thumb_width = array(
				'slug' => 'thumb_'.$this->module.'_width',
				'title' => 'Largura',
				'description' => 'Defina a largura para a imagem',
				'`default`' => '25',
				'`value`' => '25',
				'type' => 'text',
				'`options`' => '',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => $this->module
		);
		
		$thumb_height = array(
				'slug' => 'thumb_'.$this->module.'_height',
				'title' => 'Altura',
				'description' => 'Defina a altura para a imagem',
				'`default`' => '20',
				'`value`' => '20',
				'type' => 'text',
				'`options`' => '',
				'is_required' => 0,
				'is_gui' => 1,
				'module' => $this->module
		);

		if( ! $this->db->insert('settings', $thumb_width)): return FALSE; endif;
		if( ! $this->db->insert('settings', $thumb_height)): return FALSE; endif;
		
		return $this->install_tables($tables[0]);
		$this->db->query("ALTER TABLE  `".$this-module."` CHANGE  `update`  `update` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
		

	}

	public function uninstall(){
		
		$this->dbforge->drop_table($this->module);
		$this->db->delete('settings', array('module' => $this->module));
		$this->funutillang->choiceChange(1,'',$this->module);
		
		return true;
	}

	public function upgrade($old_version) { return TRUE; }

	public function help() { return "Help não encontrado para este módulo"; }
	
}