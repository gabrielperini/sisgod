var tempImg = false;
$(document).ready(function() {
    
	$('.progress').hide();
	
	var bar = $('.bar');
	var status = $('#status');
	var erroimg = $('.erroimg');
	   
	$('#ajax-form-upload').ajaxForm({
		beforeSend: function() {
			status.empty();
			var percentVal = '0%';
			bar.width(percentVal)
			$(erroimg).html('')
			$('.progress').show();
		},
		uploadProgress: function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			bar.width(percentVal)
		},
		success: function(obj) {
			
			$('.progress').hide();
			
			// Delete old image first
			$.post(SITE_URL + '/admin/languages/ajax_delete_image/', {
				thumbnail: $('input[name="thumbnail"]').val(),
				image: $('input[name="image"]').val(),
				csrf_hash_name:  $('input[name="csrf_hash_name"]').val()
			},
			function(data) {});
			
			if(obj.erroimg){ 
				$(erroimg).html(obj.erroimg);
			} else {
				$('input[name="thumbnail"]').val(obj.thumbnail);
				$('input[name="image"]').val(obj.image);
				$('#thumbnail img').attr('src', obj.upload_path + obj.thumbnail);
				tempImg = true;
			}
		},
		error: function(){
			$('.progress').hide();
			$(erroimg).html('Algo deu Errado!');
		}
	}); 
	
	$('a#delete-image-button').click(deleteImg);  

	$('button[name=btnAction][value=save]').click(function(){
		window.onunload = null;
		return true;
	});  

	window.onunload = function(e){
		if(tempImg){
			deleteImg([]);
		}
		return true
	}
});

var deleteImg = (e) => {
	$.post(SITE_URL + 'admin/languages/ajax_delete_image/', {
		thumbnail: $('input[name="thumbnail"]').val(),
		image: $('input[name="image"]').val(),
		csrf_hash_name:  $('input[name="csrf_hash_name"]').val()
	},
	function(obj) {
		$('input[name="thumbnail"]').val('');
		$('input[name="image"]').val('');
		$('#thumbnail img').attr('src', 'http://placehold.it/'+obj.width+'x'+obj.height);
		tempImg = false;
	});
	return false;
}