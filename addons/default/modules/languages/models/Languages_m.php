<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for LazzoCMS
 *
 * @author 		Rico Vilela - BF2 Tecnologia
 * @website		http://bf2tecnologia.com.br
 * @package 	LazzoCMS
 * @subpackage 	Sample Module
 */
class languages_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		$this->_table = 'languages';
	}
	
	//create a new item
	public function create($input)
	{
		$to_insert = array(
			'prefix' => $input['prefix'],
						'name' => $input['name'],
						'folder' => $input['folder'],
						'direction' => $input['direction'],
						'codes' => $input['codes'],
						'ckeditor' => $input['ckeditor'],
						'thumbnail' => $input['thumbnail'],
						'image' => $input['image'],
						'id_users' => $input['id_users'],
						
		);

		return $this->db->insert('languages', $to_insert);
	}
	
	public function update($id, $input)
	{
		
		$to_insert = array(
			'prefix' => $input['prefix'],
						'name' => $input['name'],
						'folder' => $input['folder'],
						'direction' => $input['direction'],
						'codes' => $input['codes'],
						'ckeditor' => $input['ckeditor'],
						'thumbnail' => $input['thumbnail'],
						'image' => $input['image'],
						'id_users' => $input['id_users'],
						
		);
		
		$this->db
			->where('id', $id)
			->update($this->_table, $to_insert);
			
		return true;
	}
	
	public function  get_all_by(){
		
		$this->db->select('languages.*');
		
		return $this->db->get_where('languages',"id_mod = NULL")->result();
	}
	
	public function get_by_id($id){
		$this->db
		->select('languages.*')
		->where('id',$id);
		
		return $this->db->get('languages')->result();
	}
	
	public function get_languages_category($id){
		
		$this->db
		->select('languages.*');
		//->where('categoria',$id);
		
		return $this->db->get('languages')->result();
	}
	
	public function get_next_id(){
	
		$result = $this->db->query("SHOW TABLE STATUS LIKE 'default_languages' ")->result();
		
		foreach ($result as $objeto){	}
		
		return $objeto->Auto_increment;
	}
	
	public function insertLangs($input){
	
		$id = $this->db->insert_id();
	
		$count = count($_POST['id_lang']) - 1;
	
		$query = $this->db->get_where('settings',"module = 'languages' AND slug = 'set_languages_language'");
		$rs = $query->result();
		$rt = explode(',',$rs[0]->value);
	
		for($i=0;$i<$count;$i++){
			$to_insert = '';
			foreach($rt as $ret){
				$to_insert[$ret] = $input[$ret.'_lang'][$i];
			}
	
			$to_insert['id_users'] = $input['id_users'];
			$to_insert['id_lang'] = $input['id_lang'][$i+1];
			$to_insert['id_mod'] = $id;
	
			$this->db->insert('languages', $to_insert);
		}
	
		if($i>0): return true; else: return false; endif;
	}
	
	public function updateLangs($input){
	
		$count = count($_POST['id_lang']) - 1;
	
		$query = $this->db->get_where('settings',"module = 'languages' AND slug = 'set_languages_language'");
		$rs = $query->result();
		$rt = explode(',',$rs[0]->value);
	
		for($i=0;$i<$count;$i++){
			$to_insert = '';
			foreach($rt as $ret){
				$to_insert[$ret] = $input[$ret.'_lang'][$i];
			}
	
			$to_insert['id_users'] = $input['id_users'];
	
			if($input['id'][$i] == 'x'):
				$to_insert['id_users'] = $input['id_users'];
				$to_insert['id_lang'] = $input['id_lang'][$i+1];
				$to_insert['id_mod'] = $this->uri->segment(4);
				$this->db->insert('modeconstruct', $to_insert);
			else:
				$this->db
				->where('id', $input['id'][$i])
				->update($this->_table, $to_insert);
			endif;
		}
	
		if($i>0): return true; else: return false; endif;
	}
	
}
