<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends Admin_Controller
{
	protected $section = 'items';
	
    public function __construct()
    {
        parent::__construct();
        // Load all the required classes
        $this->load->model('BASEMODELADMIN_m');
        $this->load->library('form_validation');
        $this->lang->load('BASEMODELADMIN');
        
        $this->item_validation_rules = array(
        		BASEFORMADMIN
        );
       
    }

    public function index()
    {
    	$items = $this->BASEMODELADMIN_m->get_all();
    	
    	$this->template
    	->title($this->module_details['name'])
    	->set('items', $items)
    	->build('admin/'.$this->module.'/items');
    }
    
    public function create()
    {
    	$this->form_validation->set_rules($this->item_validation_rules);
    
    	if($this->form_validation->run())
    	{
    		if($this->BASEMODELADMIN_m->create($this->input->post()))
    		{
    			$this->session->set_flashdata('success', lang('BASEMODELADMIN:success'));
    			redirect('admin/'.$this->module);
    		}
    		else
    		{
    			$this->session->set_flashdata('error', lang('general:error'));
    			redirect('admin/'.$this->module.'/create');
    		}
    	}
    
    	foreach ($this->item_validation_rules AS $rule)
    	{
    		$data->{$rule['field']} = $this->input->post($rule['field']);
    	}
    
    	
		$this->template
			->title($this->module_details['name'])
			
			->append_css('module::colorpicker.css')
			->append_js('module::jquery.form.js')
			->append_js('module::image.js')
			->append_js('module::colorpicker.js')
			->append_js('module::eye.js')
			->append_js('module::utils.js');

    	$this->template->build('admin/'.$this->module.'/form', $data);
    }
    
    public function edit()
    {
    	$id = $this->uri->segment(4);
    	
    	if($id == ''){
    		show_404();exit();
    	}
    
    	$this->data = $this->BASEMODELADMIN_m->get($id);
    
    	$this->form_validation->set_rules($this->item_validation_rules);
    
    	if($this->form_validation->run())
    	{
    		unset($_POST['btnAction']);
    
    		if($this->BASEMODELADMIN_m->update($id, $this->input->post()))
    		{
    			$this->session->set_flashdata('success', lang('general:success'));
    			redirect('admin/'.$this->module);
    		}
    		else
    		{
    			$this->session->set_flashdata('error', lang('general:error'));
    			redirect('admin/'.$this->module.'/create');
    		}
    	}
    
    	
    	if (Settings::get('BASEMODELADMIN_galeria')) {
    		$this->edit_file_folder_func($this->data->pasta_imagens);
    	}
    	
    	$this->template->title($this->module_details['name'], lang('BASEMODELADMIN:edit'))
    	
    	->append_css('module::colorpicker.css')
    	->append_js('module::jquery.form.js')
    	->append_js('module::image.js')
		->append_js('module::colorpicker.js')
		->append_js('module::eye.js')
		->append_js('module::utils.js');

    	$this->template->build('admin/'.$this->module.'/form', $this->data);
    }
    
    
	/**
	 * Delete blog post
	 *
	 * @param int $id The ID of the blog post to delete
	 */
  public function delete($id = 0)
     {
         // make sure the button was clicked and that there is an array of ids
         if (isset($_POST['btnAction']) AND is_array($_POST['action_to']))
         {
         	// Se tem galeria de imagens, deve apagar os arquivos e pastas..
         	if (!Settings::get('BASEMODELADMIN_galeria')) {
            // pass the ids and let MY_Model delete the items
	        	$this->BASEMODELADMIN_m->delete_many($this->input->post('action_to'));
         	}else{
         	
	         	foreach($this->input->post('action_to') as $key => $noticia_id){
	         		$BASEMODELADMIN = $this->BASEMODELADMIN_m->get($noticia_id);
	         		
	         		// this is just a safeguard if they circumvent the JS permissions
			    	if ( ! in_array('delete_folder', Files::allowed_actions()))
			    	{
			    		show_error(lang('files:no_permissions'));
			    	}
		    		$result = Files::delete_folder_and_files($BASEMODELADMIN->pasta_imagens);
	         		$this->BASEMODELADMIN_m->delete($BASEMODELADMIN_id);
	         	}
         	}
         }
         elseif (is_numeric($id))
         {
             // they just clicked the link so we'll delete that one
             $BASEMODELADMIN = $this->BASEMODELADMIN_m->get($id);
             
             //$result = Files::delete_folder_and_files($BASEMODELADMIN->pasta_imagens);
             $this->BASEMODELADMIN_m->delete($id);
         }
         
         $this->session->set_flashdata('success', lang('general:success'));
         redirect('admin/'.$this->module);
     }
    
    public function ajax_upload_image()
    {
    	if($this->input->is_ajax_request()) {
    		$this->load->library('upload');
    
    		$upload_path = UPLOAD_PATH.'files/'.$this->module.'/';
    
    
    		if(!is_dir($upload_path))
    		{
    			@mkdir($upload_path,0777,TRUE);
    		}
    
    		if(is_dir($upload_path))
    
    		$config['upload_path'] 		=  $upload_path;
    		$config['allowed_types'] 	= 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
    		$config['remove_spaces']	= TRUE;
    		$config['overwrite']		= FALSE;
    		$config['max_size']			= 0;
    		$config['encrypt_name']		= TRUE;
    
    		$this->upload->initialize($config);
    		 
    		if($this->upload->do_upload('file')) {
    
    			$upload_data = $this->upload->data();
    
    			$this->load->library('image_lib');
    
    			unset($config);
    			$config['source_image'] = $upload_path.$upload_data['file_name'];
    			$config['maintain_ratio'] = TRUE;
    			$config['quality'] = '100%';
    			$config['width'] = '250';
    			$config['height'] = '43';
    			$config['new_image'] = $upload_path.$upload_data['raw_name'].'_thumb'.$upload_data['file_ext'];
    
    			if($upload_data['image_width'] > $upload_data['image_height'])
    			{
    				$config['master_dim'] = 'width';
    			}
    			elseif($upload_data['image_height'] > $upload_data['image_width'])
    			{
    				$config['master_dim'] = 'height';
    			}
    			else
    			{
    				$config['maintain_ratio'] = TRUE;
    			}
    
    			$this->image_lib->initialize($config);
    			$this->image_lib->resize();
    			$this->image_lib->clear();
    
    			unset($config);
    			$config['image_library'] = 'ImageMagick';
    
    			$image_size = getimagesize($upload_path.$upload_data['raw_name'].'_thumb'.$upload_data['file_ext']);
    			$config['source_image'] = $upload_path.$upload_data['raw_name'].'_thumb'.$upload_data['file_ext'];
    
    			$config['x_axis'] = (($image_size[0] - $this->settings->thumb_BASEMODELADMIN_width) / 2);
    			$config['y_axis'] = (($image_size[1] - $this->settings->thumb_BASEMODELADMIN_height) / 2);
    			$config['width'] = $this->settings->thumb_BASEMODELADMIN_width;
    			$config['height'] = $this->settings->thumb_BASEMODELADMIN_height;
    			$config['quality'] = '100%';
    			$config['maintain_ratio'] = FALSE;
    
    			$this->image_lib->initialize($config);
    			$this->image_lib->crop();
    			$this->image_lib->clear();
    
    			$json_data = array(
    					'raw_name' => $upload_data['raw_name'],
    					'thumbnail' => $upload_data['raw_name'].'_thumb'.$upload_data['file_ext'],
    					'image' => $upload_data['raw_name'].$upload_data['file_ext'],
    					'ext' => $upload_data['file_ext'],
    					'upload_path' => base_url().'uploads/'.SITE_REF.'/files/'.$this->module.'/'
    			);
    
    			$this->template->build_json($json_data);
    		}
    	}
    }
    
    public function ajax_delete_image()
    {
    	if($this->input->is_ajax_request()) {
    		if($this->input->post('thumbnail')) {
    			$image_size = getimagesize(UPLOAD_PATH.'files/'.$this->module.'/'.$this->input->post('thumbnail'));
    
    			unlink(UPLOAD_PATH.'files/'.$this->module.'/'.$this->input->post('thumbnail'));
    		}
    		if($this->input->post('image')) {
    			unlink(UPLOAD_PATH.'files/'.$this->module.'/'.$this->input->post('image'));
    		}
    		 
    		if(isset($image_size)) {
    			$json_data = array(
    					'width' => $image_size[0],
    					'height' => $image_size[1]
    			);
    
    			$this->template->build_json($json_data);
    		}
    	}
    }
    
}