<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Sisgod extends Theme
{
    public $name			= 'Tema Sistema SISGOD';
    public $author			= 'Gabriel Perini';
    public $author_website	= '';
    public $website			= '';
    public $description		= 'Template HTML5 for SISGOD';
    public $version			= '1.0.0';
	public $options 		= array();
}

/* End of file theme.php */